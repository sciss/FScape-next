/*
 *  AudioFileIn.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.swing.graph

import de.sciss.audiofile.{AudioFile, AudioFileSpec, SampleFormat}
import de.sciss.audiowidgets.AxisFormat
import de.sciss.lucre.Txn.peer
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.graph.{Const, Ex}
import de.sciss.lucre.expr.{Context, IControl, Model}
import de.sciss.lucre.impl.IChangeGeneratorEvent
import de.sciss.lucre.swing.graph.impl.{ComponentImpl, FileInExpandedImpl, PathFieldValueExpandedImpl}
import de.sciss.lucre.swing.{Graph, InputPathFieldPanel, LucreSwing, View}
import de.sciss.lucre.synth.Executor
import de.sciss.lucre.{Cursor, IChangeEvent, IExpr, IPull, ITargets, Txn}
import de.sciss.model
import de.sciss.model.Change
import de.sciss.proc.AudioCue

import java.io.FileNotFoundException
import java.net.URI
import scala.concurrent.Future
import scala.concurrent.stm.Ref
import scala.util.Try

object AudioFileIn extends ProductReader[AudioFileIn] {
  def apply(): AudioFileIn = Impl()

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): AudioFileIn = {
    require (arity == 0 && adj == 0)
    AudioFileIn()
  }

  private lazy val timeFmt = AxisFormat.Time(hours = false, millis = true)

  def specToString(spec: AudioFileSpec): String = {
    import spec._
    val smpFmt  = formatToString(sampleFormat)
    val channels = spec.numChannels match {
      case 1 => "mono"
      case 2 => "stereo"
      case n => s"$n-chan."
    }
    val sr  = f"${sampleRate/1000}%1.1f"
    val dur = timeFmt.format(numFrames.toDouble / sampleRate)
    val txt = s"${fileType.name}, $channels $smpFmt $sr kHz, $dur"
    txt
  }

  def formatToString(smp: SampleFormat): String = {
    val smpTpe = smp match {
      case SampleFormat.Float | SampleFormat.Double => "float"
      case SampleFormat.UInt8                       => "uint"
      case _                                        => "int"
    }
    val txt = s"${smp.bitsPerSample}-bit $smpTpe"
    txt
  }

  private final class Expanded[T <: Txn[T]](protected val peer: AudioFileIn)
    extends FileInExpandedImpl[T, AudioCue] with Repr[T] {

    override type C = Peer

    override protected def mkFormat(uri: URI): Future[AudioCue] = {
      if (!uri.isAbsolute) Future.failed(new FileNotFoundException(uri.toString))
      else {
        import Executor.executionContext
        AudioFile.readSpecAsync(uri).map { spec =>
          // specToString(spec)
          AudioCue(uri, spec, 0L, 1.0)
        }
      }
    }

    override protected def formatString(f: AudioCue): String = specToString(f.spec)
  }

  object Value extends ProductReader[Value] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Value = {
      require (arity == 1 && adj == 0)
      val _w = in.readProductT[AudioFileIn]()
      new Value(_w)
    }
  }
  final case class Value(w: AudioFileIn) extends Ex[URI] {
    type Repr[T <: Txn[T]] = IExpr[T, URI]

    override def productPrefix: String = s"AudioFileIn$$Value" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      import ctx.{cursor, targets}
      val ws        = w.expand[T]
      val valueOpt  = ctx.getProperty[Ex[URI]](w, PathField.keyValue)
      val value0    = valueOpt.fold[URI](PathField.defaultValue)(_.expand[T].value)
      new PathFieldValueExpandedImpl[T](ws.component.pathField, value0).init()
    }
  }

  private final class CueExpanded[T <: Txn[T]](w: AudioFileIn.Repr[T], tx0: T)
                                              (protected implicit val targets: ITargets[T], cursor: Cursor[T])
    extends IExpr[T, AudioCue] with IChangeGeneratorEvent[T, AudioCue] {

    private[this] var guiValue  = AudioCue.empty
    private[this] val txValue   = Ref(guiValue)

    override def value(implicit tx: T): AudioCue = txValue()

    override def dispose()(implicit tx: T): Unit =
      LucreSwing.deferTx {
        w.component.removeListener(ml)
      }

    override def changed: IChangeEvent[T, AudioCue] = this

    override private[lucre] def pullChange(pull: IPull[T])(implicit tx: T, phase: IPull.Phase): AudioCue =
      pull.resolveExpr(this)

    private[this] val ml: model.Model.Listener[Try[AudioCue]] = {
      case tr =>
        val before  = guiValue
        val now     = tr.getOrElse(AudioCue.empty)
        val ch      = Change(before, now)
        if (ch.isSignificant) {
          guiValue    = now
          cursor.step { implicit tx =>
            txValue.set(now)(tx.peer)
            fire(ch)
          }
        }
    }

    // constructor
    LucreSwing.deferTx {
      w.component.addListener(ml)
      ()
    } (tx0)
  }
  object Cue extends ProductReader[Cue] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Cue = {
      require (arity == 1 && adj == 0)
      val _w = in.readProductT[AudioFileIn]()
      new Cue(_w)
    }
  }
  final case class Cue(w: AudioFileIn) extends Ex[AudioCue] {
    type Repr[T <: Txn[T]] = IExpr[T, AudioCue]

    override def productPrefix: String = s"AudioFileIn$$Cue" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val wEx = w.expand[T]
      import ctx.{cursor, targets}
      new CueExpanded[T](wEx, tx)
    }
  }

  object Title extends ProductReader[Title] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Title = {
      require (arity == 1 && adj == 0)
      val _w = in.readProductT[AudioFileIn]()
      new Title(_w)
    }
  }
  final case class Title(w: AudioFileIn) extends Ex[String] {
    type Repr[T <: Txn[T]] = IExpr[T, String]

    override def productPrefix: String = s"AudioFileIn$$Title" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val valueOpt = ctx.getProperty[Ex[String]](w, PathField.keyTitle)
      valueOpt.getOrElse(Const(defaultTitle)).expand[T]
    }
  }

  object PathFieldVisible extends ProductReader[PathFieldVisible] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): PathFieldVisible = {
      require (arity == 1 && adj == 0)
      val _w = in.readProductT[AudioFileIn]()
      new PathFieldVisible(_w)
    }
  }
  final case class PathFieldVisible(w: AudioFileIn) extends Ex[Boolean] {
    type Repr[T <: Txn[T]] = IExpr[T, Boolean]

    override def productPrefix: String = s"AudioFileIn$$PathFieldVisible" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val valueOpt = ctx.getProperty[Ex[Boolean]](w, keyPathFieldVisible)
      valueOpt.getOrElse(Const(defaultPathFieldVisible)).expand[T]
    }
  }

  object FormatVisible extends ProductReader[FormatVisible] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FormatVisible = {
      require (arity == 1 && adj == 0)
      val _w = in.readProductT[AudioFileIn]()
      new FormatVisible(_w)
    }
  }
  final case class FormatVisible(w: AudioFileIn) extends Ex[Boolean] {
    type Repr[T <: Txn[T]] = IExpr[T, Boolean]

    override def productPrefix: String = s"AudioFileIn$$FormatVisible" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val valueOpt = ctx.getProperty[Ex[Boolean]](w, keyFormatVisible)
      valueOpt.getOrElse(Const(defaultFormatVisible)).expand[T]
    }
  }

  private final case class Impl() extends AudioFileIn with ComponentImpl { w =>
    override def productPrefix: String = "AudioFileIn" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): AudioFileIn.Repr[T] =
      new Expanded[T](this).initComponent()

    object value extends Model[URI] {
      def apply(): Ex[URI] = Value(w)

      def update(value: Ex[URI]): Unit = {
        val b = Graph.builder
        b.putProperty(w, PathField.keyValue, value)
      }
    }

    override def cue: Ex[AudioCue] = Cue(this)

    def title: Ex[String] = Title(this)

    def title_=(value: Ex[String]): Unit = {
      val b = Graph.builder
      b.putProperty(this, PathField.keyTitle, value)
    }

    def pathFieldVisible: Ex[Boolean] = PathFieldVisible(this)

    def pathFieldVisible_=(value: Ex[Boolean]): Unit = {
      val b = Graph.builder
      b.putProperty(this, keyPathFieldVisible, value)
    }

    def formatVisible: Ex[Boolean] = FormatVisible(this)

    def formatVisible_=(value: Ex[Boolean]): Unit = {
      val b = Graph.builder
      b.putProperty(this, keyFormatVisible, value)
    }
  }

  private[graph] val keyPathFieldVisible  = "pathFieldVisible"
  private[graph] val keyFormatVisible     = "formatVisible"
  private[graph] val defaultTitle         = "Select Audio Input File"
  
  private[graph] val defaultPathFieldVisible  = true
  private[graph] val defaultFormatVisible     = true

  type Peer = InputPathFieldPanel[AudioCue]

  trait Repr[T <: Txn[T]] extends View[T] with IControl[T] {
    type C = AudioFileIn.Peer
  }
}
trait AudioFileIn extends Component {
  type C = AudioFileIn.Peer

  type Repr[T <: Txn[T]] = AudioFileIn.Repr[T]

  var title: Ex[String]

  def value: Model[URI]
  
  def cue: Ex[AudioCue]
  
  var pathFieldVisible: Ex[Boolean]
  var formatVisible   : Ex[Boolean]
}

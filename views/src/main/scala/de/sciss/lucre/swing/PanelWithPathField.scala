/*
 *  PanelWithPathField.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.swing

import de.sciss.desktop
import de.sciss.model.Model

import scala.util.Try

abstract class PanelWithPathField extends scala.swing.Panel {
  def pathField: desktop.PathField
}

abstract class InputPathFieldPanel[+F] extends PanelWithPathField with Model[Try[F]] {
  def format: Try[F]
}
/*
 *  FileInExpandedImpl.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.swing.graph.impl

import de.sciss.asyncfile.FileNotFoundException
import de.sciss.desktop
import de.sciss.desktop.TextFieldWithPaint
import de.sciss.lucre.{Artifact, Txn}
import de.sciss.lucre.expr.Context
import de.sciss.lucre.expr.graph.Ex
import de.sciss.lucre.swing.LucreSwing.{defer, deferTx, requireEDT}
import de.sciss.lucre.swing.graph.{AudioFileIn, PathField}
import de.sciss.lucre.swing.impl.ComponentHolder
import de.sciss.lucre.swing.{InputPathFieldPanel, View}
import de.sciss.lucre.synth.Executor
import de.sciss.model.impl.ModelImpl

import java.io.File
import java.net.URI
import scala.concurrent.Future
import scala.swing.event.ValueChanged
import scala.swing.{Orientation, SequentialContainer, Swing}
import scala.util.{Failure, Success, Try}

trait FileInExpandedImpl[T <: Txn[T], F]
  extends View[T] with ComponentHolder[InputPathFieldPanel[F]] with ComponentExpandedImpl[T] {

  type C = InputPathFieldPanel[F]

  protected def mkFormat(f: URI): Future[F]

  protected def formatString(f: F): String

  override def initComponent()(implicit tx: T, ctx: Context[T]): this.type = {
    val valueOpt  = ctx.getProperty[Ex[URI    ]](peer, PathField.keyValue).map(_.expand[T].value)
    val titleOpt  = ctx.getProperty[Ex[String ]](peer, PathField.keyTitle).map(_.expand[T].value)
    val pathVis   = ctx.getProperty[Ex[Boolean]](peer, AudioFileIn.keyPathFieldVisible).fold(
      AudioFileIn.defaultPathFieldVisible)(_.expand[T].value)
    val fmtVis    = ctx.getProperty[Ex[Boolean]](peer, AudioFileIn.keyFormatVisible).fold(
      AudioFileIn.defaultFormatVisible)(_.expand[T].value)

    deferTx {
      val c: C = new InputPathFieldPanel[F] with ModelImpl[Try[F]]
        with SequentialContainer.Wrapper with PathFieldWatcher  {

        private[this] lazy val fmc = {
          val res = new TextFieldWithPaint(27)
          res.editable  = false
          res.focusable = false
          res
        }

        override protected def setFormatText(s: String): Unit =
          if (fmtVis) fmc.text = s

        override def enabled_=(b: Boolean): Unit = {
          super.enabled_=(b)
          if (pathVis ) pathField .enabled = b
          if (fmtVis  ) fmc       .enabled = b
        }

        private[this] var _format: Try[F] = Failure(new FileNotFoundException(Artifact.Value.empty))

        override def format: Try[F] = {
          requireEDT()
          _format
        }

        protected def updateFormat(uri: URI): Unit = {
          val fmtFut = mkFormat(uri)
          import Executor.executionContext
          fmtFut.onComplete { tr =>
            defer {
              if (uriOption.contains(uri)) {
                _format = tr
                dispatch(tr)
                tr match {
                  case Success(fmt) =>
                    val fmtS = formatString(fmt)
                    setFormatText(fmtS)
                    pathField.paint = None

                  case Failure(ex) =>
                    setFormatText(ex.toString)
                    pathField.paint = Some(TextFieldWithPaint.RedOverlay)
                }
              }
            }
          }
        }

        lazy val pathField: desktop.PathField = {
          val res = new desktop.PathField
          valueOpt.foreach { uri =>
            val fileOpt     = if (!uri.isAbsolute) None else Try(new File(uri)).toOption
            res.valueOption = fileOpt
          }
          titleOpt.foreach(res.title = _)
          res.listenTo(res)
          res.reactions += {
            case ValueChanged(_) => valueUpdated()
          }
          res
        }

        if (pathVis) valueUpdated()

        private[this] lazy val fb = {
          val res = new scala.swing.BoxPanel(Orientation.Horizontal)
          res.contents += fmc
          if (pathVis) {
            res.contents += Swing.HStrut(pathField.button.preferredSize.width)
          }
          res
        }

        override lazy val peer: javax.swing.JPanel = {
          val p = new javax.swing.JPanel with SuperMixin {
            override def getBaseline(width: Int, height: Int): Int =
              if (!pathVis) super.getBaseline(width, height) else {
                val pfj = pathField.peer
                val d   = pfj.getPreferredSize
                val res = pfj.getBaseline(d.width, d.height)
                res + pfj.getY
              }
          }
          val l = new javax.swing.BoxLayout(p, Orientation.Vertical.id)
          p.setLayout(l)
          p
        }

        if (pathVis ) contents += pathField
        if (fmtVis  ) contents += fb
      }

      component = c
    }
    super.initComponent()
  }
}
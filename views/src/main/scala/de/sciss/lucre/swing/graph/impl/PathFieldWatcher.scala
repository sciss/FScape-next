/*
 *  PathFieldWatcher.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.swing.graph.impl

import de.sciss.asyncfile.{AsyncFile, Watch}
import de.sciss.desktop.{PathField, TextFieldWithPaint}
import de.sciss.lucre.swing.LucreSwing.defer
import de.sciss.lucre.synth.Executor
import de.sciss.model.Model

import java.net.URI
import javax.swing.JComponent
import javax.swing.event.{AncestorEvent, AncestorListener}
import scala.util.control.NonFatal

trait PathFieldWatcher {
  // ---- abstract ----

  protected def updateFormat(uri: URI): Unit

  protected def peer: JComponent

  protected def pathField: PathField

  protected def setFormatText(s: String): Unit

  // ---- impl ----

  protected var uriOption = Option.empty[URI]

  private var fileModel = Option.empty[Model[Watch.File]]

  private val fileWatcher: Model.Listener[Watch.File] = {
    case u if uriOption.contains(u.uri) => updateFormat(u.uri)
  }

  private var useWatcher = false

  private def clearPaint(): Unit = {
    setFormatText("")
    pathField.paint = None
  }

  private def stopWatcher(): Unit =
    fileModel.foreach { fm =>
      fm.removeListener(fileWatcher)
      fileModel = None
    }

  private def startWatcher(uri: URI): Unit = {
    import Executor.executionContext
    AsyncFile.getFileSystemProvider(uri).foreach { fsp =>
      fsp.obtain().foreach { fs =>
        defer {
          if (useWatcher && uriOption.contains(uri)) {
            stopWatcher()
            val fm = fs.watchFile(uri)
            fm.addListener(fileWatcher)
            fileModel = Some(fm)
          }
        }
      }
    }
  }

  protected def valueUpdated(): Unit =
    pathField.valueOption match {
      case Some(f) =>
        try {
          val uri   = f.toURI
          uriOption = Some(uri)
          clearPaint()
          if (useWatcher) startWatcher(uri)
          updateFormat(uri)
        } catch {
          case NonFatal(ex) =>
            setFormatText(ex.toString)
            pathField.paint = Some(TextFieldWithPaint.RedOverlay)
        }
      case None =>
        uriOption = None
        clearPaint()
    }

  peer.addAncestorListener(new AncestorListener {
    override def ancestorAdded(event: AncestorEvent): Unit = {
      useWatcher = true
      uriOption.foreach(startWatcher)
    }

    override def ancestorRemoved(event: AncestorEvent): Unit = {
      useWatcher = false
      stopWatcher()
    }

    override def ancestorMoved(event: AncestorEvent): Unit = ()
  })
}

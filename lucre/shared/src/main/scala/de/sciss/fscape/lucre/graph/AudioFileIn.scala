/*
 *  AudioFileIn.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre
package graph

import de.sciss.audiofile.AudioFile
import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.graph.{ConstD, ConstL, SampleRate => _SampleRate}
import de.sciss.fscape.lucre.UGenGraphBuilder.Input
import de.sciss.fscape.lucre.graph.impl.FutureConstant
import de.sciss.fscape.stream.{StreamIn, StreamOut, Builder => SBuilder}
import de.sciss.fscape.{GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource}
import de.sciss.lucre.Artifact
import de.sciss.proc.AudioCue
import de.sciss.synth.UGenSource.Vec

import java.net.URI

object AudioFileIn extends AudioFileInPlatform with ProductReader[AudioFileIn] {
  object Offset extends ProductReader[Offset] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Offset = {
      require(arity == 1 && adj == 0)
      val _key = in.readString()
      new Offset(_key)
    }
  }

  final case class Offset(key: String) extends GE.Lazy[Long] {
    override def productPrefix = s"AudioFileIn$$Offset"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] = {
      val cueTr = AudioFileIn.getCue(key, b)
      cueTr match {
        case Left(cue) => ConstL(cue.offset)
        case Right(_) => ConstL(0L)
      }
    }
  }

  object FileOffset extends ProductReader[FileOffset] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FileOffset = {
      require(arity == 1 && adj == 0)
      val _key = in.readString()
      new FileOffset(_key)
    }
  }

  final case class FileOffset(key: String) extends GE.Lazy[Long] {
    override def productPrefix = s"AudioFileIn$$FileOffset"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] = {
      val cueTr = AudioFileIn.getCue(key, b)
      cueTr match {
        case Left(cue)  => ConstL(cue.fileOffset)
        case Right(_)   => ConstL(0L)
      }
    }
  }

  object NumFrames extends ProductReader[NumFrames] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): NumFrames = {
      require(arity == 1 && adj == 0)
      val _key = in.readString()
      new NumFrames(_key)
    }
  }

  private def futNumFrames(uri: URI): FutureConstant[Long] =
    FutureConstant[Long](Adjunct.FileIn(uri), { ctrl =>
      import ctrl.config.executionContext
      AudioFile.readSpecAsync(uri).map(_.numFrames)
    })

  final case class NumFrames(key: String) extends GE.Lazy[Long] {
    override def productPrefix = s"AudioFileIn$$NumFrames"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] = {
      val cueTr = AudioFileIn.getCue(key, b)
      cueTr match {
        case Left(cue)  => ConstL(cue.numFrames)
        case Right(uri) => futNumFrames(uri)
      }
    }
  }

  object Length extends ProductReader[Length] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Length = {
      require (arity == 1 && adj == 0)
      val _key = in.readString()
      new Length(_key)
    }
  }
  final case class Length(key: String) extends GE.Lazy[Long] {
    override def productPrefix = s"AudioFileIn$$Length"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] = {
      val cueTr = AudioFileIn.getCue(key, b)
      cueTr match {
        case Left (cue) => ConstL(math.max(0L, cue.numFrames - cue.fileOffset)) // XXX TODO use cue.length once it is implemented
        case Right(uri) => futNumFrames(uri)
      }
    }
  }

  object SampleRate extends ProductReader[SampleRate] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SampleRate = {
      require (arity == 1 && adj == 0)
      val _key = in.readString()
      new SampleRate(_key)
    }
  }
  final case class SampleRate(key: String) extends _SampleRate with GE.Lazy[Double] {
    override def productPrefix = s"AudioFileIn$$SampleRate"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] = {
      val cueTr = AudioFileIn.getCue(key, b)
      cueTr match {
        case Left (cue) => ConstD(cue.sampleRate)
        case Right(uri) => FutureConstant[Double](Adjunct.FileIn(uri), { ctrl =>
          import ctrl.config.executionContext
          AudioFile.readSpecAsync(uri).map(_.sampleRate)
        })
      }
    }
  }

  private def getCue(key: String, b: UGenGraph.Builder): Either[AudioCue, Artifact.Value] = {
    val ub  = UGenGraphBuilder.get(b)
    val v = ub.requestInput(Input.Attribute(key)).peer.getOrElse(sys.error(s"AudioFileIn missing attribute $key"))
    v match {
      case a: AudioCue        => Left(a)
      case f: Artifact.Value  => Right(f)
      case other => sys.error(s"AudioFileIn - requires AudioCue or Artifact value, found $other")
    }
  }

  object WithCue extends ProductReader[WithCue] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): WithCue = {
      require (arity == 4 && adj == 0)
      val _uri          = in.readURI()
      val _offset       = in.readLong()
      val _gain         = in.readDouble()
      val _numChannels  = in.readInt()
      new WithCue(_uri, _offset, _gain, _numChannels)
    }
  }

  /** @param offset as in `AudioCue`, that is with respect to `TimeRef.SampleRate`
    */
  final case class WithCue(uri: URI, offset: Long, gain: Double, numChannels: Int)
    extends UGenSource.MultiOut[Double] {

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
      makeUGen(Vector.empty)

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] = {
      val adjuncts =
        Adjunct.FileIn(uri) :: /*Adjunct.AudioFileSpec(cue.spec) ::*/
            Adjunct.Long(offset) :: Adjunct.Double(gain) :: Nil

      UGen.MultiOut(this, args, numOutputs = numChannels, adjuncts = adjuncts)
    }

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: SBuilder): Vec[StreamOut] = {
      stream.AudioFileIn(uri = uri, offset = offset, gain = gain, numChannels = numChannels)
    }

    override def productPrefix: String = s"AudioFileIn$$WithCue"
  }

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): AudioFileIn = {
    require (arity == 1 && adj == 0)
    val _key = in.readString()
    new AudioFileIn(_key)
  }
}
final case class AudioFileIn(key: String) extends GE.Lazy[Double] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] = {
    val cueTr = AudioFileIn.getCue(key, b)
    cueTr match {
      case Left (cue) =>
        AudioFileIn.WithCue(cue.artifact, cue.offset, cue.gain, cue.spec.numChannels)
      case Right(uri) =>
        // XXX TODO -- we do have to find a way to determine the number of channels
        // before expanding the UGen
        // - could be synchronous on JVM and yet unsupported on JS
        // - should have an asynchronous `prepare` stage like AuralProc
//        AudioFileIn.WithCue(uri, offset = 0L, gain = 1.0, numChannels = 1)
        AudioFileIn.mkCue(uri)
    }
  }

  /** The number of frames in the underlying file, irrespective of a chosen offset.
    * Usually one will want to use `length` instead.
    */
  def numFrames : GE.L        = AudioFileIn.NumFrames (key)
  /** The number of frames produced by this graph element, i.e. taking offset into account, such
    * that the returned value is the same as `Length(...)` would report
    */
  def length    : GE.L        = AudioFileIn.Length    (key)
  def sampleRate: _SampleRate = AudioFileIn.SampleRate(key)
  def offset    : GE.L        = AudioFileIn.Offset    (key)
  def fileOffset: GE.L        = AudioFileIn.FileOffset(key)
}
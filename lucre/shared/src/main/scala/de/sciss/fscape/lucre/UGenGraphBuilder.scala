/*
 *  UGenGraphBuilder.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package lucre

import de.sciss.fscape.graph.impl.GESeq
import de.sciss.fscape.graph.{ArithmSeq, BinaryOp, Const, GeomSeq, UnaryOp}
import de.sciss.fscape.lucre.UGenGraphBuilder.OutputRef
import de.sciss.fscape.lucre.graph.Attribute
import de.sciss.fscape.lucre.impl.{AbstractOutputRef, AbstractUGenGraphBuilder}
import de.sciss.fscape.stream.Control
import de.sciss.lucre.expr.graph.{Const => ExConst, Var => ExVar}
import de.sciss.lucre.{Artifact, Source, Txn, Workspace}
import de.sciss.proc.FScape.Output
import de.sciss.proc.impl.FScapeOutputImpl
import de.sciss.proc.{AuralSystem, FScape, Runner, Universe}
import de.sciss.serial.DataInput
import de.sciss.synth.UGenSource.Vec

import scala.annotation.tailrec
import scala.util.control.ControlThrowable

object UGenGraphBuilder /*extends UGenGraphBuilderPlatform*/ {
  def get(b: UGenGraph.Builder): UGenGraphBuilder = b match {
    case ub: UGenGraphBuilder => ub
    case _ => sys.error("Out of context expansion")
  }

  def build[T <: Txn[T]](context: Context[T], f: FScape[T])(implicit tx: T,
                                                            universe: Universe[T],
                                                            ctrl: Control): State[T] = {
    val b = new BuilderImpl(context, f)
    val g = f.graph.value
    b.tryBuild(g)
  }

//  def build[T <: Txn[T]](context: Context[T], g: Graph)(implicit tx: T, cursor: stm.Cursor[T],
//                                                        workspace: Workspace[T],
//                                                        ctrl: Control): State[T] =
//    buildOpt[T](context, None, g)
//
//  private def buildOpt[T <: Txn[T]](context: Context[T], fOpt: Option[FScape[T]], g: Graph)
//                                   (implicit tx: T, workspace: Workspace[T],
//                                    ctrl: Control): State[T] = {
//    val b = new BuilderImpl(context, fOpt)
//    b.tryBuild(g)
//  }

  /** A pure marker trait to rule out some type errors. */
  trait Key
  /** A scalar value found in the attribute map. */
  final case class AttributeKey(name: String) extends Key

  /** A pure marker trait to rule out some type errors. */
  trait Value {
//    def async: Boolean
  }

  case object Unit extends Value
  type Unit = Unit.type

  object Input {

    object Attribute {
      final case class Value(peer: Option[Any]) extends UGenGraphBuilder.Value {
//        def async = false
        override def productPrefix = "Input.Attribute.Value"
      }
    }
    /** Specifies access to a an attribute's value at build time.
      *
      * @param name   name (key) of the attribute
      */
    final case class Attribute(name: String) extends Input {
      type Key    = AttributeKey
      type Value  = Attribute.Value

      def key: Key = AttributeKey(name)

      override def productPrefix = "Input.Attribute"
    }

//    object InSeq {
//      final case class Value(peer: Option[Any]) extends UGenGraphBuilder.Value {
//        override def productPrefix = "Input.InSeq.Value"
//      }
//    }
//    /** Specifies access to a an attribute's value which is a `Seq[_]` at build time.
//      *
//      * @param name   name (key) of the attribute
//      */
//    final case class InSeq(name: String) extends Input {
//      type Key    = AttributeKey
//      type Value  = InSeq.Value
//
//      def key: Key = AttributeKey(name)
//
//      override def productPrefix = "Input.InSeq"
//    }

    object Action {
//      case object Value extends UGenGraphBuilder.Value {
//        def async = false
//        override def productPrefix = "Input.Action.Value"
//      }
      /** An "untyped" action reference, i.e. without system type and transactions revealed */
      trait Value extends UGenGraphBuilder.Value {
        def key: String
        def execute(value: Option[Any]): scala.Unit
      }
    }
    /** Specifies access to an action.
      *
      * @param name   name (key) of the attribute referring to an action
      */
    final case class Action(name: String) extends Input {
      type Key    = AttributeKey
      type Value  = Action.Value // .type

      def key: Key = AttributeKey(name)

      override def productPrefix = "Input.Action"
    }
  }
  trait Input {
    type Key   <: UGenGraphBuilder.Key
    type Value <: UGenGraphBuilder.Value

    def key: Key
  }

  trait Context[T <: Txn[T]] {

    def attr: Runner.Attr[T]

    def requestInput[Res](req: UGenGraphBuilder.Input { type Value = Res }, io: IO[T] with UGenGraphBuilder)
                         (implicit tx: T): Res
  }

  trait IO[T <: Txn[T]] {
    // def acceptedInputs: Set[String]
    def acceptedInputs: Map[Key, Map[Input, Input#Value]]

    /** Current set of used outputs (scan keys to number of channels).
      * This is guaranteed to only grow during incremental building, never shrink.
      */
    def outputs: List[OutputResult[T]]
  }

  sealed trait State[T <: Txn[T]] extends IO[T] {
    def rejectedInputs: Set[String]

    def isComplete: Boolean

    override def toString: String = {
      val acceptedS = {
        val keys: List[String] = acceptedInputs.keysIterator.map(_.toString).toList
        keys.sorted.mkString(s"accepted: [", ", ", "], ")
      }
      val rejectedS = if (isComplete) "" else {
        val keys: List[String] = rejectedInputs.iterator.map(_.toString).toList
        keys.sorted.mkString(s"rejected: [", ", ", "], ")
      }
      val outputsS = {
        val keys = outputs.map(_.key).sorted
        keys.mkString(s"outputs: [", ", ", "]")
      }
      val prefix = if (isComplete) "Complete" else "Incomplete"
      s"$prefix($acceptedS$rejectedS$outputsS)"
    }
  }

  trait Incomplete[T <: Txn[T]] extends State[T] {
    final def isComplete = false
  }

  trait Complete[T <: Txn[T]] extends State[T] {
    final def isComplete = true

    /** Structural hash, lazily calculated from `Vec[UGen]` */
    def structure: Long

    /** Runnable stream graph, lazily calculated from `Vec[UGen]` */
    def graph: UGenGraph

    final def rejectedInputs: Set[String] = Set.empty
  }

  // ---- resolve ----

  def canResolve[A](in: GE[A]): Either[String, scala.Unit] =
    in match {
      case _: Const[_]      => Right(())
      case _: Attribute[_]     => Right(())
      case UnaryOp (_, a   )  => canResolve(a)
      case BinaryOp(_, a, b)  =>
        for {
          _ <- canResolve(a).right
          _ <- canResolve(b).right
        } yield ()

//      case _: NumChannels         => Right(())
      case _                      => Left(s"Element: $in")
    }

  def canResolveSeq[A](in: GE[A]): Either[String, scala.Unit] =
    in match {
      case GESeq(elems) =>
        @tailrec
        def loop(seq: Vec[GE[A]]): Either[String, scala.Unit] =
          seq match {
            case head +: tail =>
              canResolve(head) match {
                case Right(_) => loop(tail)
                case not => not
              }

            case _ => Right(())
          }

        loop(elems)

      case GeomSeq(start, grow, length) =>
        for {
          _ <- canResolve(start ).right
          _ <- canResolve(grow  ).right
          _ <- canResolve(length).right
        } yield ()

      case ArithmSeq(start, step, length) =>
        for {
          _ <- canResolve(start ).right
          _ <- canResolve(step  ).right
          _ <- canResolve(length).right
        } yield ()

      case _  => canResolve(in)
    }

  def resolve[A](in: GE[A], builder: UGenGraphBuilder): Either[String, Const[A]] =
    in match {
      case c: Const[A] => Right(c)
      case a: Attribute[A] =>
        val req: Input.Attribute.Value = builder.requestInput(Input.Attribute(a.key))
        req.peer.fold[Either[String, Const[A]]] {
          a.default.fold[Either[String, Const[A]]] {
            Left(s"Missing attribute for key: ${a.key}")
          } {
            case Attribute.Scalar(c)                  => Right(c)
            case Attribute.Vector(cs) if cs.size == 1 => Right(cs.head)
            case other => Left(s"Cannot use multi-channel element as single constant: $other")
          }
        } {
          case i: Int     => Right(Const(i).asInstanceOf[Const[A]]) // XXX TODO ok?
          case d: Double  => Right(Const(d).asInstanceOf[Const[A]]) // XXX TODO ok?
          case n: Long    => Right(Const(n).asInstanceOf[Const[A]]) // XXX TODO ok?
          case b: Boolean => Right(Const(b /*if (b) 1 else 0*/).asInstanceOf[Const[A]]) // XXX TODO ok?
          case other      => Left(s"Cannot convert attribute value to Float: $other")
        }

      case u0: UnaryOp[_, A] =>
        def run[B](u: UnaryOp[B, A]): Either[String, Const[A]] = {
          val af = resolve(u.in, builder)
          af.right.map(x => Const(u.op.apply(x.value)))
        }
        run(u0)

      case b0: BinaryOp[_, _, A] =>
        def run[B, C](b: BinaryOp[B, C, A]): Either[String, Const[A]] =
          for {
            af <- resolve(b.a, builder).right
            bf <- resolve(b.b, builder).right
          } yield Const(b.op.apply(af.value, bf.value))

        run(b0)

      case other => Left(s"Cannot convert element to Float: $other")
    }

  def resolveSeq[A](in: GE[A], builder: UGenGraphBuilder): Either[String, Vec[Const[A]]] =
    in match {
      case GESeq(elems) =>
        @tailrec
        def loop(seq: Vec[GE[A]], out: Vec[Const[A]]): Either[String, Vec[Const[A]]] =
          seq match {
            case head +: tail =>
              resolve(head, builder) match {
                case Right(c) => loop(tail, out :+ c)
                case Left(x)  => Left(x)
              }

            case _ => Right(out)
          }

        loop(elems, Vector.empty)

      case sq @ GeomSeq(start, grow, length) =>
        for {
          startC  <- resolve(start , builder).right
          growC   <- resolve(grow  , builder).right
          lengthC <- resolve(length, builder).right
        } yield {
          val b       = Vector.newBuilder[Const[A]]
          val len     = lengthC.intValue
          b.sizeHint(lengthC.intValue)
          var i = 0
          var n = startC.value
          val g = growC .value
          val num = sq.num
          while (i < len) {
            b += Const(n)
            n = num.times(n, g) // n * g
            i += 1
          }
          b.result()
        }

      case sq @ ArithmSeq(start, step, length) =>
        for {
          startC  <- resolve(start , builder).right
          stepC   <- resolve(step  , builder).right
          lengthC <- resolve(length, builder).right
        } yield {
          val b       = Vector.newBuilder[Const[A]]
          val len     = lengthC.intValue
          b.sizeHint(lengthC.intValue)
          var i = 0
          var n = startC.value
          val g = stepC .value
          val num = sq.num
          while (i < len) {
            b += Const(n)
            n = num.plus(n, g) //  n + g
            i += 1
          }
          b.result()
        }

      case _  => resolve(in, builder).right.map(Vector(_))
    }

    /** An "untyped" output-setter reference */
  trait OutputRef /*extends OutputRefPlatform*/ {
    /** The key in the `FScape` objects `outputs` dictionary. */
    def key: String

    /** To be called by the stream node upon completion. Signals that
      * the node has completed and the passed `Output.Provider` is ready
      * to receive the `mkValue` call.
      */
    def complete(w: Output.Writer): scala.Unit

    /** Requests the stream control to create and memorize a
      * file that will be written during the rendering and should
      * be added as a resource associated with this key/reference.
      */
    def createCacheFile(): Artifact.Value
  }
  /** An extended references as returned by the completed UGB. */
  trait OutputResult[T <: Txn[T]] extends OutputRef /*with OutputResultPlatform*/ {
    def reader: Output.Reader

    /** Returns `true` after `complete` has been called, or `false` before.
      * `true` signals that `updateValue` may now be called.
      */
    def hasWriter: Boolean

    def writer: Output.Writer

    /** Issues the underlying `Output` implementation to replace its
      * value with the new updated value.
      */
    def updateValue(in: DataInput)(implicit tx: T): scala.Unit

    /** A list of cache files created during rendering for this key,
      * created via `createCacheFile()`, or `Nil` if this output did not
      * produce any additional resource files.
      */
    def cacheFiles: List[Artifact.Value]
  }

  final case class MissingIn(input: String) extends ControlThrowable {
    override def getMessage: String = input

    override def toString: String = s"UGenGraphBuilder.MissingIn: $input"
  }

  // -----------------

  private final class BuilderImpl[T <: Txn[T]](protected val context: Context[T], fscape: FScape[T])
                                              (implicit tx: T, // cursor: stm.Cursor[T],
                                               universe: Universe[T])
    extends AbstractUGenGraphBuilder[T] { builder =>

    override def auralSystem: AuralSystem = universe.auralSystem

//    override def serverOption: Option[Server] = tx match {
//      case rt: RT => auralSystem.serverOption(rt)
//      case _      => None
//    }

    // we first check for a named output, and then try to fallback to
    // an `expr.graph.Var` provided attr argument.
    protected def requestOutputImpl(reader: Output.Reader): Option[OutputResult[T]] = {
      val key = reader.key
      val outOpt = fscape.outputs.get(key)
      outOpt match {
        case Some(out: FScapeOutputImpl[T]) =>
          if (out.valueType.typeId == reader.tpe.typeId) {
            import universe.workspace
            val res = new ObjOutputRefImpl(reader, tx.newHandle(out))
            Some(res)
          } else {
            None
          }
        case _ =>
          val attrOpt = context.attr.get(key)
          attrOpt match {
            case Some(ex: ExVar.Expanded[T, _]) =>
              val res = new CtxOutputRefImpl(reader, ex)
              Some(res)
            case _ =>
              None
          }
      }
    }
  }

  private final class CtxOutputRefImpl[T <: Txn[T], A](val reader: Output.Reader,
                                                       vr: ExVar.Expanded[T, A])
//                                                      (implicit workspace: Workspace[T])
    extends AbstractOutputRef[T] {

    def updateValue(in: DataInput)(implicit tx: T): scala.Unit = {
      val value = reader.readOutputValue(in)
      vr.fromAny.fromAny(value).foreach { valueT =>
        vr.update(new ExConst.Expanded(valueT))
      }
    }
  }

  private final class ObjOutputRefImpl[T <: Txn[T]](val reader: Output.Reader,
                                                 outputH: Source[T, FScapeOutputImpl[T]])
                                                (implicit workspace: Workspace[T])
    extends AbstractOutputRef[T] {

    def updateValue(in: DataInput)(implicit tx: T): scala.Unit = {
      val obj     = reader.readOutput[T](in)
      val output  = outputH()
      output.value_=(Some(obj))
    }
  }
}
trait UGenGraphBuilder extends UGenGraph.Builder {
//  def requestAttribute(key: String): Option[Any]
//
//  def requestAction   (key: String)          : Option[ActionRef]

  def auralSystem: AuralSystem

//  def serverOption: Option[Server]

  def requestInput(input: UGenGraphBuilder.Input): input.Value

  def requestOutput(reader: Output.Reader): Option[OutputRef]
}
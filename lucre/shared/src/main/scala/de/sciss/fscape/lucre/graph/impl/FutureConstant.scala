/*
 *  FutureConstant.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre.graph.impl

import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.lucre.stream
import de.sciss.fscape.stream.{Control, StreamIn, StreamOut, Builder => SBuilder}
import de.sciss.fscape.{DataType, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource}
import de.sciss.synth.UGenSource.Vec

import scala.concurrent.Future

final case class FutureConstant[A](adj: Adjunct, fut: Control => Future[A])(implicit tpe: DataType[A])
  extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    makeUGen(Vector.empty)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    UGen.SingleOut(this, args, adjuncts = adj :: Nil, isIndividual = true)
  }

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: SBuilder): StreamOut = {
    val res = stream.FutureConstant[A](fut)
    tpe.mkStreamOut(res)
  }
}
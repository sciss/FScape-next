/*
 *  InSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.graph.ConstI
import de.sciss.fscape.lucre.{UGenGraphBuilder => UGB}
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{DataType, GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}
import de.sciss.lucre.Adjunct.{BooleanSeqTop, DoubleSeqTop, FromAny, IntSeqTop, LongSeqTop}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object InSeq extends ProductReader[InSeq[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): InSeq[_] = {
    type A = Any
    require (arity == 1 && adj == 1)
    val _key = in.readString()
    implicit val _from: FromAny[Seq[A]] = in.readAdjunct()
    new InSeq[A](_key)
  }

  private final case class WithRef[A] protected[InSeq](key: String, elems: Vec[A])(implicit tpe: DataType[A])
    extends UGenSource.SingleOut[Double] {

    override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
      makeUGen(Vector.empty)

    override protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
      UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.String(key) :: Nil)

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
      if (tpe.isInt) {
        stream.ValueSeq.int(elems.asInstanceOf[Vec[Int]].toArray) // : StreamOut
      } else if (tpe.isLong) {
        stream.ValueSeq.long(elems.asInstanceOf[Vec[Long]].toArray) // : StreamOut
      } else if (tpe.isDouble) {
        stream.ValueSeq.double(elems.asInstanceOf[Vec[Double]].toArray) // : StreamOut
      } else {
        throw new AssertionError(tpe.toString)
//        val out = stream.ValueSeq[A](elems.toArray)
//        tpe.mkStreamOut(out)
      }
    }
  }

  object Size extends ProductReader[Size[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Size[_] = {
      type A = Any
      require (arity == 1 && adj == 1)
      val _key = in.readString()
      implicit val _from: FromAny[Seq[A]] = in.readAdjunct()
      new Size[A](_key)
    }
  }
  final case class Size[A](key: String)(implicit from: FromAny[Seq[A]])
    extends GE.Lazy[Int] with ProductWithAdjuncts {

    override def adjuncts: List[LAdjunct] = from :: Nil

    override def productPrefix = s"InSeq$$Size"

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Int] = {
      val ub = UGB.get(b)
      val req = ub.requestInput(UGB.Input.Attribute(key))
      val sz: Int = req.peer.fold[Int] {
        0
      } { value =>
        val valueA: Seq[A] = from.fromAny(value).getOrElse(
          sys.error(s"Cannot use value $value at key $key with InSeq UGen")
        )
        valueA.size
      }
      ConstI(sz)
    }
  }
}
final case class InSeq[A](key: String)(implicit from: FromAny[Seq[A]])
  extends GE.Lazy[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = from :: Nil

  def size    : GE.I = InSeq.Size[A](key)
  def length  : GE.I = size

  override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    val ub = UGB.get(b)
    val req = ub.requestInput(UGB.Input.Attribute(key))
    val sq: Vec[A] = req.peer.fold[Vec[A]] {
      Vec.empty[A]
    } { value =>
      val valueA: Seq[A] = from.fromAny(value).getOrElse(
        sys.error(s"Cannot use value $value at key $key with InSeq UGen")
      )
      valueA.toIndexedSeq
    }

    from.asInstanceOf[FromAny[_]] match {
      case IntSeqTop =>
        val sqC = sq.asInstanceOf[Vec[Int]]
        InSeq.WithRef(key, sqC).asInstanceOf[GE[A]]
      case LongSeqTop =>
        val sqC = sq.asInstanceOf[Vec[Long]]
        InSeq.WithRef(key, sqC).asInstanceOf[GE[A]]
      case DoubleSeqTop =>
        val sqC = sq.asInstanceOf[Vec[Double]]
        InSeq.WithRef(key, sqC).asInstanceOf[GE[A]]
      case BooleanSeqTop =>
        val sqC = sq.asInstanceOf[Vec[Boolean]]
        val sqT = sqC.map(if(_) 1 else 0)
        InSeq.WithRef(key, sqT).asInstanceOf[GE[A]]

      case _ =>
        throw new AssertionError(from.toString)
    }
  }
}
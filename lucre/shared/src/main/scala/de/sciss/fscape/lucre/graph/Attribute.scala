/*
 *  Attribute.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenGraph.Builder
import de.sciss.fscape.graph.{Const, ConstD, UGenInGroup}
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.lucre.UGenGraphBuilder
import de.sciss.fscape.lucre.UGenGraphBuilder.Input
import de.sciss.fscape.{GE, UGenGraph, UGenInLike}
import de.sciss.lucre.Adjunct.FromAny
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}
import scala.language.implicitConversions

object Attribute extends ProductReader[Attribute[_]] {
  final class Factory(val `this`: String) extends AnyVal { me =>
    import me.{`this` => name}

    /** Creates an attribute without defaults (attribute must be present). */
    def attr[A: FromAny]: Attribute[A] = Attribute[A](key = name)
    /** Creates an attribute with defaults (attribute may be absent). */
    def attr[A: FromAny](values: Default[A]): Attribute[A] = Attribute[A](key = name, default = values)
  }

  object Default {
    implicit def fromInt    (in:     Int    ): Default[Int]     = Scalar(in)
    implicit def fromDouble (in:     Double ): Default[Double]  = Scalar(in)
    implicit def fromLong   (in:     Long   ): Default[Long]    = Scalar(in)
    implicit def fromBoolean(in:     Boolean): Default[Boolean] = Scalar(in)
    implicit def fromDoubles(in: Vec[Double]): Default[Double]  = Vector(in.map(x => ConstD(x)))
  }
  /** Magnet pattern */
  sealed trait Default[A] extends Product {
    def numChannels: Int
    // def tabulate(n: Int): Vec[Float]
    def tabulate(n: Int)(implicit b: UGenGraph.Builder): UGenInLike[A]

    def expand: UGenInLike[A]
  }

  object Scalar extends ProductReader[Scalar[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Scalar[_] = {
      require (arity == 1 && adj == 0)
      val _const = in.readProductT[Const[Any]]()
      new Scalar[Any](_const)
    }
  }
  final case class Scalar[A](const: Const[A]) extends Default[A] {
    def numChannels = 1

    def expand: UGenInLike[A] = const

    def tabulate(n: Int)(implicit b: UGenGraph.Builder): UGenInLike[A] = {
      val exp = const.expand
      if (n == 1) exp else UGenInGroup(scala.Vector.fill(n)(exp))
    }

    // serialization!
    override def productPrefix: String = s"Attribute$$Scalar"
  }

  object Vector extends ProductReader[Vector[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Vector[_] = {
      require (arity == 1 && adj == 0)
      val _values = in.readVec(in.readProductT[Const[Any]]())
      new Vector(_values)
    }
  }
  final case class Vector[A](values: Vec[Const[A]]) extends Default[A] {
    def numChannels: Int = values.size

    def expand: UGenInLike[A] = UGenInGroup(values)

    def tabulate(n: Int)(implicit b: UGenGraph.Builder): UGenInLike[A] = {
      val exp   = values.map(_.expand)
      val sz    = exp.size
      val wrap  = if (n == sz) exp else scala.Vector.tabulate(n)(idx => exp(idx % sz))
      UGenInGroup(wrap)
    }

    // serialization!
    override def productPrefix: String = s"Attribute$$Vector"
  }

  def apply[A: FromAny](key: String): Attribute[A] =
    apply(key, None, fixed = -1)

  def apply[A: FromAny](key: String, fixed: Int): Attribute[A] =
    apply(key, None, fixed = fixed)

  def apply[A: FromAny](key: String, default: Default[A]): Attribute[A] =
    mk(key, default, fixed = false)

  def apply[A: FromAny](key: String, default: Default[A], fixed: Boolean): Attribute[A] =
    mk(key, default, fixed = fixed)

  private def mk[A: FromAny](key: String, default: Default[A], fixed: Boolean): Attribute[A] =
    new Attribute(key, Some(default), fixed = if (fixed) default.numChannels else -1)

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Attribute[_] = {
    require (arity == 3)
    val _key      = in.readString()
    val _default  = in.readOption(in.readProductT[Attribute.Default[Any]]())
    val _fixed    = in.readInt()
    val _from     = if (adj == 0) LegacyAdjunct.FromAny else {
      require (adj == 1)
      in.readAdjunct[FromAny[Any]]()
    }
    new Attribute[Any](_key, _default, _fixed)(_from)
  }
}
final case class Attribute[A](key: String, default: Option[Attribute.Default[A]], fixed: Int)
                             (implicit from: FromAny[A])
  extends GE.Lazy[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = from :: Nil

  private val isLegacy = from == LegacyAdjunct.FromAny

  protected def makeUGens(implicit b: Builder): UGenInLike[A] = {
    val ub = UGenGraphBuilder.get(b)
    // val defChans  = default.fold(-1)(_.size)
    val req = ub.requestInput(Input.Attribute(key))
    val res: UGenInLike[A] = req.peer.fold[UGenInLike[A]] {
      val d = default.getOrElse(sys.error(s"Missing Attribute $key"))
      if (fixed < 0) d.expand else d.tabulate(fixed)
    } { value =>
      val valueA: Any = if (isLegacy) value else from.fromAny(value).getOrElse(
        sys.error(s"Cannot use value $value as Attribute UGen $key")
      )
      val res0: UGenInLike[A] = valueA match {
        case d: Double  => Const(d).asInstanceOf[Const[A]]  // XXX TODO ok?
        case i: Int     => Const(i).asInstanceOf[Const[A]]  // XXX TODO ok?
        case n: Long    => Const(n).asInstanceOf[Const[A]]  // XXX TODO ok?
        case b: Boolean => Const(b /*if (b) 1 else 0*/).asInstanceOf[Const[A]]  // XXX TODO ok?
        case sq: Vec[_] if sq.forall(_.isInstanceOf[Double]) =>
          val sqT = sq.asInstanceOf[Vec[Double]]
          UGenInGroup(sqT.map(Const(_).asInstanceOf[Const[A]])) // XXX TODO ok?
        case sq: Vec[_] if sq.forall(_.isInstanceOf[Int]) =>
          val sqT = sq.asInstanceOf[Vec[Int]]
          UGenInGroup(sqT.map(Const(_).asInstanceOf[Const[A]]))  // XXX TODO ok?
        case other      => sys.error(s"Cannot use value $other as Attribute UGen $key")
      }
      val sz = res0.outputs.size
      if (fixed < 0 || fixed == sz) res0 else
        UGenInGroup(Vector.tabulate(fixed)(idx => res0.outputs(idx % sz)))
    }
    res
  }
}
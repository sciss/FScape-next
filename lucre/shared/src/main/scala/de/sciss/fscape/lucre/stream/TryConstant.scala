/*
 *  TryConstant.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package lucre.stream

import akka.stream.stage.{GraphStage, GraphStageLogic, OutHandler}
import akka.stream.{Attributes, SourceShape}
import de.sciss.fscape.stream._
import de.sciss.fscape.stream.impl.NodeImpl

import scala.util.{Failure, Success, Try}

object TryConstant {
  def apply[A](elemTr: Try[Buf.E[A]])(implicit b: stream.Builder): Out[A] = {
    val stage0  = new Stage[A](elemTr, b.layer)
    val stage   = b.add(stage0)
    stage.out
  }

  private type Shp[A] = SourceShape[Buf.E[A]]

  private final class Stage[A](elemTr: Try[Buf.E[A]], layer: Layer)(implicit a: Allocator)
    extends GraphStage[Shp[A]] {

    private val name: String = elemTr match {
      case Success(e)   => e.buf(0).toString
      case Failure(ex)  => s"${ex.getClass}(${ex.getMessage})"
    }

    override def toString: String = name

    val shape: Shape = SourceShape(
      Out[A](s"$name.out")
    )

    override def initialAttributes: Attributes = Attributes.name(toString)

    def createLogic(attr: Attributes): GraphStageLogic = new Logic[A](shape, name, layer, elemTr)
  }

  private final class Logic[A](shape: Shp[A], name: String, layer: Layer, elemTr: Try[Buf.E[A]])
                                                       (implicit a: Allocator)
    extends NodeImpl(name, layer, shape) with OutHandler {

    override def toString: String = name

    def onPull(): Unit = elemTr match {
      case Success(elem) =>
        push(shape.out, elem)
        completeStage()
      case Failure(ex) =>
        notifyFail(ex)
    }

    setHandler(shape.out, this)
  }
}
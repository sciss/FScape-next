/*
 *  FutureConstant.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre
package stream

import akka.stream.stage.OutHandler
import akka.stream.{Attributes, SourceShape}
import de.sciss.fscape.DataType
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}
import de.sciss.fscape.stream.{Allocator, Buf, Builder, Control, Layer, Out}

import scala.concurrent.Future

object FutureConstant {
  def apply[A](fut: Control => Future[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer, fut)
    val stage   = b.add(stage0)
    stage.out
  }

  private final val name = "FutureConstant"

  private type Shp[A] = SourceShape[Buf.E[A]]

  private final class Stage[A](layer: Layer, fut: Control => Future[A])(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = SourceShape(
      Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](shape, layer, fut)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer, fut: Control => Future[A])
                              (implicit a: Allocator, tpe: DataType[A])
    extends NodeImpl[Shp[A]](name, layer, shape) with OutHandler { logic =>

    private[this] var result: Buf.E[A] = _
    private[this] var _stopped = false

    override def toString: String = logic.name

    override protected def launch(): Unit = {
      super.launch()
      val futV = fut(control)
      import control.config.executionContext
      futV.foreach { v =>
        async {
          if (!_stopped) {
            val b = tpe.allocBuf()
            b.buf(0)  = v
            b.size    = 1
            result    = b
            if (isAvailable(shape.out)) onPull()
          }
        }
      }
    }

    override protected def stopped(): Unit = {
      super.stopped()
      _stopped = true
    }

    def onPull(): Unit = {
      if (result != null) {
        push(shape.out, result)
        completeStage()
      }
    }
    setHandler(shape.out, this)
  }
}

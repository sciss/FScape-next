/*
 *  MkLong.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.lucre.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.lucre.UGenGraphBuilder.OutputRef
import de.sciss.fscape.lucre.{UGenGraphBuilder, stream => Lstream}
import de.sciss.fscape.stream.StreamIn
import de.sciss.fscape.{GE, Lazy, UGen, UGenGraph, UGenIn, UGenSource, stream}
import de.sciss.lucre.{Adjunct => LAdjunct}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{LongObj, Obj, ProductWithAdjuncts, Txn, Workspace}
import de.sciss.serial.{DataInput, TFormat}
import de.sciss.proc.FScape.Output

import scala.collection.immutable.{IndexedSeq => Vec}

object MkLong extends ProductReader[MkLong[_]] {
  final case class WithRef[L] protected[MkLong](peer: MkLong[L], ref: OutputRef) extends UGenSource.ZeroOut {

    protected def makeUGens(implicit b: UGenGraph.Builder): Unit =
      unwrap(this, Vector(peer.in.expand))

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): Unit = {
      UGen.ZeroOut(this, args, adjuncts = Adjunct.String(ref.key) :: Nil)
      ()
    }

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Unit = {
      val Vec(in) = args: @unchecked
      Lstream.MkLong(in = in.toLong, ref = ref)
    }

    override def productPrefix: String = s"MkLong$$WithRef"
  }

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): MkLong[_] = {
    require (arity == 2)
    val _key  = in.readString()
    val _in   = in.readGE[Any]()
    val _numL = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[Any]]()
    }
    new MkLong[Any](_key, _in)(_numL)
  }
}
final case class MkLong[L](key: String, in: GE[L])(implicit numL: NumInt[L])
  extends Lazy.Expander[Unit] with Output.Reader with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = numL :: Nil

  def tpe: Obj.Type = LongObj

  override def readOutputValue(in: DataInput): Long =
    TFormat.Long.read(in)

  def readOutput[T <: Txn[T]](in: DataInput)(implicit tx: T, workspace: Workspace[T]): Obj[T] = {
    val flat = readOutputValue(in)
    LongObj.newConst(flat)
  }

  protected def makeUGens(implicit b: UGenGraph.Builder): Unit = {
    val ub      = UGenGraphBuilder.get(b)
    val refOpt  = ub.requestOutput(this)
    val ref     = refOpt.getOrElse(sys.error(s"Missing output $key"))
    MkLong.WithRef(this, ref)
    ()
  }
}
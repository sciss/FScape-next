/*
 *  FScapePlatform.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.proc.impl

trait FScapePlatform {
  private lazy val _init: Unit = {
//    Graph.addProductReaderSq({
//      import de.sciss.fscape.graph._
//      Seq(
////        PhysicalIn,
////        PhysicalOut,
//      )
//    })
  }

  protected def initPlatform(): Unit = _init
}

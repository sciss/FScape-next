/*
 *  ModSonogramExport.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.modules

import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, _}
import de.sciss.lucre.Txn
import de.sciss.proc.{FScape, Widget}

import scala.Predef.{any2stringadd => _, intWrapper}

object ModSonogramExport extends Module {
  val name = "Sonogram Export"

  /**
    * Attributes:
    *
    * - `"in"`: audio file input
    * - `"out"`: image file output
    * - `"out-type"`: image file output type (PNG: 0, JPEG: 1)
    * - `"out-format"`: image file output sample format (Int8: 8, Int16: 1, Float: 2)
    * - `"out-quality"`: image file output JPEG quality (1-100)
    * - `"ceil-db"`: signal ceiling clipping level, in decibels
    * - `"floor-db"`: noise floor clipping level, in decibels
    * - `"freq-lo"`: lowest frequency bound in Hertz
    * - `"freq-hi"`: highest frequency bound in Hertz
    * - `"bands-per-oct"`: number of frequency bands per octave
    * - `"fft-size"`: maximum fft size (bass resolution)
    * - `"time-res-ms"`: maximum time resolution in milliseconds (treble resolution)
    * - `"rotate"`: whether to rotate (1) the sonogram counter clockwise so that time is horizontal and
    *       frequencies are vertical, or not (0)
    */
  def apply[T <: Txn[T]]()(implicit tx: T): FScape[T] = {
    import de.sciss.fscape.GE
    import de.sciss.fscape.lucre.graph.Ops._
    import de.sciss.fscape.lucre.graph._
    val f = FScape[T]()
    import de.sciss.fscape.lucre.MacroImplicits._
    f.setGraph {
      // version: 26-Oct-2023
      val fIn         = "in"
      val fOut        = "out"
      val freqLo      = "freq-lo"       .attr(32.0)
      val freqHi      = "freq-hi"       .attr(18000.0)
      val floorDb     = "floor-db"      .attr(-96.0)
      val ceilDb      = "ceil-db"       .attr( 0.0)
      val bandsPerOct = "bands-per-oct" .attr(12)
      val fileType    = "out-type"      .attr(0)
      val smpFmt      = "out-format"    .attr(1)
      val quality     = "out-quality"   .attr(90)
      val fftSize     = "fft-size"      .attr(8192)
      val timeResMS   = "time-res-ms"   .attr(20.0)
      val rotate      = "rotate"        .attr(false) // .toInt.clip(0, 1)
      val invert      = "invert"        .attr(false) // .toInt.clip(0, 1)

      val in        = AudioFileIn(fIn)
      val sr        = in.sampleRate
      val numFrames = in.length // numFrames
      val minFreqN  = freqLo / sr
      val maxFreqN  = freqHi / sr
      val numOct    = (freqHi / freqLo).log2
      val numBands  = ((numOct * bandsPerOct) + 0.5).toInt.max(1)
      val timeRes   = (timeResMS * 0.001 * sr + 0.5).toInt.max(1)
      val slid      = Sliding(in, size = fftSize, step = timeRes)
      val win0      = GenWindow.Hann(fftSize)
      val numWin    = ((numFrames + timeRes - 1).toDouble / timeRes).toInt
      val win       = RepeatWindow(win0, fftSize, num = numWin)
      val windowed0 = slid * win
      val windowed  = RotateWindow(windowed0, fftSize, fftSize >> 1)
      val fft       = Real1FFT(windowed, size = fftSize, mode = 0)

      val cq0 = ConstQ(fft, fftSize = fftSize,
        minFreqN = minFreqN, maxFreqN = maxFreqN, numBands = numBands)

      val numFramesOut = numWin * numBands
      val cq    = cq0.take(numFramesOut)
      val cqDb  = cq.ampDb

      def mkProgress[A](x: GE[A], label: String): Unit = {
        ProgressFrames(x, numFramesOut, label)
        ()
      }

      val outLo = invert.toDouble
      val outHi = (1 - invert.toInt).toDouble
      val sig0  = cqDb.linLin(floorDb, ceilDb, outLo, outHi)
      val sig1  = sig0.clip(0.0, 1.0)
      val sig   = If (rotate) Then {
        RotateFlipMatrix(sig1, rows = numWin, columns = numBands,
          mode = RotateFlipMatrix.Rot90CCW)
      } Else sig1
      val rotateI = rotate.toInt
      val r1      = 1 - rotateI
      val width   = numBands * r1 + numWin   * rotateI
      val height  = numWin   * r1 + numBands * rotateI

      ImageFileOut(fOut, sig, width = width, height = height, fileType = fileType,
        sampleFormat = smpFmt, quality = quality)

      mkProgress(sig1, "write")
    }
    f
  }

  def ui[T <: Txn[T]]()(implicit tx: T): Widget[T] = {
    import de.sciss.lucre.expr.ExImport._
    import de.sciss.lucre.expr.graph._
    import de.sciss.lucre.swing.graph._
    val w = Widget[T]()
    import de.sciss.proc.MacroImplicits._
    w.setGraph {
      // version: 22-Jul-2021

      val r     = Runner("run")
      val m     = r.messages
      m.changed.filter(m.nonEmpty) --> PrintLn(m.mkString("\n"))

      val in    = AudioFileIn()
      in.value <-> Artifact("run:in")
      val out   = ImageFileOut()
      out.value         <-> Artifact("run:out")
      out.fileType      <-> "run:out-type".attr(0)
      out.sampleFormat  <-> "run:out-format".attr(1)
      out.quality       <-> "run:out-quality".attr(90)

      val ggCeil = DoubleField()
      ggCeil.unit = "dB"
      ggCeil.min  = -240.0
      ggCeil.max  =  180.0
      ggCeil.value <-> "run:ceil-db".attr(0.0)

      val ggFloor = DoubleField()
      ggFloor.unit = "dB"
      ggFloor.min  = -240.0
      ggFloor.max  =  180.0
      ggFloor.value <-> "run:floor-db".attr(-96.0)

      val ggMaxFFTSize = ComboBox(
        (8 to 15).map(1 << _) // 256 to 32768
      )
      val aMaxFFTSize = "run:fft-size".attr[Int](8192)
//      val vMaxFFTSize: Ex[Int] = 1 << (ggMaxFFTSize.index() + 8)
//      vMaxFFTSize --> aMaxFFTSize
//      ggMaxFFTSize.index <-- (aMaxFFTSize.log2.toInt - 8).clip(0, 7)
      ggMaxFFTSize.valueOption --> aMaxFFTSize
      ggMaxFFTSize.valueOption <-- (Some(aMaxFFTSize): Ex[Option[Int]])

      val ggFreqLo     = DoubleField()
      ggFreqLo.unit    = "Hz"
      ggFreqLo.min     = 1.0
      ggFreqLo.max     = 192000.0
      ggFreqLo.value <-> "run:freq-lo".attr(32.0)

      val ggFreqHi     = DoubleField()
      ggFreqHi.unit    = "Hz"
      ggFreqHi.min     = 1.0
      ggFreqHi.max     = 192000.0
      ggFreqHi.value <-> "run:freq-hi".attr(18000.0)

      val ggBandsPerOct = IntField()
      ggBandsPerOct.min     = 1
      ggBandsPerOct.max     = 32768
      ggBandsPerOct.value <-> "run:bands-per-oct".attr(12)

      val ggTimeRes    = DoubleField()
      ggTimeRes.unit    = "ms"
      ggTimeRes.min     = 0.0
      ggTimeRes.max     = 60000.0
      ggTimeRes.value <-> "run:time-res-ms".attr(20.0)

      val ggRotate = CheckBox()
      ggRotate.selected <-> "run:rotate".attr(false)

      val ggInvert = CheckBox()
      ggInvert.selected <-> "run:invert".attr(false)

      def mkLabel(text: String) = {
        val l = Label(text)
        l.hAlign = Align.Trailing
        l
      }

      def left(c: Component*): Component = {
        val f = FlowPanel(c: _*)
        f.align = Align.Leading
        f.vGap = 0
        f
      }

      val p = GridPanel(
        mkLabel("Audio Input:" ), in,
        mkLabel("Image Output:"), out,
        Label(" "), Empty(),
        mkLabel("Lowest Frequency:"     ), left(ggFreqLo),
        mkLabel("Highest Frequency:"    ), left(ggFreqHi),
        mkLabel("Bands per Octave:"     ), left(ggBandsPerOct),
        mkLabel("Max. FFT Size:"        ), left(ggMaxFFTSize),
        mkLabel("Max. Time Resolution:" ), left(ggTimeRes),
        mkLabel("Signal Ceiling:"       ), left(ggCeil),
        mkLabel("Noise Floor:"          ), left(ggFloor),
        mkLabel("Rotate Axes:"          ), ggRotate,
        mkLabel("Invert Brightness:"    ), ggInvert,
      )
      p.columns = 2
      p.hGap    = 8
      p.compact = true

      val ggRender  = Button(" Render ")
      val ggCancel  = Button(" X ")
      ggCancel.tooltip = "Cancel Rendering"
      val pb        = ProgressBar()
      ggRender.clicked --> r.run
      ggCancel.clicked --> r.stop
      val stopped = (r.state sig_== 0) || (r.state > 3)
      ggRender.enabled = stopped
      ggCancel.enabled = !stopped
      pb.value = (r.progress * 100).toInt
      val bot = BorderPanel(
        center  = pb,
        east    = {
          val f = FlowPanel(ggCancel, ggRender)
          f.vGap = 0
          f
        }
      )
      bot.vGap = 0
      val bp = BorderPanel(
        north = p,
        south = bot
      )
      bp.vGap = 8
      bp.border = Border.Empty(8, 8, 0, 4)
      bp
    }
    w
  }
}

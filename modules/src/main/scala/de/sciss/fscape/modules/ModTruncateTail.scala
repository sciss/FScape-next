/*
 *  ModTruncateTail.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.modules

import de.sciss.lucre.Txn
import de.sciss.proc.{FScape, Widget}

import scala.Predef.{any2stringadd => _}

object ModTruncateTail extends Module {
  val name = "Truncate Tail"

  /**
    * Attributes:
    *
    * - `"in"`: audio file input
    * - `"out"`: audio file output
    * - `"out-type"`: audio file output type (AIFF: 0, Wave: 1, Wave64: 2, IRCAM: 3, NeXT: 4)
    * - `"out-format"`: audio file output sample format (Int16: 0, Int24: 1, Float: 2, Int32: 3, Double: 4, UInt8: 5, Int8: 6)
    * - `"gain-type"`: either `0` (normalized) or `1` (relative)
    * - `"thresh-db"`: threshold in decibels, below which the tail is truncated
    * - `"fade-len-ms"`: tail fade-out duration in milliseconds
    */
  def apply[T <: Txn[T]]()(implicit tx: T): FScape[T] = {
    import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, _}
    import de.sciss.fscape.lucre.graph.Ops._
    import de.sciss.fscape.lucre.graph._
    val f = FScape[T]()
    import de.sciss.fscape.lucre.MacroImplicits._
    f.setGraph {
      // version: 26-Oct-2023
      val in        = AudioFileIn("in")
      val inLen     = in.length // numFrames
      val sr        = in.sampleRate
      val fileType  = "out-type"    .attr(0)
      val smpFmt    = "out-format"  .attr(2)
      val thresh    = "thresh-db"   .attr(-60.0).dbAmp
      val fadeLenMs = "fade-len-ms" .attr(100.0).max(0.0)
      val fadeLen   = (fadeLenMs * 0.001 * sr).toInt
      val rev       = Slices(in, inLen ++ 0L) // reverse trick
      val amp       = OnePole(rev.abs, 0.995) * 2
      val silent    = amp.takeWhile(amp < thresh)
      val tailLen   = Length(silent)
      // (tailLen / sr).poll("tail len [s]")
      val outLen  = (inLen - tailLen).max(0L)
      val trunc   = AudioFileIn("in").take(outLen)
      val fadeLenT = fadeLen.toLong.min(outLen)
      val env     = DEnvGen(
        levels  = ValueDoubleSeq(1.0, 1.0, 0.0),
        lengths = (outLen - fadeLenT) ++ fadeLenT
      )
      val sig     = trunc * env

      val written = AudioFileOut("out", sig, fileType = fileType,
        sampleFormat = smpFmt, sampleRate = sr)
      ProgressFrames(written, outLen)
      ()
    }
    f
  }

  def ui[T <: Txn[T]]()(implicit tx: T): Widget[T] = {
    import de.sciss.lucre.expr.ExImport._
    import de.sciss.lucre.expr.graph._
    import de.sciss.lucre.swing.graph._
    val w = Widget[T]()
    import de.sciss.proc.MacroImplicits._
    w.setGraph {
      // version: 13-Oct-2021
      val r     = Runner("run")
      val m     = r.messages
      m.changed.filter(m.nonEmpty) --> PrintLn(m.mkString("\n"))

      val in    = AudioFileIn()
      in.value <-> Artifact("run:in")
      val out   = AudioFileOut()
      out.value         <-> Artifact("run:out")
      out.fileType      <-> "run:out-type".attr(0)
      out.sampleFormat  <-> "run:out-format".attr(2)

      val ggThresh     = DoubleField()
      ggThresh.unit    = "dB FS"
      ggThresh.min     = -320.0
      ggThresh.max     =    0.0
      ggThresh.value <-> "run:thresh-db".attr(-60.0)

      val ggFadeLen     = DoubleField()
      ggFadeLen.unit    = "ms"
      ggFadeLen.min     = 0.0
      ggFadeLen.max     = 60 * 1000.0
      ggFadeLen.value <-> "run:fade-len-ms".attr(100.0)

      def mkLabel(text: String) = {
        val l = Label(text)
        l.hAlign = Align.Trailing
        l
      }

      def left(c: Component*): Component = {
        val f = FlowPanel(c: _*)
        f.align = Align.Leading
        f.vGap = 0
        f
      }

      val p = GridPanel(
        mkLabel("Input:" ), in,
        mkLabel("Output:"), out,
        Label(" "), Empty(),
        mkLabel("Threshold:"), left(ggThresh),
        mkLabel("Fade Out Length:"), left(ggFadeLen),
      )
      p.columns = 2
      p.hGap    = 8
      p.compact = true

      val ggRender  = Button(" Render ")
      val ggCancel  = Button(" X ")
      ggCancel.tooltip = "Cancel Rendering"
      val pb        = ProgressBar()
      ggRender.clicked --> r.run
      ggCancel.clicked --> r.stop
      val stopped = (r.state sig_== 0) || (r.state > 3)
      ggRender.enabled = stopped
      ggCancel.enabled = !stopped
      pb.value = (r.progress * 100).toInt
      val bot = BorderPanel(
        center  = pb,
        east    = {
          val f = FlowPanel(ggCancel, ggRender)
          f.vGap = 0
          f
        }
      )
      bot.vGap = 0
      val bp = BorderPanel(
        north = p,
        south = bot
      )
      bp.vGap = 8
      bp.border = Border.Empty(8, 8, 0, 4)
      bp
    }
    w
  }
}

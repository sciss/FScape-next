# Truncate Tail

Truncates the silent tail part of an audio file, based on an amplitude threshold.
It looks from the file's end backwards to determine the earliest point at which
the amplitude rises above the given threshold, and then drops this last part.
To avoid a click at the end, an additional linear fade-out is applied to the truncated
end, the duration of which can be specified.

# Sonogram Export

Converts a sound file to a sonogram image file.  
The sonogram is created using a Constant-Q transform, resulting in a logarithmically spaced frequency axis. 
The sonogram is written as a grayscale image.

## Parameters

_Audio Input:_ Sound file to analyse.

_Image Output:_ Sonogram image file to produce.

_Lowest Frequency:_ The lowest band's center frequency to represent in the sonogram

_Highest Frequency:_ The highest band's center frequency to represent in the sonogram

_Bands per Octave:_ The number of bands (pixels) per octave

_Max. FFT Size:_ Specifies the maximum frequency resolution of bass frequencies.
The smaller this size, the less temporal smearing occurs in the bass frequencies, but the worse the frequency
resolution in the bass register.

_Max. Temporal Resolution:_ Specifies how fine-grained the high frequencies are resolved in time.
In the base frequencies, the actual temporal resolution is a trade-off with the steepness of the frequency filters,
and depends on the maximum FFT size. The temporal resolution corresponds to the stepping size across the windowed
input.

_Signal Ceiling:_ The spectral energy level represented as maximum brightness (white). If a frequency band is
stronger than this level, it will be clipped to white. Lowering the level means increasing brightness of the peaks.

_Noise Floor:_ The spectral energy level represented as minimum brightness (black). If a frequency band is
weaker than this level, it will be clipped to black. Lowering the level means increasing brightness of the 
background noise.

_Rotate Axes:_ When selected, the sonogram is written with time corresponding to the horizontal axis (left to right),
and frequency corresponding to the vertical axis (bottom to top). If not selected, frequency runs left to right
on the horizontal axis, and time passes from top to bottom. This is default, because it is more efficient, whereas
rotation requires that the entire sonogram be held in memory prior to rotation.

_Invert Brightness:_ When selected, the lowest energy are represented by white, and the highest energy is represented 
by black.

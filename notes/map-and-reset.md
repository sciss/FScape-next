# Map and Reset, 09-May-2022

This possibility of resetting the internal state of streams, in order to create sequential operations,
has been mentioned in the paper 'A Pattern System for Sound Processes':

> ... the notion of resetable pattern sub-programs may carry a profound opportunity for understanding and 
integrating resetable programs in the FScape formalism as well, which is currently too closely tied to the
constraints of real-time UGens after which it is modelled.

This would be intimately tied to a notion of nested or hierarchical signals, as would occur along with operations
such as `map` or `flatMap`. It would thus introduce a notion of time "windows", and probably render a number of
UGens, such as `ReverseWindow`, `WindowApply`, ... superfluous. It would also open up for the opportunity to
"batch process" within a single FScape program ("mapping a sequence of audio input files").

Think of the following pattern example from said paper:

```
val lPat = Pat.loop()((8 to 12).mirror)
val rPat = Pat.loop()((5 to 8).mirror) / 25.0
val xs = for {
  len     <- lPat.bubble
  rests   <- rPat.bubble
  offset  <- ArithmSeq(0, 1).bubble
  cantus0 <- Brown(-6, 6, 3).grouped(len)
} yield ???
```

It should almost be possible to interpret this code as a valid FScape code. (Perhaps `Sig.loop` or `GE.loop` 
instead of `Pat.loop`)

Since the `Brown` is defined inside of `len` and `rests` here, it should be reset for each iteration of the outer
loops. If it were defined along `lPat` and `rPat`, it should _not_ be reset.

## Examples

Since it must thus be possible to reset elements that are pure generators, we cannot rely on a "stop bit" in the
input buffers.

```
val segm = Sig.loop(4) {
  val b = Brown(-1d, 1d, 1.0e-5)
  b.take(40000)
}
```

Here, `segm` would probably be a "flat" stream of 160000 sample frames, which "jump" every 40000 frames.

Questions:

- would it be possible for "some" functions to provide signal lengths "in advance" (i.e. chunks with pre-known lengths)
- how could that be used by sink UGens? Would it be useful at all?
- for example, say we introduce a GE operation `.reverse`, kind of making `ReverseWindow` superfluous. Would it help
  that function to distinguish between known and unknown "next segment length"? I guess it will just duplicate work,
  because certainly there will be cases where the segment lengths cannot be predetermined. So the replacement for
  `ReverseWindow` would have to "intelligently buffer" the incoming data, possibly switching to secondary memory
  ad-hoc when the internal buffer grows beyond a given threshold. The big advantage for the use site in any case is
  we can now simply write `Real1FFT(...).map(_.reverse)`. If we were to implement the optional "known segment lengths",
  they would go out now from the FFT, possibly making the `reverse` easier and more efficient. I guess we could add
  this kind of optimisation at a later point if deemed useful.

The last example puts the question whether `Real1FFT` should now automatically produce a `GE[Seq[Double]]` or
remain with a flat representation `GE.D`? If this were a project from scratch, the nested version looks nicer, but
can we make this backward compatible (legacy-deserializable) at all?

We could even ask if `Real1FFT` needs a length argument at all now, since we could think of
`sig.sliding(...).map(_.real1fft)`...

Which elements would potentially produce a 'nested' and/or 'segmented' output?

What about those that would logically require a 'windowed' input? Say `ConstQ`, it takes input of one window size,
and produces output of another window size. That, should it "increase" the nesting level at all?

It almost sounds as if we should move that `ConstQ` to some `LegacyConstQ` position, and introduce it newly so that
it operates on a "segmented input", possibly without explicit input window length argument. Let's see an example
where `ConstQ` is used.

Before:

```
// ModSonagramExport
val fIn         = "in"
val fOut        = "out"
val freqLo      = "freq-lo"       .attr(32.0)
val freqHi      = "freq-hi"       .attr(18000.0)
val floorDb     = "floor-db"      .attr(-96.0)
val ceilDb      = "ceil-db"       .attr( 0.0)
val bandsPerOct = "bands-per-oct" .attr(12)
val fileType    = "out-type"      .attr(0)
val smpFmt      = "out-format"    .attr(1)
val quality     = "out-quality"   .attr(90)
val fftSize     = "fft-size"      .attr(8192)
val timeResMS   = "time-res-ms"   .attr(20.0)
val rotate      = "rotate"        .attr(false)
val invert      = "invert"        .attr(false)

val in        = AudioFileIn(fIn)
val sr        = in.sampleRate
val numFrames = in.numFrames
val minFreqN  = freqLo / sr
val maxFreqN  = freqHi / sr
val numOct    = (freqHi / freqLo).log2
val numBands  = ((numOct * bandsPerOct) + 0.5).toInt.max(1)
val timeRes   = (timeResMS * 0.001 * sr + 0.5).toInt.max(1)
val slid      = Sliding(in, size = fftSize, step = timeRes)
val win0      = GenWindow.Hann(fftSize)
val numWin    = ((numFrames + timeRes - 1).toDouble / timeRes).toInt
val win       = RepeatWindow(win0, fftSize, num = numWin)
val windowed0 = slid * win
val windowed  = RotateWindow(windowed0, fftSize, fftSize >> 1)
val fft       = Real1FFT(windowed, size = fftSize, mode = 0)

val cq0 = ConstQ(fft, fftSize = fftSize,
  minFreqN = minFreqN, maxFreqN = maxFreqN, numBands = numBands)

val numFramesOut = numWin * numBands
val cq    = cq0.take(numFramesOut)
val cqDb  = cq.ampDb

val outLo = invert.toDouble
val outHi = (1 - invert.toInt).toDouble
val sig0  = cqDb.linLin(floorDb, ceilDb, outLo, outHi)
val sig1  = sig0.clip(0.0, 1.0)
val sig   = If (rotate) Then {
  RotateFlipMatrix(sig1, rows = numWin, columns = numBands,
    mode = RotateFlipMatrix.Rot90CCW)
} Else sig1
val rotateI = rotate.toInt
val r1      = 1 - rotateI
val width   = numBands * r1 + numWin   * rotateI
val height  = numWin   * r1 + numBands * rotateI

ImageFileOut(fOut, sig, width = width, height = height, fileType = fileType,
  sampleFormat = smpFmt, quality = quality)
```

Hypothetical new form:


```
val slid: GE[Seq[Double]] = Sliding(in, size = fftSize, step = timeRes)
val numWin    = ((numFrames + timeRes - 1).toDouble / timeRes).toInt
val windowed0 = slid.map { frag =>
  val win = GenWindow.Hann(fftSize)  // assume it doesn't repeat now!
  frag * win
}
val windowed  = RotateWindow(windowed0, fftSize >> 1)
val fft: GE[Seq[Double]] = windowed.flatMap { frag => Real1FFT(frag, mode = 0) } // or GE[Seq[Complex]]

val cq = fft.flatMap { frag => ConstQ(frag, minFreqN = minFreqN, maxFreqN = maxFreqN, numBands = numBands) }
val cqDb  = cq.map(_.ampDb) // this looks odd

val outLo = invert.toDouble
val outHi = (1 - invert.toInt).toDouble
val sig0  = cqDb.map(_.linLin(floorDb, ceilDb, outLo, outHi)) // this looks odd
val sig1  = sig0.map(_.clip(0.0, 1.0))
val sig   = If (rotate) Then {
  RotateFlipMatrix(sig1.flatten, rows = numWin, columns = numBands,
    mode = RotateFlipMatrix.Rot90CCW).flatten
} Else sig1.flatten
val rotateI = rotate.toInt
val r1      = 1 - rotateI
val width   = numBands * r1 + numWin   * rotateI
val height  = numWin   * r1 + numBands * rotateI

ImageFileOut(fOut, sig, width = width, height = height, fileType = fileType,
  sampleFormat = smpFmt, quality = quality)
```

This looks overly complicated. It opens all sorts of ambiguities in the decisions where nested structures are
introduced and where not. It especially seems odd having to map the unary and binary operators across nested
structures. Or:


```
val slid   = in.sliding(size = fftSize, step = timeRes)
val sig1   = slid.flatMap /* .map */ { frag =>
  val win       = GenWindow.Hann(fftSize)
  val windowed0 = frag * win
  val windowed  = Rotate(windowed0, fftSize >> 1)  // ?
  val fft       = Real1FFT(windowed, mode = 0)
  val cq        = ConstQ(fft, minFreqN = minFreqN, maxFreqN = maxFreqN, numBands = numBands)
  val cqDb      = cq.ampDb
  val sig0      = cqDb.linLin(floorDb, ceilDb, outLo, outHi)
  sig0.clip(0.0, 1.0)
}
...
```

This looks "ok". Perhaps questionable is the `Rotate`. Perhaps it requires that the input is segmented, e.g.
`case class Rotate[A](in: GE.Seq[A], ...) extends GE.Seq[A]`?

Do we run risk now of exploding the memory, e.g. forgetting that `ConstQ` needs a segment 
termination? We could "tag" signals als of indefinite length, and throw a runtime error if encountered. E.g.
`ConstQ(SinOsc(...))` could result in an immediate runtime error? Then are there UGens where it is unclear whether
they terminate or not (`.takeWhile`)? I guess this "unknown" state would _not_ cause a runtime error but eventually 
cause the out-of-memory error.

---

About known segment sizes of the above example: `frag` in `slid.flatMap` does have predetermined sizes, so will
`win`; so will `windowed0`, `windowed`, `fft`, `cq`, `cqDb`, `sig0` and `_.clip`. Where are lengths "read"? In
`Rotate` (even negative rotation must be taken modulus window length I think, so without knowing the size, the thing
cannot start producing output). So we need a sort of new buffer abstraction that will either

- grow in memory while the input length is unknown. when a threshold is passed, it will switch to secondary memory
- directly allocate in memory or on disk if the length is known in advance.

Dito for `Real1FFT` and `ConstQ`. Whereas `ampDb`, `linLin` and `clip` do not care about the lengths. That also means
they should "forward" the length information?

A problem with secondary memory buffering is that we cannot directly use `AudioFile` for it unless we restrict to
numeric types. So we always need a serializer on the `DataType`.

## Communicating Segment Ends

In the most unfortunate case, a source has just pushed a `Buf`, and only afterwards determines that a segment has
ended. So it cannot just "tag" the last buffer as 'tail'. It could send a successive empty buffer marked as tail,
though.

## Multi-dimensional data (nested segments)

Say two-dimensional packets: a sequence of images. `ImageFileSeqIn`. Say that gives a `GE[Seq[Seq[Double]]]`. If we
don't want that, we should be able to say `.flatten`, and that should be "cheap" operation, ideally not requiring
another graph logic stage at all, or at least one that quickly passes on buffers and re-tags them.

If we look at other data processing frameworks, you usually combine a flat backend array with a sort of 'shape'
attribute. So `WhiteNoise` would have shape `[?]`. If we call `.take(10)` on that, the `Take` UGen would push out
the first output buffer with shape `[10]`, thus indicating the known length. Do we want to model the rank in the type
system or not? Do we want `GE[Seq[X]]` at all? Or: would there be a scenario where the rank couldn't be determined 
at compile time?

## How are 'resets' issued

This is slightly different from the question of communicating segment lengths and/or shapes.

Take a simple UGen, binary operator. This assumes a one dimensional input and one dimensional output shape.
So the UGen terminates or resets when the input terminates or resets. If the buffer is "tagged", then we need to
make sure we tag the output buffer, and if an empty tagged input buffer arrives, also emit an empty tagged output
buffer. The alternative would be that tags arrive _between_ buffers are dedicated signals. Then those special tag
symbols would be forwarded. This would be more similar to the current approach, because we already use Akka's own
stream termination mechanism.

/*
 *  Widen.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import de.sciss.fscape.Ops.geOps1

trait WidenMidPriority /*extends WidenLowPriority*/ {
  implicit object intDoubleDouble extends Widen2[Int, Double, Double] {
    override def widen1(a: GE[Int   ]): GE[Double] = a.toDouble
    override def widen2(a: GE[Double]): GE[Double] = a
  }

  implicit object longDoubleDouble extends Widen2[Long, Double, Double] {
    override def widen1(a: GE[Long  ]): GE[Double] = a.toDouble
    override def widen2(a: GE[Double]): GE[Double] = a
  }
}

object Widen extends WidenMidPriority {
  implicit def identity[A]: Widen2[A, A, A] = anyWiden.asInstanceOf[Identity[A]]

  private val anyWiden = new Identity[Any]

  private final class Identity[A] extends Widen2[A, A, A] {
    def widen1(a: GE[A]): GE[A] = a
    def widen2(a: GE[A]): GE[A] = a
  }
}
trait Widen[A1, A] /*extends Adjunct*/ {
  def widen1(a: GE[A1]): GE[A]
}

object Widen2 {
  implicit object doubleIntDouble extends Widen2[Double, Int, Double] {
    override def widen1(a: GE[Double]): GE[Double] = a
    override def widen2(a: GE[Int   ]): GE[Double] = a.toDouble
  }
  implicit object doubleLongDouble extends Widen2[Double, Long, Double] {
    override def widen1(a: GE[Double]): GE[Double] = a
    override def widen2(a: GE[Long  ]): GE[Double] = a.toDouble
  }
  implicit object longIntLong extends Widen2[Long, Int, Long] {
    override def widen1(a: GE[Long]): GE[Long] = a
    override def widen2(a: GE[Int ]): GE[Long] = a.toLong
  }
}

trait Widen2[A1, A2, A] extends Widen[A1, A] {
  def widen1(a: GE[A1]): GE[A]
  def widen2(a: GE[A2]): GE[A]
}
/*
 *  LinExp.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}

import scala.collection.immutable.{IndexedSeq => Vec}

object LinExp extends ProductReader[LinExp] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): LinExp = {
    require (arity == 5 && adj == 0)
    val _in       = in.readGE_D()
    val _inLow    = in.readGE_D()
    val _inHigh   = in.readGE_D()
    val _outLow   = in.readGE_D()
    val _outHigh  = in.readGE_D()
    new LinExp(_in, _inLow, _inHigh, _outLow, _outHigh)
  }
}
final case class LinExp(in: GE.D, inLow: GE.D, inHigh: GE.D, outLow: GE.D, outHigh: GE.D)
  extends UGenSource.SingleOut[Double] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, inLow.expand, inHigh.expand, outLow.expand, outHigh.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, inLow, inHigh, outLow, outHigh) = args: @unchecked
    stream.LinExp(
      in      = in      .toDouble,
      inLow   = inLow   .toDouble,
      inHigh  = inHigh  .toDouble,
      outLow  = outLow  .toDouble,
      outHigh = outHigh .toDouble,
    )
  }
}

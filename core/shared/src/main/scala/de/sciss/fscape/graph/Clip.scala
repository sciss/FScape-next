/*
 *  Clip.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Clip extends ProductReader[Clip[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Clip[_] = {
    require (arity == 3)
    val _in = in.readGE[Any]()
    val _lo = in.readGE[Any]()
    val _hi = in.readGE[Any]()
    val _num = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new Clip[Any](_in, _lo, _hi)(_num)
  }
}
final case class Clip[A](in: GE[A], lo: GE[A], hi: GE[A])(implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, lo.expand, hi.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, lo, hi) = args: @unchecked
    if (in.isInt && lo.isInt && hi.isInt) {
      stream.Clip.int   (in = in.toInt   , lo = lo.toInt   , hi = hi.toInt   )
    } else if ((in.isInt || in.isLong) && (lo.isInt || lo.isLong) && (hi.isInt || hi.isLong)) {
      stream.Clip.long  (in = in.toLong  , lo = lo.toLong  , hi = hi.toLong  )
    } else {
      stream.Clip.double(in = in.toDouble, lo = lo.toDouble, hi = hi.toDouble)
    }
  }
}
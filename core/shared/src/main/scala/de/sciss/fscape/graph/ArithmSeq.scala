/*
 *  ArithmSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{Num, NumInt}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object ArithmSeq extends ProductReader[ArithmSeq[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ArithmSeq[_, _] = {
    require (arity == 3)
    val _start  = in.readGE[Any]()
    val _step   = in.readGE[Any]()
    val _length = in.readGE[Any]()
    // legacy serialization is without adjuncts
    require (adj == 2 || adj == 0)
    val _numA   = if (adj == 0) LegacyAdjunct.Num     else in.readAdjunct[Num   [Any]]()
    val _numB   = if (adj == 0) LegacyAdjunct.NumInt  else in.readAdjunct[NumInt[Any]]()
    new ArithmSeq[Any, Any](_start, _step, _length)(_numA, _numB)
  }
}
/** A UGen that produces an arithmetic sequence of values.
  * If both `start` and `step` are of integer type (`Int` or `Long`),
  * this produces an integer output, otherwise it produces a floating point output.
  *
  * E.g. `ArithmSeq(start = 1, step = 2)` will produce a series `1, 3, 5, 7, ...`.
  *
  * @param start    the first output element
  * @param step     the amount added to each successive element
  * @param length   the number of elements to output
  */
final case class ArithmSeq[A, L](start: GE[A] = 0, step: GE[A] = 1, length: GE[L] = Long.MaxValue)
                                (implicit val num: Num[A], val numL: NumInt[L])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(start.expand, step.expand, length.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(start, step, length) = args: @unchecked

    if (start.isInt && step.isInt) {
      stream.ArithmSeq[Int   ](start = start.toInt   , step = step.toInt   , length = length.toLong)
    } else if ((start.isInt || start.isLong) && (step.isInt || step.isLong)) {
      stream.ArithmSeq[Long  ](start = start.toLong  , step = step.toLong  , length = length.toLong)
    } else {
      stream.ArithmSeq[Double](start = start.toDouble, step = step.toDouble, length = length.toLong)
    }
  }
}
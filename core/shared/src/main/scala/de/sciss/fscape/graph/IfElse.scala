/*
 *  IfElse.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape
import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{Out, OutD, OutI, OutL, StreamIn, StreamOut}
import de.sciss.fscape.{DataType, Graph, Lazy, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream, GE => _GE}

import scala.annotation.tailrec
import scala.collection.immutable.{IndexedSeq => Vec}

/** Beginning of a conditional block.
  *
  * @param cond   the condition, which is treated as a boolean with zero being treated as `false` and
  *               non-zero as `true`. Note that multi-channel expansion does not apply here, because
  *               the branches may have side-effects which are not supposed to be repeated across
  *               channels. Instead, if `cond` expands to multiple channels, they are combined using
  *               logical `|` (OR).
  *
  * @see  [[IfThen]]
  */
final case class If(cond: _GE.B) {
  def Then [A](branch: => A): IfThen[A] = {
    var res: A = null.asInstanceOf[A]
    val g = Graph {
      res = branch
    }
    IfThen(cond, g, res)
  }
}

object Then {
  private[fscape] case class UnitCase (cond: _GE.B, branchLayer: Int)
  private[fscape] case class GECase[A](cond: UGenIn[Boolean], branchLayer: Int, branchOut: Vec[UGenIn[A]])

  private[fscape] def gatherUnit(e: Then[Any])(implicit builder: UGenGraph.Builder): List[UnitCase] = {
    @tailrec
    def loop(t: Then[Any], res: List[UnitCase]): List[UnitCase] = {
      val layer = builder.allocLayer()
      builder.withLayer(layer)(builder.expandNested(t.branch))
      val res1  = UnitCase(t.cond, layer) :: res
      t match {
        case hd: ElseOrElseIfThen[Any] => loop(hd.pred, res1)
        case _ => res1
      }
    }

    loop(e, Nil)
  }

  private[fscape] def gatherGE[A](e: Then[_GE[A]])(implicit builder: UGenGraph.Builder): List[GECase[A]] = {
    @tailrec
    def loop(t: Then[_GE[A]], res: List[GECase[A]]): List[GECase[A]] = {
      val layer = builder.allocLayer()
      builder.withLayer(layer)(builder.expandNested(t.branch))
      val condE       = t.cond    .expand
      val branchOutE  = t.result  .expand
      val condI       = condE.unbubble match {
        case u: UGenIn[Boolean] => u
        case g: UGenInGroup[Boolean] =>
          import de.sciss.fscape.Ops._
          g.flatOutputs.foldLeft(false: UGenIn[Boolean])((a, b) => (a || b).expand.flatOutputs.head)
      }
      val branchOutI  = branchOutE.flatOutputs  // XXX TODO --- what else could we do?
      val res1        = GECase(condI, layer, branchOutI) :: res
      t match {
        case hd: ElseOrElseIfThen[_GE[A]] => loop(hd.pred, res1)
        case _ => res1
      }
    }

    loop(e, Nil)
  }

  case class SourceUnit protected[graph](cases: List[UnitCase]) extends UGenSource.ZeroOut {
    protected def makeUGens(implicit b: UGenGraph.Builder): Unit =
      unwrap(this, cases.iterator.map(_.cond.expand).toIndexedSeq)

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): Unit = {
      UGen.ZeroOut(this, args, adjuncts = cases.map(c => Adjunct.Int(c.branchLayer)))
      ()
    }

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Unit = {
      val cond = (args.iterator zip cases.iterator).map { case (cond0, UnitCase(_, bl)) =>
        (cond0.toInt, bl)
      } .toVector
      stream.IfThenUnit(cond)
    }
  }

  case class SourceGE[A] protected[graph](cases: List[GECase[A]]) extends UGenSource.MultiOut[A] {
    private[this] val numCases = cases.size

    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
      val branchSizes = cases.map(_.branchOut.size)
      val isEmpty     = branchSizes.isEmpty || branchSizes.contains(0)
      val numOutputs  = if (isEmpty) 0 else branchSizes.max
      val args = cases.iterator.flatMap {
        case GECase(cond, _, branchOut) =>
          val branchOutExp = Vector.tabulate(numOutputs) { ch =>
            branchOut(ch % branchOut.size)
          }
          cond +: branchOutExp
      } .toIndexedSeq
      makeUGen(args)
    }

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] = {
      val numOutputs = args.size / numCases - 1 // one conditional, N branch-outs
      UGen.MultiOut(this, args, numOutputs = numOutputs, adjuncts = cases.map(c => Adjunct.Int(c.branchLayer)))
    }

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Vec[StreamOut] = {
      val sliceSize   = args.size / numCases
      val numOutputs  = sliceSize - 1 // one conditional, N branch-outs
      val condT       = Vector.tabulate(numCases)(bi => args(bi * sliceSize).toInt)
      val outs        = Vector.tabulate(numCases)(bi => args.slice(bi * sliceSize + 1, (bi + 1) * sliceSize))
      val outsF       = outs.flatten
      val layers      = cases .map(_.branchLayer)

      if (outsF.forall(_.isInt)) {
        val outsT: Seq[Vec[OutI]] = outs.map(_.map(_.toInt))
        val cases = (condT, layers, outsT).zipped.toList
        stream.IfThenGE[Int](numOutputs, cases)

      } else if (outsF.forall(o => o.isInt || o.isLong)) {
        val outsT: Seq[Vec[OutL]] = outs.map(_.map(_.toLong))
        val cases = (condT, layers, outsT).zipped.toList
        stream.IfThenGE[Long](numOutputs, cases)

      } else if (outsF.exists(_.isDouble)) {
        val outsT: Seq[Vec[OutD]] = outs.map(_.map(_.toDouble))
        val cases = (condT, layers, outsT).zipped.toList
        stream.IfThenGE[Double](numOutputs, cases)

      } else {
        val outsT: Seq[Vec[Out[A]]] = outs.map(_.map(_.cast[A].toElem))
        val cases = (condT, layers, outsT).zipped.toList
        val tpe: DataType[A] = outs.head.head.cast[A].tpe
        val out = stream.IfThenGE[A](numOutputs, cases)
        out.map(tpe.mkStreamOut)
      }
    }
  }
}
sealed trait Then[+A] extends Lazy {
  def cond  : _GE.B
  def branch: Graph
  def result: A
}

sealed trait IfOrElseIfThen[+A] extends Then[A] {
  import fscape.graph.{Else => _Else}
  def Else [B >: A, Res](branch: => B)(implicit result: _Else.Result[B, Res]): Res = {
    Graph.builder.removeLazy(this)
    result.make(this, branch)
  }
}

sealed trait IfThenLike[+A] extends IfOrElseIfThen[A] with Lazy.Expander[Unit] {
  final def ElseIf (cond: _GE.B): ElseIf[A] = {
    Graph.builder.removeLazy(this)
    new ElseIf(this, cond)
  }

  final protected def makeUGens(implicit b: UGenGraph.Builder): Unit = {
    val cases = Then.gatherUnit(this)
    // println(s"cases = $cases")
    Then.SourceUnit(cases)
    ()
  }
}

object IfThen extends ProductReader[IfThen[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): IfThen[_] = {
    require (arity == 3 && adj == 0)
    val _cond   = in.readGE_B()
    val _branch = in.readGraph()
    val _result = in.readElem()
    new IfThen(_cond, _branch, _result)
  }
}
/** A side effecting conditional block. To turn it into a full `if-then-else` construction,
  * call `Else` or `ElseIf`.
  *
  * @see  [[Else]]
  * @see  [[ElseIf]]
  */
final case class IfThen[+A](cond: _GE.B, branch: Graph, result: A) extends IfThenLike[A]

final case class ElseIf[+A](pred: IfOrElseIfThen[A], cond: _GE.B) {
  def Then [B >: A](branch: => B): ElseIfThen[B] = {
    var res: B = null.asInstanceOf[B]
    val g = Graph {
      res = branch
    }
    ElseIfThen[B](pred, cond, g, res)
  }
}

sealed trait ElseOrElseIfThen[+A] extends Then[A] {
  def pred: IfOrElseIfThen[A]
}

object ElseIfThen extends ProductReader[ElseIfThen[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ElseIfThen[_] = {
    require (arity == 4 && adj == 0)
    val _pred   = in.readProductT[IfOrElseIfThen[Any]]()
    val _cond   = in.readGE_B()
    val _branch = in.readGraph()
    val _result = in.readElem()
    new ElseIfThen[Any](_pred, _cond, _branch, _result)
  }
}
final case class ElseIfThen[+A](pred: IfOrElseIfThen[A], cond: _GE.B, branch: Graph, result: A)
  extends IfThenLike[A] with ElseOrElseIfThen[A]

object Else {
  object Result extends LowPri {
    implicit def GE[A]: Else.GE[A] = Else.GE()
  }
  sealed trait Result[-A, Res] {
    def make(pred: IfOrElseIfThen[A], branch: => A): Res
  }

  case class GE[A]() extends Result[_GE[A], ElseGE[A]] {
    def make(pred: IfOrElseIfThen[_GE[A]], branch: => _GE[A]): ElseGE[A] = {
      var res: _GE[A] = null
      val g = Graph {
        res = branch
      }
      ElseGE[A](pred, g, res)
    }
  }

  final class Unit[A] extends Result[A, ElseUnit] {
    def make(pred: IfOrElseIfThen[A], branch: => A): ElseUnit =  {
      val g = Graph {
        branch
      }
      ElseUnit(pred, g)
    }
  }

  trait LowPri {
    implicit final def Unit[A]: Unit[A] = instance.asInstanceOf[Unit[A]]
    private final val instance = new Unit[Any]
  }
}

sealed trait ElseLike[+A] extends ElseOrElseIfThen[A] {
  def cond: _GE.B = true // ConstantI.C1
}

object ElseUnit extends ProductReader[ElseUnit] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ElseUnit = {
    require (arity == 2 && adj == 0)
    val _pred   = in.readProductT[IfOrElseIfThen[Any]]()
    val _branch = in.readGraph()
    new ElseUnit(_pred, _branch)
  }
}
final case class ElseUnit(pred: IfOrElseIfThen[Any], branch: Graph)
  extends ElseLike[Any] with Lazy.Expander[Unit] {

  def result: Any = ()

  protected def makeUGens(implicit b: UGenGraph.Builder): Unit = {
    val cases = Then.gatherUnit(this)
    // println(s"cases = $cases")
    Then.SourceUnit(cases)
    ()
  }
}

object ElseGE extends ProductReader[ElseGE[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ElseGE[_] = {
    require (arity == 3 && adj == 0)
    val _pred   = in.readProductT[IfOrElseIfThen[_GE[Any]]]()
    val _branch = in.readGraph()
    val _result = in.readGE[Any]()
    new ElseGE[Any](_pred, _branch, _result)
  }
}
final case class ElseGE[A](pred: IfOrElseIfThen[_GE[A]], branch: Graph, result: _GE[A])
  extends ElseLike[_GE[A]] with _GE.Lazy[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    val cases = Then.gatherGE(this)
    // println(s"cases = $cases")
    Then.SourceGE(cases)
  }
}
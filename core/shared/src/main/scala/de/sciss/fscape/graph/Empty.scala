/*
 *  Empty.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}

import scala.collection.immutable.{IndexedSeq => Vec}

object Empty extends ProductReader[Empty[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Empty[_] = {
    require (arity == 0 && adj == 0)
    type A = Any
    new Empty[A]
  }
}
/** A single channel UGen of zero length. */
final case class Empty[A]() extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = makeUGen(Vector.empty)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut[A](this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut =
    stream.Empty()
}

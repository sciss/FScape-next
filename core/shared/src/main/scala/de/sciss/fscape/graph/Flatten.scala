/*
 *  Flatten.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.{GE, UGenGraph, UGenInLike}

object Flatten extends ProductReader[Flatten[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Flatten[_] = {
    require (arity == 1 && adj == 0)
    val _elem = in.readGE[Any]()
    new Flatten(_elem)
  }
}
/** A graph element that flattens the channels from a nested multi-channel structure.
  *
  * @param elem the element to flatten
  */
final case class Flatten[A](elem: GE[A]) extends GE.Lazy[A] {
  override def toString = s"$elem.flatten"

  /** Abstract method which must be implemented by creating the actual `UGen`s
    * during expansion. This method is at most called once during graph
    * expansion
    *
    * @return the expanded object (depending on the type parameter `U`)
    */
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGenInGroup(elem.expand.flatOutputs)
}
/*
 *  WhiteNoise.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object WhiteNoise extends ProductReader[WhiteNoise] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): WhiteNoise = {
    require (arity == 1 && adj == 0)
    val _mul = in.readGE_D()
    new WhiteNoise(_mul)
  }
}
/** Nominal signal range is `-mul` to `+mul` (exclusive). */
final case class WhiteNoise(mul: GE.D = 1.0) extends UGenSource.SingleOut[Double] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(mul.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] = {
    val noiseUGen = UGen.SingleOut(this, Vector.empty)
    val mulUGen   = args.head.asInstanceOf[UGenIn[Double]]
    BinaryOp.Times[Double]().make(noiseUGen, mulUGen)
  }

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut =
    stream.WhiteNoise()
}

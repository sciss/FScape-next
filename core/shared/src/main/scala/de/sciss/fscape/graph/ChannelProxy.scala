/*
 *  ChannelProxy.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}

object ChannelProxy extends ProductReader[ChannelProxy[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ChannelProxy[_] = {
    require (arity == 2 && adj == 0)
    val _elem   = in.readGE[Any]()
    val _index  = in.readInt()
    new ChannelProxy(_elem, _index)
  }
}
/** Straight outta ScalaCollider. */
final case class ChannelProxy[A](elem: GE[A], index: Int) extends GE.Lazy[A] {
  override def toString = s"$elem.\\($index)"

  def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    val _elem = elem.expand
    _elem.unwrap(index)
  }
}
/*
 *  Empty.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.stage.OutHandler
import akka.stream.{Attributes, SourceShape}
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object Empty {
  def apply()(implicit b: Builder): OutD = {
    val stage0  = new Stage(b.layer)
    val stage   = b.add(stage0)
    stage.out
  }

  private final val name = "Empty"

  private type Shp = SourceShape[Buf.D]

  private final class Stage(layer: Layer)(implicit a: Allocator) extends StageImpl[Shp](name) { stage =>
    val shape: Shape = new SourceShape(OutD(s"${stage.name}.out"))

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic(shape, layer)
  }

  private final class Logic(shape: Shp, layer: Layer)(implicit a: Allocator)
    extends NodeImpl(name, layer, shape) with OutHandler {

    override protected def launch(): Unit = completeStage()

    def onPull(): Unit = ()

    setHandler(shape.out, this)
  }
}
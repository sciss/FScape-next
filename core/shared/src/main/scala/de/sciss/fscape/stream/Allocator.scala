/*
 *  Allocator.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import java.util.concurrent.ConcurrentLinkedQueue
import scala.util.Random

object Allocator {
  def apply(control: Control, blockSize: Int, randomSeed: Long): Allocator = {
    require (blockSize > 0)
    new Impl(control, blockSize = blockSize, randomSeed = randomSeed)
  }

  implicit def fromBuilder(implicit b: Builder): Allocator = b.allocator

  final case class Stats(numBufD: Int, numBufI: Int, numBufL: Int) {
    override def toString =
      s"$productPrefix(numBufD = $numBufD, numBufI = $numBufI, numBufL = $numBufL)"
  }

  private final class Impl(val control: Control, val blockSize: Int, randomSeed: Long) extends Allocator {
    private[this] val queueD      = new ConcurrentLinkedQueue[BufD]
    private[this] val queueI      = new ConcurrentLinkedQueue[BufI]
    private[this] val queueL      = new ConcurrentLinkedQueue[BufL]
//    private[this] val queueB      = new ConcurrentLinkedQueue[BufB]
    private[this] val metaRand    = new Random(randomSeed)

    override def stats: Stats = Stats(
      numBufD = queueD.size(), numBufI = queueI.size(), numBufL = queueL.size(),
    )

    override def borrowBufD(): BufD = {
      val res0 = queueD.poll()
      val res  = if (res0 == null) BufD.alloc(blockSize) else {
        res0.acquire()
        res0
      }
      // println(s"Control.borrowBufD(): $res / ${res.allocCount()}")
      // assert(res.allocCount() == 1, res.allocCount())
      res
    }

    override def returnBufD(buf: BufD): Unit = {
      require(buf.allocCount() == 0)
      // println(s"control: ${buf.hashCode.toHexString} - ${buf.buf.toVector.hashCode.toHexString}")
      queueD.offer(buf) // XXX TODO -- limit size?
      ()
    }

    override def borrowBufI(): BufI = {
      val res0 = queueI.poll()
      if (res0 == null) BufI.alloc(blockSize) else {
        res0.acquire()
        res0
      }
    }

    override def returnBufI(buf: BufI): Unit = {
      require(buf.allocCount() == 0)
      queueI.offer(buf) // XXX TODO -- limit size?
      ()
    }

    override def borrowBufL(): BufL = {
      val res0 = queueL.poll()
      if (res0 == null) BufL.alloc(blockSize) else {
        res0.acquire()
        res0
      }
    }

    override def returnBufL(buf: BufL): Unit = {
      require(buf.allocCount() == 0)
      queueL.offer(buf) // XXX TODO -- limit size?
      ()
    }

//    override def borrowBufB(): BufB = {
//      val res0 = queueB.poll()
//      if (res0 == null) BufB.alloc(blockSize) else {
//        res0.acquire()
//        res0
//      }
//    }
//
//    override def returnBufB(buf: BufB): Unit = {
//      require(buf.allocCount() == 0)
//      queueB.offer(buf) // XXX TODO -- limit size?
//      ()
//    }

    override def newSeed(): Long = metaRand.nextLong()

    override def mkRandom(): Random = /* sync.synchronized */ {
      new Random(metaRand.nextLong())
    }
  }
}
trait Allocator {
  def control: Control

  /** The allocator specific buffer size. The guaranteed size of the double and integer arrays. */
  def blockSize: Int

  /** Borrows a double buffer. Its size is reset to `bufSize`. */
  def borrowBufD(): BufD

  /** Borrows an integer buffer. Its size is reset to `bufSize`. */
  def borrowBufI(): BufI

  /** Borrows a long buffer. Its size is reset to `bufSize`. */
  def borrowBufL(): BufL

//  /** Borrows a boolean buffer. Its size is reset to `bufSize`. */
//  def borrowBufB(): BufB

  /** Returns a double buffer. When `buf.borrowed` is `false`, this is a no-op.
    * This should never be called directly but only by the buffer itself
    * through `buf.release()`.
    */
  def returnBufD(buf: BufD): Unit

  /** Returns an integer buffer. When `buf.borrowed` is `false`, this is a no-op.
    * This should never be called directly but only by the buffer itself
    * through `buf.release()`.
    */
  def returnBufI(buf: BufI): Unit

  /** Returns a long integer buffer. When `buf.borrowed` is `false`, this is a no-op.
    * This should never be called directly but only by the buffer itself
    * through `buf.release()`.
    */
  def returnBufL(buf: BufL): Unit

//  /** Returns a boolean buffer. When `buf.borrowed` is `false`, this is a no-op.
//    * This should never be called directly but only by the buffer itself
//    * through `buf.release()`.
//    */
//  def returnBufB(buf: BufB): Unit

  def mkRandom(): Random

  def newSeed(): Long

  def stats : Allocator.Stats
}

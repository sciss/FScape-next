/*
 *  BlockSize.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.{Graph, Lazy, UGenGraph, UGenInLike, GE => _GE}
import de.sciss.numbers.Implicits.intNumberWrapper

import scala.{Unit => _Unit}

object BlockSize {
  object Result extends LowPri {
    implicit def GE[A]: GEResult[A] = instance.asInstanceOf[GEResult[A]]

    private final val instance = new GEResult[Any]
  }
  sealed trait Result[-A, Res] {
    def make(n: Int, g: Graph, result: A): Res
  }

  final class GEResult[A] extends Result[_GE[A], _GE[A]] {
    override def make(n: Int, g: Graph, result: _GE[A]): _GE[A] = GEBlock(n, g, result)
  }
  object GEBlock extends ProductReader[GEBlock[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GEBlock[_] = {
      require (arity == 3 && adj == 0)
      val _n      = in.readInt()
      val _g      = in.readGraph()
      val _result = in.readGE[Any]()
      new GEBlock[Any](_n, _g, _result)
    }
  }
  final case class GEBlock[A](n: Int, g: Graph, result: _GE[A]) extends _GE.Lazy[A] {
    override def productPrefix: String = s"BlockSize$$GE" // serialization

    override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
      b.withBlockSize(n) {
        b.expandNested(g)
        result.expand
      }
  }

  trait LowPri {
    implicit final def Unit[A]: UnitResult[A] = instance.asInstanceOf[UnitResult[A]]
    private final val instance = new UnitResult[Any]
  }

  final class UnitResult[A] extends Result[A, _Unit] with ProductReader[UnitBlock] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UnitBlock = {
      require (arity == 2 && adj == 0)
      val _n      = in.readInt()
      val _g      = in.readGraph()
      UnitBlock(_n, _g)
    }

    override def make(n: Int, g: Graph, result: A): _Unit = {
      UnitBlock(n, g)
      ()
    }
  }
  object UnitBlock extends ProductReader[UnitBlock] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UnitBlock = {
      require (arity == 2 && adj == 0)
      val _n      = in.readInt()
      val _g      = in.readGraph()
      UnitBlock(_n, _g)
    }
  }
  final case class UnitBlock(n: Int, g: Graph) extends Lazy.Expander[_Unit] {
    override def productPrefix: String = s"BlockSize$$UnitBlock" // serialization

    override protected def makeUGens(implicit b: UGenGraph.Builder): _Unit = {
      b.withBlockSize(n) {
        b.expandNested(g)
      }
      ()
    }
  }

  def apply[A, Res](n: Int)(branch: => A)(implicit result: Result[A, Res]): Res = {
    require (n > 0 && n.isPowerOfTwo)
    var res: A = null.asInstanceOf[A]
    val g = Graph {
      res = branch
    }
    result.make(n, g, res)
  }
}

//object TestImplicit {
//  implicitly[BlockSize.Result[ControlBlockSize, _GE]]
//}

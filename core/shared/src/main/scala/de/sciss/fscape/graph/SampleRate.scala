/*
 *  SampleRate.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.{GE, UGenGraph, UGenInLike}

object SampleRate extends ProductReader[SampleRate] {
  /** Defines an explicit sampling rate from a given value. */
  def apply(value: GE[Double]): SampleRate = Impl(value)

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SampleRate = {
    require (arity == 1 && adj == 0)
    val _value = in.readGE_D()
    SampleRate(_value)
  }

  private final case class Impl(value: GE.D) extends SampleRate {
    override def productPrefix: String = "SampleRate" // serialization

    override private[fscape] def expand(implicit b: UGenGraph.Builder): UGenInLike[Double] =
      value.expand
  }
}
/** A sample rate element simply signalizes that it refers to a sampling rate in Hertz.
  * When in implicit scope, it enables extension methods on graph elements, such as
  * `440.hertz` or `2.3.seconds`.
  */
trait SampleRate extends GE[Double]

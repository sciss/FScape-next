/*
 *  DelayN.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object DelayN extends ProductReader[DelayN[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DelayN[_] = {
    require (arity == 3 && adj == 0)
    val _in         = in.readGE[Any]()
    val _maxLength  = in.readGE_I()
    val _length     = in.readGE_I()
    new DelayN(_in, _maxLength, _length)
  }
}
final case class DelayN[A](in: GE[A], maxLength: GE.I, length: GE.I) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, maxLength.expand, length.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, maxLength, length) = args: @unchecked
    import in.tpe
    val out = stream.DelayN[in.A](in = in.toElem, maxLength = maxLength.toInt, length = length.toInt)
    tpe.mkStreamOut(out)
  }
}
/*
 *  FilterWindowedInAOutA.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream
package impl.logic

import akka.stream.Shape
import de.sciss.fscape.DataType
import de.sciss.fscape.stream.impl.Handlers
import de.sciss.fscape.stream.impl.Handlers.{InMain, OutMain}

/** An abstract class implementing both `Handlers` and `WindowedInAOutA`. This solves a bug in Scala
  * specialization where we cannot call `InMain` inside a specialized class. In this case,
  * `FilterWindowedInAOutA` is not specialized itself, but classes extending `FilterWindowedInAOutA`
  * may well choose to do so.
  */
abstract class FilterWindowedInAOutA[A, S <: Shape](name: String, layer: Layer, shape: S)
                                                   (inlet: In[A], outlet: Out[A])
                                                                    (implicit allocator: Allocator,
                                                                     protected val tpe: DataType[A])
  extends Handlers[S](name, layer, shape) with WindowedInAOutA[A] {

  protected final val hIn : InMain [A] = InMain [A](this, inlet )
  protected final val hOut: OutMain[A] = OutMain[A](this, outlet)
}

abstract class FilterWindowedInAOutB[A, B, C, S <: Shape](name: String, layer: Layer, shape: S)
                                                         (inlet: In[A], outlet: Out[B])
                                                         (implicit a: Allocator,
                                                          protected val aTpe: DataType[A],
                                                          protected val bTpe: DataType[B]
                                                         )
  extends Handlers[S](name, layer, shape) with WindowedInAOutB[A, B, C] {

  protected final val hIn : InMain [A] = InMain [A](this, inlet )
  protected final val hOut: OutMain[B] = OutMain[B](this, outlet)
}

/*
 *  Masking2D.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}

import scala.collection.immutable.{IndexedSeq => Vec}

object Masking2D extends ProductReader[Masking2D] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Masking2D = {
    require (arity == 8 && adj == 0)
    val _fg          = in.readGE_D()
    val _bg          = in.readGE_D()
    val _rows        = in.readGE_I()
    val _columns     = in.readGE_I()
    val _threshNoise = in.readGE_D()
    val _threshMask  = in.readGE_D()
    val _blurRows    = in.readGE_I()
    val _blurColumns = in.readGE_I()
    new Masking2D(_fg, _bg, _rows, _columns, _threshNoise, _threshMask, _blurRows, _blurColumns)
  }
}
final case class Masking2D(fg         : GE.D,
                           bg         : GE.D,
                           rows       : GE.I,
                           columns    : GE.I,
                           threshNoise: GE.D,
                           threshMask : GE.D,
                           blurRows   : GE.I,
                           blurColumns: GE.I,
                          )
  extends UGenSource.SingleOut[Double] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(fg.expand, bg.expand, rows.expand, columns.expand,
      threshNoise.expand, threshMask.expand, blurRows.expand, blurColumns.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGen.SingleOut[Double] =
    UGen.SingleOut(this, inputs = args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(fg, bg, rows, columns, threshNoise, threshMask, blurRows, blurColumns) = args: @unchecked
    stream.Masking2D(
      fg          = fg          .toDouble,
      bg          = bg          .toDouble,
      rows        = rows        .toInt,
      columns     = columns     .toInt,
      threshNoise = threshNoise .toDouble,
      threshMask  = threshMask  .toDouble,
      blurRows    = blurRows    .toInt,
      blurColumns = blurColumns .toInt,
    )
  }
}

/*
 *  Distinct.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Eq
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Distinct extends ProductReader[Distinct[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Distinct[_] = {
    require (arity == 1)
    val _in = in.readGE[Any]()
    val _eq = if (adj == 0) LegacyAdjunct.Eq else {
      require (adj == 1)
      in.readAdjunct[Eq[Any]]()
    }
    new Distinct[Any](_in)(_eq)
  }
}
/** A UGen that collects distinct values from the input and outputs them
  * in the original order.
  *
  * '''Note:''' Currently keeps all seen values in memory.
  */
final case class Distinct[A](in: GE[A])(implicit eq: Eq[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = eq :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    import in.tpe
    val out = stream.Distinct[in.A](in = in.toElem)
    tpe.mkStreamOut(out)
  }
}
/*
 *  Trig.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Trig extends ProductReader[Trig[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Trig[_] = {
    require (arity == 1)
    val _in   = in.readGE[Any]()
    val _num  = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new Trig[Any](_in)(_num)
  }
}
/** A UGen that creates a trigger when input changes from non-positive to positive.
  * This is useful when a gate argument of another UGen should indeed be used like
  * a single trigger, as a constant positive input will not produce a held signal.
  */
final case class Trig[A](in: GE[A])(implicit num: Num[A])
  extends UGenSource.SingleOut[Boolean] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    UGen.SingleOut(this, inputs = args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    import in.tpe
    stream.Trig[in.A](in = in.toElem)
  }
}

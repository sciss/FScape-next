/*
 *  Differentiate.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Differentiate extends ProductReader[Differentiate[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Differentiate[_] = {
    require (arity == 1)
    val _in   = in.readGE[Any]()
    val _num  = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new Differentiate[Any](_in)(_num)
  }
}
/** A UGen that outputs the differences between
  * adjacent input samples.
  *
  * The length of the signal is the length of the input.
  * The first output will be the first input (internal
  * x1 state is zero).
  */
final case class Differentiate[A](in: GE[A])(implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    import in.tpe
    val out = stream.Differentiate[in.A](in = in.toElem)
    tpe.mkStreamOut(out)
  }
}
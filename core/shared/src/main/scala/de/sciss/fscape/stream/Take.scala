/*
 *  Take.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.DataType
import de.sciss.fscape.graph.ConstL
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.annotation.tailrec
import scala.math.{max, min}

object Take {
  def head[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val length = ConstL(1L).toLong
    apply[A](in = in, length = length)
  }

  def apply[A](in: Out[A], length: OutL)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in    , stage.in0)
    b.connect(length, stage.in1)
    stage.out
  }

  private final val name = "Take"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.L, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.in"    ),
      in1 = InL   (s"${stage.name}.length"),
      out = Out[A](s"${stage.name}.out"   )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](shape, layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hIn   = Handlers.InMain [A](this, shape.in0)
    private[this] val hLen  = Handlers.InLAux    (this, shape.in1)(max(0L, _))
    private[this] val hOut  = Handlers.OutMain[A](this, shape.out)

    private[this] var takeRemain  = -1L
    private[this] var init        = true

    protected def onDone(inlet: Inlet[_]): Unit = {
      assert (inlet == hIn.inlet)
      if (hOut.flush()) {
        completeStage()
      }
    }

    @tailrec
    protected def process(): Unit = {
      if (init) {
        if (!hLen.hasNext) return
        takeRemain  = hLen.next()
        init        = false
      }

      val remIn = hIn.available
      if (remIn == 0) return

      val remOut  = hOut.available
      val numCopy = min(remOut, min(remIn, takeRemain).toInt)
      val hasCopy = numCopy > 0
      if (hasCopy) {
        hIn.copyTo(hOut, numCopy)
        takeRemain -= numCopy
      }

      if (takeRemain == 0L || hIn.isDone) {
        if (hOut.flush()) {
          completeStage()
        }
        return
      }

      if (hasCopy) process()
    }
  }
}
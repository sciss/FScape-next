/*
 *  OffsetOverlapAdd.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object OffsetOverlapAdd extends ProductReader[OffsetOverlapAdd[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): OffsetOverlapAdd[_] = {
    require (arity == 5)
    val _in         = in.readGE[Any]()
    val _size       = in.readGE_I()
    val _step       = in.readGE_I()
    val _offset     = in.readGE_I()
    val _minOffset  = in.readGE_I()
    val _num        = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new OffsetOverlapAdd[Any](_in, _size, _step, _offset, _minOffset)(_num)
  }
}
/** Overlapping window summation with offset (fuzziness) that can be modulated.
  */
final case class OffsetOverlapAdd[A](in: GE[A], size: GE.I, step: GE.I, offset: GE.I, minOffset: GE.I)
                                    (implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, step.expand, offset.expand, minOffset.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, step, offset, minOffset) = args: @unchecked
    val inC = in.cast[A]
    implicit val tpe: DataType.Num[A] = inC.tpe.castNum
    val out = stream.OffsetOverlapAdd(
      in        = inC       .toElem,
      size      = size      .toInt,
      step      = step      .toInt,
      offset    = offset    .toInt,
      minOffset = minOffset .toInt,
    )
    tpe.mkStreamOut(out)
  }
}
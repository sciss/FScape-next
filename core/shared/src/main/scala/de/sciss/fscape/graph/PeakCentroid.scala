/*
 *  PeakCentroid.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenGraph.Builder
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object PeakCentroid1D extends ProductReader[PeakCentroid1D] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): PeakCentroid1D = {
    require (arity == 5 && adj == 0)
    val _in       = in.readGE_D()
    val _size     = in.readGE_I()
    val _thresh1  = in.readGE_D()
    val _thresh2  = in.readGE_D()
    val _radius   = in.readGE_I()
    new PeakCentroid1D(_in, _size, _thresh1, _thresh2, _radius)
  }
}
/**
  * '''Warning:''' window parameter modulation is currently not working correctly (issue #30)
  */
final case class PeakCentroid1D(in: GE.D, size: GE.I, thresh1: GE.D = 0.5, thresh2: GE.D = 0.33, radius: GE.I = 1)
  extends GE.Lazy[Double] {

  protected def makeUGens(implicit b: Builder): UGenInLike[Double] = {
    val c   = PeakCentroid2D(in = in, width = size, height = 1, thresh1 = thresh1, thresh2 = thresh2, radius = radius)
    val tx  = c.translateX
    val p   = c.peak
    Seq(tx, p): GE[Double]
  }

  def translate: GE.D = ChannelProxy(this, 0)
  def peak     : GE.D = ChannelProxy(this, 1)
}

object PeakCentroid2D extends ProductReader[PeakCentroid2D] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): PeakCentroid2D = {
    require (arity == 6 && adj == 0)
    val _in       = in.readGE_D()
    val _width    = in.readGE_I()
    val _height   = in.readGE_I()
    val _thresh1  = in.readGE_D()
    val _thresh2  = in.readGE_D()
    val _radius   = in.readGE_I()
    new PeakCentroid2D(_in, _width, _height, _thresh1, _thresh2, _radius)
  }
}
/**
  * '''Warning:''' window parameter modulation is currently not working correctly (issue #30)
  */
final case class PeakCentroid2D(in: GE.D, width: GE.I, height: GE.I, thresh1: GE.D = 0.5, thresh2: GE.D = 0.33,
                                radius: GE.I = 1)
  extends UGenSource.MultiOut[Double] {

  def translateX: GE.D = ChannelProxy(this, 0)
  def translateY: GE.D = ChannelProxy(this, 1)
  def peak      : GE.D = ChannelProxy(this, 2)

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, width.expand, height.expand, thresh1.expand, thresh2.expand, radius.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.MultiOut(this, inputs = args, numOutputs = 3)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Vec[StreamOut] = {
    val Vec(in, width, height, thresh1, thresh2, radius) = args: @unchecked
    val (tx, ty, p) = stream.PeakCentroid2D(
      in      = in      .toDouble,
      width   = width   .toInt,
      height  = height  .toInt,
      thresh1 = thresh1 .toDouble,
      thresh2 = thresh2 .toDouble,
      radius  = radius  .toInt,
    )
    Vec(tx, ty, p)
  }
}
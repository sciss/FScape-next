/*
 *  GeomSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{Num, NumInt}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object GeomSeq extends ProductReader[GeomSeq[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GeomSeq[_, _] = {
    require (arity == 3)
    val _start  = in.readGE[Any]()
    val _grow   = in.readGE[Any]()
    val _length = in.readGE[Any]()
    require (adj == 2 || adj == 0)
    val _num    = if (adj == 0) LegacyAdjunct.Num     else in.readAdjunct[Num   [Any]]()
    val _numL   = if (adj == 0) LegacyAdjunct.NumInt  else in.readAdjunct[NumInt[Any]]()
    new GeomSeq[Any, Any](_start, _grow, _length)(_num, _numL)
  }
}
/** A UGen that produces a geometric sequence of values.
  * If both `start` and `grow` are of integer type (`Int` or `Long`),
  * this produces an integer output, otherwise it produces a floating point output.
  *
  * E.g. `GeomSeq(start = 1, grow = 2)` will produce a series `1, 2, 4, 8, ...`.
  *
  * @param start    the first output element
  * @param grow     the multiplicative factor by which each successive element will grow
  * @param length   the number of elements to output
  */
final case class GeomSeq[A, L](start: GE[A] = 1, grow: GE[A] = 2, length: GE[L] = Long.MaxValue)
                           (implicit val num: Num[A], val numL: NumInt[L])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(start.expand, grow.expand, length.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(start, grow, length) = args: @unchecked

    if (start.isInt && grow.isInt) {
      stream.GeomSeq[Int    ](start = start.toInt   , grow = grow.toInt   , length = length.toLong)
    } else if ((start.isInt || start.isLong) && (grow.isInt || grow.isLong)) {
      stream.GeomSeq[Long   ](start = start.toLong  , grow = grow.toLong  , length = length.toLong)
    } else {
      stream.GeomSeq[Double ](start = start.toDouble, grow = grow.toDouble, length = length.toLong)
    }
  }
}
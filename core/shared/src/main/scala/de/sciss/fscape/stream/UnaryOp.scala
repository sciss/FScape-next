/*
 *  UnaryOp.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FlowShape, Inlet}
import de.sciss.fscape.stream.impl.Handlers._
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.annotation.tailrec

object UnaryOp {
  def apply[A, B](opName: String, op: A => B, in: Out[A])(implicit b: Builder, aTpe: DataType[A],
                                                          bTpe: DataType[B]): Out[B] = {
    val stage0  = new Stage[A, B](b.layer, opName, op)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "UnaryOp"

  private type Shp[A, B] = FlowShape[Buf.E[A], Buf.E[B]]

  private final class Stage[A, B](layer: Layer, opName: String, op: A => B)
                                 (implicit a: Allocator, aTpe: DataType[A], bTpe: DataType[B])
    extends StageImpl[Shp[A, B]](s"$name($opName)") { stage =>

    val shape: Shape = new FlowShape(
      in  = In [A](s"${stage.name}e.in" ),
      out = Out[B](s"${stage.name}e.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_, _] = if (aTpe.isDouble) {
        if (bTpe.isDouble) {
          new Logic[Double, Double](
            shape.asInstanceOf[Shp[Double, Double]], layer, opName, op.asInstanceOf[Double  => Double ])
        } else if (bTpe.isInt) {
          new Logic[Double, Int](
            shape.asInstanceOf[Shp[Double, Int]], layer, opName, op.asInstanceOf[Double  => Int    ])
        } else {
          assert (bTpe.isLong)
          new Logic[Double, Long](
            shape.asInstanceOf[Shp[Double, Long]], layer, opName, op.asInstanceOf[Double  => Long   ])
        }
      } else if (aTpe.isInt) {
        if (bTpe.isDouble) {
          new Logic[Int, Double](
            shape.asInstanceOf[Shp[Int, Double]], layer, opName, op.asInstanceOf[Int     => Double ])
        } else if (bTpe.isInt) {
          new Logic[Int, Int](
            shape.asInstanceOf[Shp[Int, Int]], layer, opName, op.asInstanceOf[Int     => Int    ])
        } else {
          assert (bTpe.isLong)
          new Logic[Int, Long](
            shape.asInstanceOf[Shp[Int, Long]], layer, opName, op.asInstanceOf[Int     => Long   ])
        }
      } else {
        assert(aTpe.isLong)
        if (bTpe.isDouble) {
          new Logic[Long, Double](
            shape.asInstanceOf[Shp[Long, Double]], layer, opName, op.asInstanceOf[Long    => Double ])
        } else if (bTpe.isInt) {
          new Logic[Long, Int](
            shape.asInstanceOf[Shp[Long, Int]], layer, opName, op.asInstanceOf[Long    => Int    ])
        } else {
          assert (bTpe.isLong)
          new Logic[Long, Long](
            shape.asInstanceOf[Shp[Long, Long]], layer, opName, op.asInstanceOf[Long    => Long   ])
        }
      }
      res.asInstanceOf[Logic[A, B]]
    }
  }

  private final class Logic[@specialized(Args) A, @specialized(Args) B](shape: Shp[A, B], layer: Layer,
                                                                        opName: String, op: A => B)
                                                                       (implicit a: Allocator,
                                                                        aTpe: DataType[A],
                                                                        bTpe: DataType[B])
    extends Handlers(s"$name($opName)", layer, shape) {

    private[this] val hIn : InMain  [A] = InMain  [A](this, shape.in )
    private[this] val hOut: OutMain [B] = OutMain [B](this, shape.out)

    private def run(a: Array[A], ai0: Int, b: Array[B], bi0: Int, stop: Int): Unit = {
      var ai = ai0
      var bi = bi0
      while (ai < stop) {
        b(bi) = op(a(ai))
        ai += 1
        bi += 1
      }
    }

    @tailrec
    protected def process(): Unit = {
      val rem = math.min(hIn.available, hOut.available)
      if (rem == 0) return

      val a     = hIn .array
      val ai    = hIn .offset
      val b     = hOut.array
      val bi    = hOut.offset
      val stop  = ai + rem
      run(a = a, ai0 = ai, b = b, bi0 = bi, stop = stop)
      hIn .advance(rem)
      hOut.advance(rem)

      if (hIn.isDone) {
        if (hOut.flush()) completeStage()
        return
      }

      process()
    }

    protected def onDone(inlet: Inlet[_]): Unit =
      if (hOut.flush()) {
        completeStage()
      }
  }
}
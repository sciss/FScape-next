/*
 *  Trig.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.{Attributes, FlowShape}
import de.sciss.fscape.DataType
import de.sciss.fscape.stream.impl.logic.FilterIn1Out1
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object Trig {
  def apply[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): OutI = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "Trig"

  private type Shp[A] = FlowShape[Buf.E[A], Buf.I]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FlowShape(
      in  = In[A](s"${stage.name}.in" ),
      out = OutI (s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double]], layer)(_ > 0.0)
      } else if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int]], layer)(_ > 0)
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape.asInstanceOf[Shp[Long]], layer)(_ > 0L)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer)(high: A => Boolean)
                                                 (implicit a: Allocator, tpe: DataType[A])
    extends FilterIn1Out1[A, Int](name, layer, shape) {

    private[this] var x1 = false

    protected def run(in: Array[A], inOff0: Layer, out: Array[Int], outOff0: Int, n: Int): Unit = {
      var inOff   = inOff0
      var outOff  = outOff0
      val stop    = inOff0 + n
      var _x1     = x1
      while (inOff < stop) {
        val x0 = high(in(inOff))
        val y0 = if (x0 && !_x1) 1 else 0
        out(outOff) = y0
        _x1     = x0
        inOff  += 1
        outOff += 1
      }
      x1 = _x1
    }
  }
}
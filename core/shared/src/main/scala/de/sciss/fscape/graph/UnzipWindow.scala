/*
 *  UnzipWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object UnzipWindow extends ProductReader[UnzipWindow[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UnzipWindow[_] = {
    require (arity == 3 && adj == 0)
    val _in   = in.readGE[Any]()
    val _size = in.readGE_I()
    new UnzipWindow[Any](_in, _size)
  }
}
final case class UnzipWindow[A](in: GE[A], size: GE.I = 1) extends GE.Lazy[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UnzipWindowN(numOutputs = 2, in = in, size = size)
}

object UnzipWindowN extends ProductReader[UnzipWindowN[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UnzipWindowN[_] = {
    require (arity == 3 && adj == 0)
    val _numOutputs = in.readInt()
    val _in         = in.readGE[Any]()
    val _size       = in.readGE_I()
    new UnzipWindowN(_numOutputs, _in, _size)
  }
}
final case class UnzipWindowN[A](numOutputs: Int, in: GE[A], size: GE.I = 1) extends UGenSource.MultiOut[A] {
  protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.MultiOut(this, args, numOutputs = numOutputs, adjuncts = Adjunct.Int(numOutputs) :: Nil)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit builder: stream.Builder): Vec[StreamOut] = {
    val Vec(in, size) = args: @unchecked
    import in.tpe
    val outs = stream.UnzipWindowN[in.A](numOutputs = numOutputs, in = in.toElem, size = size.toInt)
    outs.map(tpe.mkStreamOut)
  }
}
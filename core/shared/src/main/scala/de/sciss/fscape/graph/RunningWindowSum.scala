/*
 *  RunningWindowSum.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object RunningWindowSum extends ProductReader[RunningWindowSum[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RunningWindowSum[_] = {
    require (arity == 3)
    val _in     = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _gate   = in.readGE_B()
    val _num    = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new RunningWindowSum[Any](_in, _size, _gate)(_num)
  }
}
/** A UGen that like `RunningSum` calculates the sum of the
  * running input. However, it operates on entire windows, i.e. it outputs
  * windows that contain the sum elements of all the past windows observed.
  *
  * @param in     the windowed signal to monitor
  * @param size   the window size. This should normally be a constant. If modulated,
  *               the internal buffer will be re-allocated, essentially causing
  *               a reset trigger.
  * @param gate   a gate signal that clears the internal state. When a gate is open (> 0),
  *               the ''currently processed'' window is reset altogether
  *               until its end, beginning accumulation again from the successive
  *               window.
  */
final case class RunningWindowSum[A](in: GE[A], size: GE.I, gate: GE.B = false)(implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, gate) = args: @unchecked
    import in.tpe
    val out = stream.RunningWindowSum[in.A](in = in.toElem, size = size.toInt, gate = gate.toInt)
    tpe.mkStreamOut(out)
  }
}
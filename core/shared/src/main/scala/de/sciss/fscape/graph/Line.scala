/*
 *  Line.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Line extends ProductReader[Line[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Line[_] = {
    require (arity == 3)
    val _start  = in.readGE_D()
    val _end    = in.readGE_D()
    val _length = in.readGE[Any]()
    val _numL   = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[Any]]()
    }
    new Line[Any](_start, _end, _length)(_numL)
  }
}
/** A line segment generating UGen. The UGen terminates
  * when the segment has reached the end.
  *
  * A line can be used to count integers (in the lower
  * ranges, where floating point noise is not yet relevant),
  * e.g. `Line(a, b, b - a + 1)` counts from `a` to
  * `b` (inclusive).
  *
  * @param start  starting value
  * @param end    ending value
  * @param length length of the segment in sample frames
  */
final case class Line[L](start: GE.D, end: GE.D, length: GE[L])(implicit numL: NumInt[L])
  extends UGenSource.SingleOut[Double] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(start.expand, end.expand, length.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(start, end, length) = args: @unchecked
    stream.Line(
      start   = start .toDouble,
      end     = end   .toDouble,
      length  = length.toLong,
    )
  }
}
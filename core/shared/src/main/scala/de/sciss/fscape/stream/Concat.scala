/*
 *  Concat.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

object Concat {
  def apply[A](a: Out[A], b: Out[A])
              (implicit builder: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](nameCC, builder.layer)
    val stage   = builder.add(stage0)
    builder.connect(a, stage.in0)
    builder.connect(b, stage.in1)
    stage.out
  }

  private final val nameCC = "Concat"

  type Shp[A] = FanInShape2[Buf.E[A], Buf.E[A], Buf.E[A]]

  final class Stage[A](name: String, layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.a"  ),
      in1 = In [A](s"${stage.name}.b"  ),
      out = Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](name, shape, layer)
  }

  private final class Logic[A](name: String, shape: Shp[A], layer: Layer)
                              (implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hInA  = Handlers.InMain [A](this, shape.in0)
    private[this] val hInB  = Handlers.InMain [A](this, shape.in1)
    private[this] val hOut  = Handlers.OutMain[A](this, shape.out)

    private[this] var first = true

    protected def onDone(inlet: Inlet[_]): Unit =
      if (first) {
        if (inlet == hInA.inlet) {
          first = false
          if (checkDoneB()) return
          process()
        }
      } else {
        assert (inlet == hInB.inlet)
        checkDoneB()
        ()
      }

    private def checkDoneB(): Boolean = {
      val res = hInB.isDone
      if (res) {
        if (hOut.flush()) completeStage()
      }
      res
    }

    protected def process(): Unit = {
      while (first) {
        val rem = math.min(hInA.available, hOut.available)
        if (rem == 0) return
        hInA.copyTo(hOut, rem)
        if (hInA.isDone) {
          first = false
          if (checkDoneB()) return
        }
      }

      while (true) {
        val rem = math.min(hInB.available, hOut.available)
        if (rem == 0) return
        hInB.copyTo(hOut, rem)
        if (checkDoneB()) return
      }
    }
  }
}
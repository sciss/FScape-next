/*
 *  BleachKernel.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object BleachKernel extends ProductReader[BleachKernel] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): BleachKernel = {
    require (arity == 4 && adj == 0)
    val _in         = in.readGE_D()
    val _filterLen  = in.readGE_I()
    val _feedback   = in.readGE_D()
    val _filterClip = in.readGE_D()
    new BleachKernel(_in, _filterLen, _feedback, _filterClip)
  }
}
/** An UGen that outputs an adaptive filter kernel based on an input signal.
  * The finite impulse response kernel can be used to remove resonances from the input signal.
  * This UGen operates like the `Bleach` UGen, but instead of filtering the input,
  * it outputs the raw filter coefficients. For each input sample, `filterLen` output
  * samples will be generated, representing the current kernel.
  *
  * If one is only interested in a "final" kernel, one can just `TakeRight` the last
  * `filterLen` output samples.
  *
  * @param in           the signal to analyze
  * @param filterLen    the length of the FIR filter
  * @param feedback     the adaptation speed of the filter. Lower values
  *                     mean it takes longer for the filter to adjust to input signal,
  *                     higher values mean it is faster changing according to the input.
  *                     Typical values are in the order of -60 dB.
  * @param filterClip   a clipping threshold for the filter coefficients, to avoid that
  *                     a filter "explodes". Each filter coefficient is clipped to
  *                     have a magnitude no larger than the given clip value.
  *
  * @see [[Bleach]]
  */
final case class BleachKernel(in: GE.D, filterLen: GE.I = 256, feedback: GE.D = 0.001, filterClip: GE.D = 8.0)
  extends UGenSource.SingleOut[Double] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, filterLen.expand, feedback.expand, filterClip.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, filterLen, feedback, filterClip) = args: @unchecked
    stream.BleachKernel(in = in.toDouble, filterLen = filterLen.toInt, feedback = feedback.toDouble,
      filterClip = filterClip.toDouble)
  }
}
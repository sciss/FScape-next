/*
 *  Blobs2D.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Blobs2D extends ProductReader[Blobs2D] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Blobs2D = {
    require (arity == 5 && adj == 0)
    val _in     = in.readGE_D()
    val _width  = in.readGE_I()
    val _height = in.readGE_I()
    val _thresh = in.readGE_D()
    val _pad    = in.readGE_I()
    new Blobs2D(_in, _width, _height, _thresh, _pad)
  }
}
/** A blob detection UGen. It is based on the meta-balls algorithm by
  * Julien Gachadoat (http://www.v3ga.net/processing/BlobDetection/).
  *
  * Currently, we output four channels:
  * - 0 - `numBlobs` - flushes for each window one element with the number of blobs detected
  * - 1 - `bounds` - quadruplets of `xMin`, `xMax`, `yMin`, `yMax` - for each blob
  * - 2 - `numVertices` - the number of vertices (contour coordinates) for each blob
  * - 3 - `vertices` - tuples of `x` and `y` for the vertices of each blob
  *
  * @param in         the image(s) to analyse
  * @param width      the width of the image
  * @param height     the height of the image
  * @param thresh     the threshold for blob detection between zero and one
  * @param pad        size of "border" to put around input matrices. If greater than
  *                   zero, a border of that size is created internally, filled with
  *                   the threshold value
  */
final case class Blobs2D(in: GE.D, width: GE.I, height: GE.I, thresh: GE.D = 0.3, pad: GE.I = 0)
  extends UGenSource.MultiOut[Any] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Any] =
    unwrap(this, Vector(in.expand, width.expand, height.expand, thresh.expand, pad.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Any] =
    UGen.MultiOut(this, args, numOutputs = 4)

  def numBlobs    : GE.I = ChannelProxy[Any](this, 0).asInstanceOf[ChannelProxy[Int    ]]
  def bounds      : GE.D = ChannelProxy[Any](this, 1).asInstanceOf[ChannelProxy[Double ]]
  def numVertices : GE.I = ChannelProxy[Any](this, 2).asInstanceOf[ChannelProxy[Int    ]]
  def vertices    : GE.D = ChannelProxy[Any](this, 3).asInstanceOf[ChannelProxy[Double ]]

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Vec[StreamOut] = {
    val Vec(in, width, height, thresh, pad) = args: @unchecked
    val (out0, out1, out2, out3) = stream.Blobs2D(in = in.toDouble, width = width.toInt, height = height.toInt,
      thresh = thresh.toDouble, pad = pad.toInt)
    Vector[StreamOut](out0, out1, out2, out3)
  }
}
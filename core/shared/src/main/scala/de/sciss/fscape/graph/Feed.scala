/*
 *  Feed.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{Builder, StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Feed extends ProductReader[Feed[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Feed[_] = {
    require (arity == 1)
    val _init = in.readGE[Any]()
    new Feed(_init)
  }

  object Back extends ProductReader[Back[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Back[_] = {
      require (arity == 2)
      val _to = in.readProductT[Feed[Any]]()
      val _in = in.readGE[Any]()
      new Back(_to, _in)
    }

    private final case class Expanded[A](to: Feed.Expanded[A], in: UGenInLike[A]) extends UGenSource.ZeroOut {
      override protected def makeUGens(implicit b: UGenGraph.Builder): Unit =
        unwrap(this, Vector(/*to.expand,*/ in /*.expand*/))

      override protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): Unit = {
        UGen.ZeroOut(this, args /*.tail*/) // drop 'to'
        ()
      }

      override private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: Builder): Unit = {
        val Vec(/*to,*/ in) = args: @unchecked
        import in.tpe
        stream.Feed.back[in.A](in.toElem, to) // XXX TODO -- we have no guarantee that `in.A` == `init.A`
      }
    }
  }
  case class Back[A](to: Feed[A], in: GE[A]) extends Lazy.Expander[Unit] {
    override def productPrefix: String = s"Feed$$Back"  // serialization

    override protected def makeUGens(implicit b: UGenGraph.Builder): Unit = {
      val toEx = to.expand
      val inEx = in.expand
      toEx.outputs.zipWithIndex.foreach {
        case (toExU: UGen.SingleOut[A], ch) =>
          toExU.source match {
            case toEx: Feed.Expanded[A] =>
              Back.Expanded(toEx, inEx.unwrap(ch))
            case _ => throw new AssertionError("Feed does not expand to Feed.Expanded")
          }

        case _ => throw new AssertionError("Feed does not expand to Feed.Expanded")
      }
    }
  }

  private final case class Expanded[A](init: UGenInLike[A]) extends UGenSource.SingleOut[A] { self =>
    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
      unwrap(this, Vector(init/*.expand*/))

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
      UGen.SingleOut(this, args)

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
      val Vec(init) = args: @unchecked
      import init.tpe
      val out = stream.Feed[init.A](init.toElem, self)
      tpe.mkStreamOut(out)
    }
  }
}
/** First stage of a feedback process. This UGen outputs the signal eventually fed to it
  * via `.back`.
  */
final case class Feed[A](init: GE[A]) extends GE.Lazy[A] {
  override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    val initEx = init.expand
    initEx.outputs.map { initCh =>
      Feed.Expanded(initCh)
    } : GE[A]
  }

  def back(in: GE[A]): Unit = {
    Feed.Back(this, in)
    ()
  }
}

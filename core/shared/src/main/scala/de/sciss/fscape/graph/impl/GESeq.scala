/*
 *  GESeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph
package impl

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}

import scala.collection.immutable.{IndexedSeq => Vec}

object GESeq extends ProductReader[GESeq[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GESeq[_] = {
    require (arity == 1 && adj == 0)
    val _in = in.readVec(in.readGE[Any]())
    new GESeq[Any](_in)
  }
}
final case class GESeq[A](elems: Vec[GE[A]]) extends GE[A] {
  private[fscape] def expand(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGenInGroup(elems.map(_.expand))

  override def toString: String = elems.mkString("GESeq(", ",", ")")
}
/*
 *  Filter.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.impl

import de.sciss.lucre.{Adjunct, Random}
import de.sciss.serial.DataInput

object LegacyAdjunct {
  private lazy val _init: Unit = {
    Adjunct.addFactory(Instance)
  }

  def init(): Unit = _init

    private def undefined(name: String): Nothing = throw new NotImplementedError(name)

  def Eq          : Adjunct.Eq          [Any] = Instance
  def ScalarEq    : Adjunct.ScalarEq    [Any] = Instance
  def Ord         : Adjunct.Ord         [Any] = Instance
  def ScalarOrd   : Adjunct.ScalarOrd   [Any] = Instance
  def Num         : Adjunct.Num         [Any] = Instance
  def NumDiv      : Adjunct.NumDiv      [Any] = Instance
  def NumInt      : Adjunct.NumInt      [Any] = Instance
  def NumBool     : Adjunct.NumBool     [Any] = Instance
  def FromAny     : Adjunct.FromAny     [Any] = Instance
  def ScalarToNum : Adjunct.ScalarToNum [Any] = Instance

  object Instance
    extends Adjunct.NumInt[Any]
      with Adjunct.NumBool[Any]
      with Adjunct.FromAny[Any]
      with Adjunct.ToNum  [Any]
      with Adjunct.Scalar [Any]
      with Adjunct.Factory {

    final val id = 5000

    override def readIdentifiedAdjunct(in: DataInput): Adjunct = this

    override def plus     (a: Any, b: Any): Any = undefined("plus")
    override def minus    (a: Any, b: Any): Any = undefined("minus")
    override def times    (a: Any, b: Any): Any = undefined("times")
    override def rem      (a: Any, b: Any): Any = undefined("rem")
    override def mod      (a: Any, b: Any): Any = undefined("mod")
    override def min      (a: Any, b: Any): Any = undefined("min")
    override def max      (a: Any, b: Any): Any = undefined("max")
    override def roundTo  (a: Any, b: Any): Any = undefined("roundTo")
    override def roundUpTo(a: Any, b: Any): Any = undefined("roundUpTo")
    override def trunc    (a: Any, b: Any): Any = undefined("trunc")
    override def difSqr   (a: Any, b: Any): Any = undefined("difSqr")
    override def sumSqr   (a: Any, b: Any): Any = undefined("sumSqr")
    override def sqrSum   (a: Any, b: Any): Any = undefined("sqrSum")
    override def sqrDif   (a: Any, b: Any): Any = undefined("sqrDif")
    override def absDif   (a: Any, b: Any): Any = undefined("absDif")
    override def clip2    (a: Any, b: Any): Any = undefined("clip2")
    override def excess   (a: Any, b: Any): Any = undefined("excess")
    override def fold2    (a: Any, b: Any): Any = undefined("fold2")
    override def wrap2    (a: Any, b: Any): Any = undefined("wrap2")

    override def negate   (a: Any): Any = undefined("negate")
    override def abs      (a: Any): Any = undefined("abs")
    override def signum   (a: Any): Any = undefined("signum")
    override def squared  (a: Any): Any = undefined("squared")
    override def cubed    (a: Any): Any = undefined("cubed")

    override def zero : Any = undefined("zero")
    override def one  : Any = undefined("one")

    override def rand [Tx](a: Any)(implicit r: Random[Tx], tx: Tx): Any = undefined("rand")
    override def rand2[Tx](a: Any)(implicit r: Random[Tx], tx: Tx): Any = undefined("rand2")

    override def rangeRand[Tx](a: Any, b: Any)(implicit r: Random[Tx], tx: Tx): Any = undefined("rangeRand")

    override def fold(a: Any, lo: Any, hi: Any): Any = undefined("fold")
    override def clip(a: Any, lo: Any, hi: Any): Any = undefined("clip")
    override def wrap(a: Any, lo: Any, hi: Any): Any = undefined("wrap")

    override def lt   (a: Any, b: Any): Boolean = undefined("lt")
    override def lteq (a: Any, b: Any): Boolean = undefined("lteq")
    override def gt   (a: Any, b: Any): Boolean = undefined("gt")
    override def gteq (a: Any, b: Any): Boolean = undefined("gteq")
    override def eq   (a: Any, b: Any): Boolean = undefined("eq")
    override def neq  (a: Any, b: Any): Boolean = undefined("neq")

    override def not(a: Any): Any = undefined("not")

    override def and(a: Any, b: Any): Any = undefined("and")
    override def or (a: Any, b: Any): Any = undefined("or")
    override def xor(a: Any, b: Any): Any = undefined("xor")
    override def lcm(a: Any, b: Any): Any = undefined("lcm")
    override def gcd(a: Any, b: Any): Any = undefined("gcd")

    override def shiftLeft          (a: Any, b: Any): Any = undefined("shiftLeft")
    override def shiftRight         (a: Any, b: Any): Any = undefined("shiftRight")
    override def unsignedShiftRight (a: Any, b: Any): Any = undefined("unsignedShiftRight")

    override def isPowerOfTwo   (a: Any): Boolean = undefined("isPowerOfTwo")
    override def nextPowerOfTwo (a: Any): Any     = undefined("nextPowerOfTwo")

    override def isEven (a: Any): Boolean = undefined("isEven")
    override def isOdd  (a: Any): Boolean = undefined("isOdd")

    override def div(a: Any, b: Any): Any = undefined("div")

    override def fromAny(in: Any): Option[Any] = undefined("fromAny")

    override def toInt    (a: Any): Instance.Int    = undefined("toInt")
    override def toDouble (a: Any): Instance.Double = undefined("toDouble")
    override def toLong   (a: Any): Instance.Long   = undefined("toLong")
  }
}

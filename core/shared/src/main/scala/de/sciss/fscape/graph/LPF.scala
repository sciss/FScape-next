/*
 *  LPF.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object LPF extends ProductReader[LPF] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): LPF = {
    require (arity == 2 && adj == 0)
    val _in     = in.readGE_D()
    val _freqN  = in.readGE_D()
    new LPF(_in, _freqN)
  }
}
/** A second order low-pass filter UGen.
  * It implements the same type of filter as the SuperCollider equivalent.
  *
  * @param  in    the signal to filter
  * @param  freqN the normalized cut-off frequency (frequency in Hertz divided by sampling rate)
  */
final case class LPF(in: GE.D, freqN: GE.D = 0.02)
  extends UGenSource.SingleOut[Double] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, freqN.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, freqN) = args: @unchecked
    stream.LPF(in = in.toDouble, freqN = freqN.toDouble)
  }
}
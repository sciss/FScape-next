/*
 *  MatchLen.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object MatchLen extends ProductReader[MatchLen[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): MatchLen[_, _] = {
    require (arity == 2 && adj == 0)
    val _in   = in.readGE[Any]()
    val _ref  = in.readGE[Any]()
    new MatchLen(_in, _ref)
  }
}
/** A UGen that extends or truncates its first argument to match
  * the length of the second argument. If `in` is shorter than
  * `ref`, it will be padded with the zero element of its number type.
  * If `in` is longer, the trailing elements will be dropped.
  */
final case class MatchLen[A, B](in: GE[A], ref: GE[B]) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, ref.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, ref) = args: @unchecked
    import in.tpe
    val out = stream.MatchLen[in.A, ref.A](in = in.toElem, ref = ref.toElem)
    tpe.mkStreamOut(out)
  }
}
/*
 *  Distinct.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.{Attributes, FlowShape, Inlet}
import de.sciss.fscape.DataType
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream.impl.Handlers._
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.collection.mutable

object Distinct {
  def apply[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "Distinct"

  private type Shp[A] = FlowShape[Buf.E[A], Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FlowShape(
      in  = In [A](s"${stage.name}.in"),
      out = Out[A](s"${stage.name}.out"),
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](shape, layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val seen    = mutable.Set.empty[A]

    private[this] val hIn : InMain  [A]  = InMain [A](this, shape.in )
    private[this] val hOut: OutMain [A]  = OutMain[A](this, shape.out)

    override protected def stopped(): Unit = {
      super.stopped()
      seen.clear()
    }

    protected def onDone(inlet: Inlet[_]): Unit =
      if (hOut.flush()) {
        completeStage()
      }

    protected def process(): Unit = {
      logStream.debug(s"$this process()")

      while (hIn.hasNext && hOut.hasNext) {
        val v     = hIn.next()
        val isNew = seen.add(v)
        if (isNew) hOut.next(v)
      }

      if (hIn.isDone && hOut.flush()) {
        completeStage()
      }
    }
  }
}
/*
 *  PriorityQueue.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{Builder, Out, OutI, StreamIn, StreamInElem, StreamOut}
import de.sciss.lucre.Adjunct.Ord
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object PriorityQueue extends ProductReader[PriorityQueue[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): PriorityQueue[_, _] = {
    require (arity == 3)
    val _keys   = in.readGE[Any]()
    val _values = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _ord    = if (adj == 0) LegacyAdjunct.Ord else {
      require (adj == 1)
      in.readAdjunct[Ord[Any]]()
    }
    new PriorityQueue[Any, Any](_keys, _values, _size)(_ord)
  }
}
/** A sorting UGen that can be thought of as a bounded priority queue.
  * It keeps all data in memory but limits the size to the
  * top `size` items. By its nature, the UGen only starts outputting
  * values once the input signal (`keys`) has finished.
  *
  * Both inputs are "hot" and the queue filling ends when either of
  * `keys` or `values` is finished.
  *
  * @param keys   the sorting keys; higher values mean higher priority
  * @param values the values corresponding with the keys and eventually
  *               output by the UGen. It is well possible to use the
  *               same signal both for `keys` and `values`.
  * @param size   the maximum size of the priority queue.
  */
final case class PriorityQueue[A, B](keys: GE[A], values: GE[B], size: GE.I)(implicit ord: Ord[A])
  extends UGenSource.SingleOut[B] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = ord :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[B] =
    unwrap(this, Vector(keys.expand, values.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[B] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(keys, values, size) = args: @unchecked
    mkStream[keys.A, values.A](keys = keys, values = values, size = size.toInt)  // IntelliJ doesn't get it
  }

  private def mkStream[A1, B1](keys  : StreamInElem[A1], values: StreamInElem[B1], size: OutI)
                              (implicit b: Builder): StreamOut = {
    implicit val kTpe: DataType[A1] = keys  .tpe
    implicit val vTpe: DataType[B1] = values.tpe
    val out: Out[B1] = stream.PriorityQueue[A1, B1](keys = keys.toElem, values = values.toElem, size = size)
    vTpe.mkStreamOut(out)
  }
}
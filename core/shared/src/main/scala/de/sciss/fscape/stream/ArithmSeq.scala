/*
 *  ArithmSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape3}
import de.sciss.fscape.stream.impl.{NodeImpl, SeqGenLogicD, SeqGenLogicI, SeqGenLogicL, StageImpl}

object ArithmSeq {
  def apply[A](start: Out[A], step: Out[A], length: OutL)
              (implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(start , stage.in0)
    b.connect(step  , stage.in1)
    b.connect(length, stage.in2)
    stage.out
  }

  private final val name = "ArithmSeq"

  private type Shp[A] = FanInShape3[Buf.E[A], Buf.E[A], Buf.L, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape = new FanInShape3(
      in0 = In[A] (s"${stage.name}.start" ),
      in1 = In[A] (s"${stage.name}.step"  ),
      in2 = InL   (s"${stage.name}.length"),
      out = Out[A](s"${stage.name}.out"   )
    )

    // handle specialization
    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: NodeImpl[_] = if (tpe.isInt) {
        new SeqGenLogicI(stage.name, shape.asInstanceOf[Shp[Int   ]], layer)((a, b) => a + b)
      } else if (tpe.isLong) {
        new SeqGenLogicL(stage.name, shape.asInstanceOf[Shp[Long  ]], layer)((a, b) => a + b)
      } else /*if (tpe.isDouble)*/ {
        assert (tpe.isDouble)
        new SeqGenLogicD(stage.name, shape.asInstanceOf[Shp[Double]], layer)((a, b) => a + b)
      }

      res.asInstanceOf[NodeImpl[Shape]]
    }
  }
}
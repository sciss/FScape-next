/*
 *  Histogram.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Histogram extends ProductReader[Histogram[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Histogram[_] = {
    require (arity == 6)
    val _in     = in.readGE[Any]()
    val _bins   = in.readGE_I()
    val _lo     = in.readGE[Any]()
    val _hi     = in.readGE[Any]()
    val _mode   = in.readGE_I()
    val _reset  = in.readGE_B()
    val _num    = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new Histogram[Any](_in, _bins, _lo, _hi, _mode, _reset)(_num)
  }
}
/** A UGen that calculates running histogram of an input signal with
  * given boundaries and bin-size. The bins are divided linearly,
  * if another mapping (e.g. exponential) is needed, it must be pre-applied to the input signal.
  *
  * '''Note:''' currently parameter modulation (`bin`, `lo`, `hi`, `mode`, `reset`) is not
  * working correctly.
  *
  * @param in       the input signal
  * @param bins     the number of bins. this is read at initialization time only or when `reset`
  *                 fires
  * @param lo       the lowest bin boundary. input values below this value are clipped.
  *                 this value may be updated (although that is seldom useful).
  * @param hi       the highest bin boundary. input values above this value are clipped.
  *                 this value may be updated (although that is seldom useful).
  * @param mode     if `0` (default), outputs only after `in` has finished, if `1` outputs
  *                 the entire histogram for every input sample.
  * @param reset    when greater than zero, resets the count.
  *
  * @see  [[NormalizeWindow]]
  */
final case class Histogram[A](in: GE[A], bins: GE.I, lo: GE[A] = 0, hi: GE[A] = 1, mode: GE.I = 0,
                              reset: GE.B = false)(implicit num: Num[A])
  extends UGenSource.SingleOut[Int] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    unwrap(this, Vector(in.expand, bins.expand, lo.expand, hi.expand, mode.expand, reset.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, bins, lo, hi, mode, reset) = args: @unchecked
    // XXX TODO: respect numeric type
    stream.Histogram(in = in.toDouble, bins = bins.toInt, lo = lo.toDouble, hi = hi.toDouble,
      mode = mode.toInt, reset = reset.toInt)
  }
}
/*
 *  Ops.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import scala.language.implicitConversions

object Ops extends Ops
trait Ops {
  implicit def geOps1    [A](g: GE[A] ): GEOps1[A] = new GEOps1(g)
  implicit def geOps2    [A](g: GE[A] ): GEOps2[A] = new GEOps2(g)
  implicit def intGeOps2   (i: Int   ): GEOps2[Int]     = new GEOps2(i)
  implicit def doubleGeOps2(d: Double): GEOps2[Double]  = new GEOps2(d)
}

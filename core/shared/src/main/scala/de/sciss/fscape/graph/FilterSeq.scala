/*
 *  FilterSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import akka.stream.Outlet
import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object FilterSeq extends ProductReader[FilterSeq[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FilterSeq[_] = {
    require (arity == 2 && adj == 0)
    val _in   = in.readGE[Any]()
    val _gate = in.readGE_B()
    new FilterSeq(_in, _gate)
  }
}
/** A UGen that filters its input, retaining only those elements in the output sequence
  * for which the predicate `gate` holds.
  *
  * You can think of it as `Gate` but instead of
  * outputting zeroes when the gate is closed, no values are output at all. Consequently,
  * the length of the output stream will differ from the length of the input stream.
  *
  * @see [[Gate]]
  */
final case class FilterSeq[A](in: GE[A], gate: GE.B) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, gate) = args: @unchecked
    import in.tpe
    val out: Outlet[in.Buf] = stream.FilterSeq[in.A](in = in.toElem, gate = gate.toInt)
    tpe.mkStreamOut(out)
  }
}
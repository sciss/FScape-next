/*
 *  Feed.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import de.sciss.fscape.stream.Concat.Shp

object Feed {
  def apply[A](init: Out[A], ref: AnyRef)
              (implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Concat.Stage(name, b.layer)
    val stage: Shp[A] = b.add(stage0)
    b.put(ref, stage) // stage.in1 is connected later
    b.connect(init, stage.in0)
    stage.out
  }

  def back[A](in: Out[A], ref: AnyRef)(implicit b: Builder, tpe: DataType[A]): Unit = {
    val stage: Shp[A] = b.get(ref).getOrElse(sys.error("Feed element not found"))
    b.connect(in, stage.in1)
  }

  private final val name = "Feedback"
}
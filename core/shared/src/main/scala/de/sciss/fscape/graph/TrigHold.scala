/*
 *  TrigHold.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object TrigHold extends ProductReader[TrigHold[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): TrigHold[_] = {
    require (arity == 3)
    val _in     = in.readGE_B()
    val _length = in.readGE[Any]()
    val _clear  = in.readGE_B()
    require (adj == 1 || adj == 0)
    val _numL: NumInt[Any] = if (adj == 0) LegacyAdjunct.NumInt else in.readAdjunct()
    new TrigHold[Any](_in, _length, _clear)(_numL)
  }
}
/** A UGen that holds an input trigger signal for a given duration.
  * When a trigger is received from `in`, the output changes from zero
  * to one for the given `length` amount of frames. If a new trigger
  * arrives within this period, the internal length counter is reset
  * (the high state is kept for further `length` frames).
  *
  * @param  in      an input trigger or gate signal. Whenever the signal
  *                 is positive, the internal hold state is reset. If a strict
  *                 trigger is needed instead of a gate signal, a `Trig` UGen
  *                 can be inserted.
  * @param  length  the number of sample frames to hold the high output once
  *                 a high input is received.
  *                 A new value is polled whenever `in` is high.
  * @param  clear   an auxiliary trigger or gate signal that resets the
  *                 output to low if there is currently no high `in`.
  *                 This signal is read synchronously with `in.`
  *                 If both `clear` and `in` are high at a given point,
  *                 then the UGen outputs high but has its length counter
  *                 reset.
  */
final case class TrigHold[L](in: GE.B, length: GE[L], clear: GE.B = false)(implicit numL: NumInt[L])
  extends UGenSource.SingleOut[Boolean] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    unwrap(this, Vector(in.expand, length.expand, clear.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, length, clear) = args: @unchecked
    stream.TrigHold(in = in.toInt, length = length.toLong, clear = clear.toInt)
  }
}

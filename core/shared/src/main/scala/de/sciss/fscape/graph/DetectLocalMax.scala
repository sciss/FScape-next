/*
 *  DetectLocalMax.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Ord
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object DetectLocalMax extends ProductReader[DetectLocalMax[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DetectLocalMax[_] = {
    require (arity == 2)
    val _in   = in.readGE[Any]()
    val _size = in.readGE_I()
    val _ord  = if (adj == 0) LegacyAdjunct.Ord else {
      require (adj == 1)
      in.readAdjunct[Ord[Any]]()
    }
    new DetectLocalMax[Any](_in, _size)(_ord)
  }
}
/** A UGen that outputs triggers for local maxima within
  * a sliding window. If multiple local maxima occur
  * within the current window, only the one with
  * the largest value will cause the trigger.
  *
  * By definition, the first and last value in the input stream
  * cannot qualify for local maxima.
  *
  * If a local maximum spreads across multiple samples (adjacent values
  * are the same), the location of the last sample is reported.
  *
  * @param in     the signal to analyze for local maxima
  * @param size   the sliding window size. Each two
  *               emitted triggers are spaced apart at least
  *               by `size` frames.
  */
final case class DetectLocalMax[A](in: GE[A], size: GE.I)(implicit ord: Ord[A])
  extends UGenSource.SingleOut[Boolean] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = ord :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    unwrap(this, Vector(in.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size) = args: @unchecked
    val inE = in.toElem
    import in.tpe
    stream.DetectLocalMax[in.A](in = inE, size = size.toInt)
  }
}
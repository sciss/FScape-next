/*
 *  Gate.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.annotation.tailrec
import scala.math.min

object Gate {
  def apply[A](in: Out[A], gate: OutI)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in  , stage.in0)
    b.connect(gate, stage.in1)
    stage.out
  }

  private final val name = "Gate"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.I, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A] (s"${stage.name}.in"  ),
      in1 = InI    (s"${stage.name}.gate"),
      out = Out[A] (s"${stage.name}.out" )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int]], layer)
      } else if (tpe.isLong) {
        new Logic[Long  ](shape.asInstanceOf[Shp[Long]], layer)
      } else if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double]], layer)
      } else {
        new Logic[A](shape, layer)  // not specialized
      }

      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer)
                                                 (implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hIn   = Handlers.InMain [A](this, shape.in0)
    private[this] val hGate = Handlers.InIAux    (this, shape.in1)()
    private[this] val hOut  = Handlers.OutMain[A](this, shape.out)

    private[this] val zero  = tpe.zero

    protected def onDone(inlet: Inlet[_]): Unit =
      if (hOut.flush()) completeStage()

    @tailrec
    protected def process(): Unit = {
      val rem     = min(hIn.available, min(hOut.available, hGate.available))
      if (rem == 0) return

      val in      = hIn .array
      val out     = hOut.array
      var inOff   = hIn .offset
      var outOff  = hOut.offset
      val stop0   = inOff + rem
      while (inOff < stop0) {
        val gate  = hGate.next() > 0
        val v0    = if (gate) in(inOff) else zero // 0.0
        out(outOff) = v0
        inOff  += 1
        outOff += 1
      }
      hIn .advance(rem)
      hOut.advance(rem)

      if (hIn.isDone) {
        if (hOut.flush()) completeStage()
        return
      }
      process()
    }
  }
}
/*
 *  ZipWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object ZipWindow extends ProductReader[ZipWindow[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ZipWindow[_] = {
    require (arity == 3 && adj == 0)
    val _a    = in.readGE[Any]()
    val _b    = in.readGE[Any]()
    val _size = in.readGE_I()
    new ZipWindow(_a, _b, _size)
  }
}
final case class ZipWindow[A](a: GE[A], b: GE[A], size: GE.I = 1) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(a.expand, b.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit builder: stream.Builder): StreamOut = {
    val Vec(a, b, size) = args: @unchecked
    if (a.isDouble || b.isDouble) {
      stream.ZipWindowN[Double](in = a.toDouble :: b.toDouble :: Nil, size = size.toInt)
    } else if (a.isLong || b.isLong) {
      stream.ZipWindowN[Long  ](in = a.toLong   :: b.toLong   :: Nil, size = size.toInt)
    } else if (a.isInt) {
      stream.ZipWindowN[Int   ](in = a.toInt    :: b.toInt    :: Nil, size = size.toInt)
    } else {
      val aC = a.cast[A]
      val bC = b.cast[A]
      import aC.tpe
      val out = stream.ZipWindowN[A](in = aC.toElem :: bC.toElem :: Nil, size = size.toInt)
      tpe.mkStreamOut(out)
    }
  }
}

object ZipWindowN extends ProductReader[ZipWindowN[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ZipWindowN[_] = {
    require (arity == 2 && adj == 0)
    type A = Any
    val _in   = in.readGE[A]()
    val _size = in.readGE_I()
    new ZipWindowN(_in, _size)
  }
}
final case class ZipWindowN[A](in: GE[A], size: GE.I = 1) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, size.expand +: in.expand.outputs)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit builder: stream.Builder): StreamOut = {
    val size +: in = args: @unchecked
    if (in.exists(_.isDouble)) {
      stream.ZipWindowN[Double](in = in.map(_.toDouble), size = size.toInt)
    } else if (in.exists(_.isLong)) {
      stream.ZipWindowN[Long  ](in = in.map(_.toLong  ), size = size.toInt)
    } else if (in.exists(_.isInt)) {
      stream.ZipWindowN[Int   ](in = in.map(_.toInt   ), size = size.toInt)
    } else {
      val inC = in.map(_.cast[A])
      implicit val tpe: DataType[A] = inC.head.tpe
      val out = stream.ZipWindowN[A](in = inC.map(_.toElem), size = size.toInt)
      tpe.mkStreamOut(out)
    }
  }
}
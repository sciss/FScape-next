/*
 *  ValueSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, Inlet, SourceShape}
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

object ValueSeq {
  def int(elems: Array[Int])(implicit b: Builder): OutI = {
    val stage0  = new Stage[Int](b.layer, elems)
    val stage   = b.add(stage0)
    stage.out
  }

  def long(elems: Array[Long])(implicit b: Builder): OutL = {
    val stage0  = new Stage[Long](b.layer, elems)
    val stage   = b.add(stage0)
    stage.out
  }

  def double(elems: Array[Double])(implicit b: Builder): OutD = {
    val stage0  = new Stage[Double](b.layer, elems)
    val stage   = b.add(stage0)
    stage.out
  }

  def apply[A](elems: Array[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer, elems)
    val stage   = b.add(stage0)
    stage.out
  }

  private final val name = "ValueSeq"

  private type Shp[A] = SourceShape[Buf.E[A]]

  private final class Stage[A](layer: Layer, elems: Array[A])(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new SourceShape(
      out = Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double ]], layer, elems.asInstanceOf[Array[Double ]])
      } else if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int    ]], layer, elems.asInstanceOf[Array[Int    ]])
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape.asInstanceOf[Shp[Long   ]], layer, elems.asInstanceOf[Array[Long   ]])
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer, elems: Array[A])
                                               (implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hOut  = Handlers.OutMain[A](this, shape.out)
    private[this] var index = 0

    protected def onDone(inlet: Inlet[_]): Unit = assert(false)

    protected def process(): Unit = {
      var i     = index
      val rem   = math.min(hOut.available, elems.length - i)
      val buf   = hOut.array
      var off   = hOut.offset
      val stop  = off + rem
      while (off < stop) {
        buf(off) = elems(i)
        i   += 1
        off += 1
      }
      hOut.advance(rem)
      index = i
      if (i == elems.length && hOut.flush()) completeStage()
    }
  }
}
/*
 *  ValueSeq.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

/** Loops the given values. */
object ValueSeq {
  def apply(elem0: Int    , rest: Int     *): GE.I = ValueIntSeq    (elem0 +: rest: _*)
  def apply(elem0: Long   , rest: Long    *): GE.L = ValueLongSeq   (elem0 +: rest: _*)
  def apply(elem0: Double , rest: Double  *): GE.D = ValueDoubleSeq (elem0 +: rest: _*)
  def apply(elem0: Boolean, rest: Boolean *): GE.B = ValueBooleanSeq(elem0 +: rest: _*)
}

object ValueIntSeq extends ProductReader[ValueIntSeq] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ValueIntSeq = {
    require (arity == 1 && adj == 0)
    val _elems = in.readIntVec()
    new ValueIntSeq(_elems: _*)
  }
}
/** Loops the given values. */
final case class ValueIntSeq(elems: Int*) extends UGenSource.SingleOut[Int] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    makeUGen(elems.iterator.map(x => x: UGenIn[Int]).toIndexedSeq)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val elems: Array[Int] = args.iterator.collect {
      case c: ConstI => c.value
    } .toArray

    stream.ValueSeq.int(elems)
  }
}

object ValueBooleanSeq extends ProductReader[ValueBooleanSeq] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ValueBooleanSeq = {
    require (arity == 1 && adj == 0)
    val _elems = in.readVec(in.readBoolean())
    new ValueBooleanSeq(_elems: _*)
  }
}
/** Loops the given values. */
final case class ValueBooleanSeq(elems: Boolean*) extends UGenSource.SingleOut[Boolean] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    makeUGen(elems.iterator.map(x => x: UGenIn[Boolean]).toIndexedSeq)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val elems: Array[Int] = args.iterator.collect {
      case c: ConstB => c.intValue
    } .toArray

    stream.ValueSeq.int(elems)
  }
}

object ValueLongSeq extends ProductReader[ValueLongSeq] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ValueLongSeq = {
    require (arity == 1 && adj == 0)
    val _elems = in.readVec(in.readLong())
    new ValueLongSeq(_elems: _*)
  }
}
/** Loops the given values. */
final case class ValueLongSeq(elems: Long*) extends UGenSource.SingleOut[Long] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    makeUGen(elems.iterator.map(x => x: UGenIn[Long]).toIndexedSeq)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val elems: Array[Long] = args.iterator.collect {
      case c: ConstL => c.value
    } .toArray

    stream.ValueSeq.long(elems)
  }
}

object ValueDoubleSeq extends ProductReader[ValueDoubleSeq] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ValueDoubleSeq = {
    require (arity == 1 && adj == 0)
    val _elems = in.readDoubleVec()
    new ValueDoubleSeq(_elems: _*)
  }
}
/** Loops the given values. */
final case class ValueDoubleSeq(elems: Double*) extends UGenSource.SingleOut[Double] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    makeUGen(elems.iterator.map(x => x: UGenIn[Double]).toIndexedSeq)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val elems: Array[Double] = args.iterator.collect {
      case c: ConstD => c.value
    } .toArray

    stream.ValueSeq.double(elems)
  }
}
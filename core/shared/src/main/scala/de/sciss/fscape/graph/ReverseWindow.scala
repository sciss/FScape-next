/*
 *  ReverseWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object ReverseWindow extends ProductReader[ReverseWindow[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ReverseWindow[_] = {
    require (arity == 3 && adj == 0)
    val _in     = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _clump  = in.readGE_I()
    new ReverseWindow[Any](_in, _size, _clump)
  }
}
/** Reverses the (clumped) elements of input windows.
  *
  * E.g. `ReverseWindow(ArithmSeq(1, 1, 9), 3)` gives `7,8,9, 4,5,6, 1,2,3`.
  * The behavior when `size % clump != 0` is to clump "from both sides" of
  * the window, e.g. `ReverseWindow(ArithmSeq(1, 1, 9), 4)` gives `6,7,8,9, 5, 1,2,3,4`.
  *
  * @param  in      the window'ed signal to reverse
  * @param  size    the window size
  * @param  clump   the grouping size of window elements
  */
final case class ReverseWindow[A](in: GE[A], size: GE.I, clump: GE.I = 1) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, clump.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, clump) = args: @unchecked
    import in.tpe
    val out = stream.ReverseWindow[in.A](in = in.toElem, size = size.toInt, clump = clump.toInt)
    tpe.mkStreamOut(out)
  }
}
/*
 *  ProgressFrames.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.Attributes
import akka.stream.stage.InHandler
import de.sciss.fscape.stream.impl.shapes.SinkShape2
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object ProgressFrames {
  def apply[A](in: Out[A], numFrames: OutL, label: String)(implicit b: Builder): Unit = {
    val stage0  = new Stage[A](layer = b.layer, label = label)
    val stage   = b.add(stage0)
    b.connect(in        , stage.in0)
    b.connect(numFrames , stage.in1)
  }

  private final val name = "ProgressFrames"

  private type Shp[A] = SinkShape2[Buf.E[A], Buf.L]

  private final class Stage[A](layer: Layer, label: String)(implicit a: Allocator)
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = SinkShape2(
      in0 = In[A] (s"${stage.name}.in"),
      in1 = InL   (s"${stage.name}.numFrames")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic(layer = layer, shape = shape, label = label)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer, label: String)(implicit a: Allocator)
    extends NodeImpl(name, layer, shape) with InHandler { logic =>

    private[this] val key       = control.mkProgress(label)
    private[this] var hasFrames = false

    private[this] var numFrames: Long = _

    private[this] var read = 0L

    private[this] var lastTime  = 0L
    private[this] var lastP500  = -1

    override def toString = s"${logic.name}-L($label)"

    private object FramesHandler extends InHandler {
      def onPush(): Unit = {
        val b = grab(shape.in1)
        if (!hasFrames && b.size > 0) {
          numFrames = math.max(1L, b.buf(0))
//          println(s"HAS_FRAMES $numFrames")
          hasFrames = true
          updateFraction()
        }
        b.release()
        tryPull(shape.in1)
      }

      override def onUpstreamFinish(): Unit =
//        if (isAvailable(shape.in1)) onPush()
//        else
        if (!hasFrames) {
          super.onUpstreamFinish()
        }
    }

    private def updateFraction(): Unit = {
      val p500_0  = (read * 500 / numFrames).toInt
      val p500    = if (p500_0 <= 500) p500_0 else 500
//      println(s"updateFraction() $p500")
      if (p500 > lastP500) {
        val time = System.currentTimeMillis()
        if (p500 == 500 || time - lastTime > 100) { // always report the 100% mark
          lastP500  = p500
          lastTime  = time
          val f     = p500 * 0.002
          control.setProgress(key, f)
        }
      }
    }

    def onPush(): Unit = {
      val b = grab(shape.in0)
      read += b.size
      b.release()
      if (hasFrames) updateFraction()
      tryPull(shape.in0)
    }

//    override def onUpstreamFinish(): Unit = {
//      if (isAvailable(shape.in0)) onPush()
//      super.onUpstreamFinish()
//    }

    setHandler(shape.in0, this)
    setHandler(shape.in1, FramesHandler)
  }
}
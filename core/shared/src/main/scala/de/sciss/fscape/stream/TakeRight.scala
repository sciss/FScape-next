/*
 *  TakeRight.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.graph.ConstI
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.annotation.tailrec
import scala.math.{max, min}

object TakeRight {
  def last[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val length = ConstI(1).toInt
    apply[A](in = in, length = length)
  }

  def apply[A](in: Out[A], length: OutI)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in    , stage.in0)
    b.connect(length, stage.in1)
    stage.out
  }

  private final val name = "TakeRight"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.I, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.in"    ),
      in1 = InI   (s"${stage.name}.length"),
      out = Out[A](s"${stage.name}.out"   )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](shape, layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hIn   = Handlers.InMain [A](this, shape.in0)
    private[this] val hLen  = Handlers.InIAux    (this, shape.in1)(max(0, _))
    private[this] val hOut  = Handlers.OutMain[A](this, shape.out)

    private[this] var len     : Int       = _
    private[this] var bufSize : Int       = _
    private[this] var bufWin  : Array[A]  = _     // circular
    private[this] var bufWritten = 0L

    private[this] var bufOff    : Int = 0
    private[this] var bufRemain : Int = _

    private[this] var state = 0 // 0 get length, 1 read into buffer, 2 write from buffer

    override protected def stopped(): Unit = {
      super.stopped()
      bufWin = null
    }

    protected def onDone(inlet: Inlet[_]): Unit =
      if (state == 0) {
        completeStage()
      } else {
        assert (state == 1)
        process()
      }

    def process(): Unit = {
      logStream.debug(s"process() $this state = $state")

      if (state == 0) {
        if (!hLen.hasNext) return
        len       = hLen.next()
        bufSize   = max(len, allocator.blockSize)
        bufWin    = tpe.newArray(bufSize)
        state     = 1
      }
      if (state == 1) {
        readIntoBuffer()
        if (!hIn.isDone) return
        bufRemain = min(bufWritten, len).toInt
        bufOff    = (max(0L, bufWritten - len) % bufSize).toInt
        state     = 2
      }

      writeFromBuffer()
      if (bufRemain == 0) {
        if (hOut.flush()) completeStage()
      }
    }

    @tailrec
    private def readIntoBuffer(): Unit = {
      val rem     = hIn.available
      if (rem == 0) return

      val chunk   = min(rem, bufSize) // maximum one full buffer, as we loop the method
      val chunk1  = min(chunk, bufSize - bufOff)
      if (chunk1 > 0) {
        hIn.nextN(bufWin, bufOff, chunk1)
        bufOff = (bufOff + chunk1) % bufSize
      }
      val chunk2  = chunk - chunk1
      if (chunk2 > 0) {
        hIn.nextN(bufWin, bufOff, chunk2)
        bufOff = (bufOff + chunk2) % bufSize
      }
      bufWritten += rem
      readIntoBuffer()
    }

    private def writeFromBuffer(): Unit = {
      val rem     = min(bufRemain, hOut.available)
      if (rem == 0) return

      val chunk1  = min(rem, bufSize - bufOff)
      hOut.nextN(bufWin, bufOff, chunk1)
      bufOff      = (bufOff + chunk1) % bufSize
      val chunk2  = rem - chunk1
      if (chunk2 > 0) {
        hOut.nextN(bufWin, bufOff, chunk2)
        bufOff = (bufOff + chunk2) % bufSize
      }

      bufRemain -= rem
    }
  }
}
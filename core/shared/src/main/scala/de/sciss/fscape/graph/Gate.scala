/*
 *  Gate.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Gate extends ProductReader[Gate[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Gate[_] = {
    require (arity == 2)
    val _in   = in.readGE[Any]()
    val _gate = in.readGE_B()
    new Gate[Any](_in, _gate)
  }
}
/** A UGen that passes through its input while the gate is open, and outputs zero while the gate is closed.
  *
  * @see [[FilterSeq]]
  * @see [[Latch]]
  */
final case class Gate[A](in: GE[A], gate: GE.B) extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, gate) = args: @unchecked
    import in.tpe
    val out = stream.Gate[in.A](in = in.toElem, gate = gate.toInt)
    tpe.mkStreamOut(out)
  }
}
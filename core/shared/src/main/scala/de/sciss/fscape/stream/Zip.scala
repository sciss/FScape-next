/*
 *  ZipWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream.impl.Handlers._
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

object Zip {
  def apply[A](a: Out[A], b: Out[A])(implicit builder: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](builder.layer)
    val stage   = builder.add(stage0)
    builder.connect(a, stage.in0)
    builder.connect(b, stage.in1)
    stage.out
  }

  private final val name = "Zip"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.E[A], Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.a"  ),
      in1 = In [A](s"${stage.name}.b"  ),
      out = Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new LogicD(shape.asInstanceOf[Shp[Double]], layer)
      } else if (tpe.isInt) {
        new LogicI(shape.asInstanceOf[Shp[Int   ]], layer)
      } else {
        assert (tpe.isLong)
        new LogicL(shape.asInstanceOf[Shp[Long  ]], layer)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private abstract class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator)
    extends Handlers(name, layer, shape) {

    protected def hInA: InMain  [A]
    protected def hInB: InMain  [A]
    protected def hOut: OutMain [A]

    final protected def onDone(inlet: Inlet[_]): Unit =
      if (hOut.flush()) completeStage()

    protected def run(n: Int): Unit

    final def process(): Unit = {
      logStream.debug(s"process() $this")

      while (true) {
        val rem = math.min(hInA.available, math.min(hInB.available, hOut.available >>> 1))
        if (rem == 0) return

        run(rem)

        if (hInA.isDone || hInB.isDone) {
          if (hOut.flush()) completeStage()
          return
        }
      }
    }
  }

  private final class LogicD(shape: Shp[Double], layer: Layer)
                            (implicit a: Allocator)
    extends Logic[Double](shape, layer) {

    override protected val hInA: InDMain  = InDMain (this, shape.in0)
    override protected val hInB: InDMain  = InDMain (this, shape.in1)
    override protected val hOut: OutDMain = OutDMain(this, shape.out)

    def run(n: Int): Unit = {
      val a   = hInA
      val b   = hInB
      val out = hOut
      var i = 0
      while (i < n) {
        out.next(a.next())
        out.next(b.next())
        i += 1
      }
    }
  }

  private final class LogicI(shape: Shp[Int], layer: Layer)
                            (implicit a: Allocator)
    extends Logic[Int](shape, layer) {

    override protected val hInA: InIMain  = InIMain (this, shape.in0)
    override protected val hInB: InIMain  = InIMain (this, shape.in1)
    override protected val hOut: OutIMain = OutIMain(this, shape.out)

    def run(n: Int): Unit = {
      val a   = hInA
      val b   = hInB
      val out = hOut
      var i = 0
      while (i < n) {
        out.next(a.next())
        out.next(b.next())
        i += 1
      }
    }
  }

  private final class LogicL(shape: Shp[Long], layer: Layer)
                            (implicit a: Allocator)
    extends Logic[Long](shape, layer) {

    override protected val hInA: InLMain  = InLMain (this, shape.in0)
    override protected val hInB: InLMain  = InLMain (this, shape.in1)
    override protected val hOut: OutLMain = OutLMain(this, shape.out)

    def run(n: Int): Unit = {
      val a   = hInA
      val b   = hInB
      val out = hOut
      var i = 0
      while (i < n) {
        out.next(a.next())
        out.next(b.next())
        i += 1
      }
    }
  }
}

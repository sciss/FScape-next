/*
 *  WindowMaxIndex.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Ord
import de.sciss.lucre.{Adjunct, ProductWithAdjuncts}

import scala.collection.immutable.{IndexedSeq => Vec}

object WindowMaxIndex extends ProductReader[WindowMaxIndex[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): WindowMaxIndex[_] = {
    require (arity == 2)
    val _in   = in.readGE[Any]()
    val _size = in.readGE_I()
    val _ord  = if (adj == 0) LegacyAdjunct.Ord else {
      require (adj == 1)
      in.readAdjunct[Ord[Any]]()
    }
    new WindowMaxIndex[Any](_in, _size)(_ord)
  }
}
/** A UGen that determines for each input window the index of the maximum element.
  * It outputs one integer value per window; if multiple elements have the same
  * value, the index of the first element is reported (notably zero if the window
  * contains only identical elements).
  *
  * @param in     the input signal.
  * @param size   the window size.
  */
final case class WindowMaxIndex[A](in: GE[A], size: GE.I)(implicit ord: Ord[A])
  extends UGenSource.SingleOut[Int] with ProductWithAdjuncts {

  override def adjuncts: List[Adjunct] = ord :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    unwrap(this, Vector(in.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size) = args: @unchecked
    import in.tpe
    stream.WindowMaxIndex[in.A](in = in.toElem, size = size.toInt)
  }
}

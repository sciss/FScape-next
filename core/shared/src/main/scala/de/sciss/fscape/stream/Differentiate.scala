/*
 *  Differentiate.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FlowShape}
import de.sciss.fscape.stream.impl.logic.FilterIn1Out1
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object Differentiate {
  def apply[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "Differentiate"

  private type Shp[A] = FlowShape[Buf.E[A], Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FlowShape(
      in  = In [A](s"${stage.name}.in"  ),
      out = Out[A](s"${stage.name}.out" )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double ]], layer)(_ - _)
      } else if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int    ]], layer)(_ - _)
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape.asInstanceOf[Shp[Long   ]], layer)(_ - _)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer)(diff: (A, A) => A)
                                                 (implicit a: Allocator, tpe: DataType[A])
    extends FilterIn1Out1[A, A](name, layer, shape) {

    private[this] var xPrev: A = tpe.zero

    protected def run(in: Array[A], inOff0: Int, out: Array[A], outOff0: Int, n: Int): Unit = {
      val stop    = inOff0 + n
      var inOff   = inOff0
      var outOff  = outOff0
      var x1      = xPrev
      while (inOff < stop) {
        val x0      = in(inOff)
        val y0      = diff(x0, x1)
        out(outOff) = y0
        x1          = x0
        inOff      += 1
        outOff     += 1
      }
      xPrev = x1
    }
  }
}
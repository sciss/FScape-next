/*
 *  UnaryOp.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.graph.UnaryOp.Op
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{Builder, StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{BooleanTop, IntTop, Num, NumBool, NumInt, Scalar, ScalarToNum, ToNum}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}
import de.sciss.numbers.{DoubleFunctions => rd, DoubleFunctions2 => rd2, IntFunctions => ri, IntFunctions2 => ri2, LongFunctions => rl, LongFunctions2 => rl2}

import scala.annotation.switch
import scala.collection.immutable.{IndexedSeq => Vec}

object UnaryOp extends ProductReader[UGenSource.SingleOut[_]] {
  object Op {
    def legacy(id: Int): Op[_, _] = (id: @switch) match {
      case Neg            .id => Neg            ()(LegacyAdjunct.Num)
      case Not            .id => Not            ()(LegacyAdjunct.NumBool)
      case BitNot         .id => BitNot         ()(LegacyAdjunct.NumInt)
      case Abs            .id => Abs            ()(LegacyAdjunct.Num)
      case ToDouble       .id => ToDouble       ()(LegacyAdjunct.ScalarToNum)
      case ToInt          .id => ToInt          ()(LegacyAdjunct.ScalarToNum)
      case Ceil           .id => Ceil           ()
      case Floor          .id => Floor          ()
      case Frac           .id => Frac           ()
      case Signum         .id => Signum         ()(LegacyAdjunct.Num)
      case Squared        .id => Squared        ()(LegacyAdjunct.Num)
      case Cubed          .id => Cubed          ()(LegacyAdjunct.Num)
      case Sqrt           .id => Sqrt           ()
      case Exp            .id => Exp            ()
      case Reciprocal     .id => Reciprocal     ()
      case MidiCps        .id => MidiCps        ()
      case CpsMidi        .id => CpsMidi        ()
      case MidiRatio      .id => MidiRatio      ()
      case RatioMidi      .id => RatioMidi      ()
      case DbAmp          .id => DbAmp          ()
      case AmpDb          .id => AmpDb          ()
      case OctCps         .id => OctCps         ()
      case CpsOct         .id => CpsOct         ()
      case Log            .id => Log            ()
      case Log2           .id => Log2           ()
      case Log10          .id => Log10          ()
      case Sin            .id => Sin            ()
      case Cos            .id => Cos            ()
      case Tan            .id => Tan            ()
      case Asin           .id => Asin           ()
      case Acos           .id => Acos           ()
      case Atan           .id => Atan           ()
      case Sinh           .id => Sinh           ()
      case Cosh           .id => Cosh           ()
      case Tanh           .id => Tanh           ()
      //      case Distort    .id => Distort
      //      case Softclip   .id => Softclip
      //      case Ramp       .id => Ramp
      //      case Scurve     .id => Scurve
      case IsNaN          .id => IsNaN          ()
      case NextPowerOfTwo .id => NextPowerOfTwo ()(LegacyAdjunct.NumInt)
      case ToLong         .id => ToLong         ()(LegacyAdjunct.ScalarToNum)
    }
  }

  sealed trait Op[A, B] extends Product {
    op =>

//    /** The default converts to `Double`, but specific operators
//      * may better preserve semantics and precision for other types such as `Int` and `Long`.
//      */
//    def apply(a: Constant[A1]): Constant[A2] // = ConstantD(funDD(a.doubleValue))

    def apply: A => B

    def name: String = plainName.capitalize

    override def productPrefix = s"UnaryOp$$$name"

    private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: stream.Builder): StreamOut

    final def make(a: GE[A]): GE[B] = a match {
      case v: Const[A]  => Const(apply(v.value))
      case _            => UnaryOp(op, a)
    }

    private def plainName: String = {
      val cn = getClass.getName
      val sz = cn.length
      val i  = cn.indexOf('$') + 1
      cn.substring(i, if (cn.charAt(sz - 1) == '$') sz - 1 else sz)
    }
  }

//  abstract class Op[A1, A2] extends Op[A1, A2] {
//    override def productPrefix = s"UnaryOp$$$name"
//
////    override def toString: String = name
////
////    def name: String
//  }

  type Adjuncts = scala.List[LAdjunct]

  abstract class OpSameBase[A] extends Op[A, A] { op =>
    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in.tpe
      val out = stream.UnaryOp[A, A](name, op.apply, in = in.toElem)
      tpe.mkStreamOut(out)
    }
  }


  /** Legacy */
  sealed trait OpDD /*extends Op*/ {
    def funDD: Double => Double
  }

  /** Legacy */
  sealed trait OpII /*extends Op*/ {
    def funII: Int => Int
  }

  /** Legacy */
  sealed trait OpLL /*extends Op*/ {
    def funLL: Long => Long
  }

  /** Legacy */
  sealed trait OpDI /*extends Op*/ {
    def funDI: Double => Int
  }

  /** Legacy */
  sealed trait OpDL /*extends Op*/ {
    def funDL: Double => Long
  }

  /** Legacy */
  sealed trait OpID /*extends Op*/ {
    def funID: Int => Double
  }

  /** Legacy */
  sealed trait OpLD /*extends Op*/ {
    def funLD: Long => Double
  }

  /** Legacy */
  sealed trait OpIL /*extends Op*/ {
    def funIL: Int => Long
  }

  /** Legacy */
  sealed trait OpLI /*extends Op*/ {
    def funLI: Long => Int
  }

  abstract class OpSame[A] extends OpSameBase[A] with OpDD with OpII with OpLL

  object Neg extends ProductReader[Neg[_]] {
    final val id = 0

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Neg[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Neg[Any]()(_num)
    }
  }
  final case class Neg[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: A => A = num.negate

    override def adjuncts: Adjuncts = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = -_
    override val funII: Int    => Int    = -_
    override val funLL: Long   => Long   = -_
  }

  object Not extends ProductReader[Not[_]] {
    final val id = 1

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Not[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumBool[Any] = in.readAdjunct()
      new Not[Any]()(_num)
    }
  }
  final case class Not[A]()(implicit num: NumBool[A])
    extends OpSameBase[A] with OpDD with OpII with ProductWithAdjuncts {

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val negateF: A => A = num.asInstanceOf[NumBool[_]] match {
      case BooleanTop => (IntTop.negate _).asInstanceOf[A => A] // .asInstanceOf[NumBool[A]]
      case _          => num.negate
    }

    override def apply: A => A = negateF // .negate

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in.tpe
      val out = stream.UnaryOp[A, A](name, negateF /*num.negate*/, in = in.toElem)
      tpe.mkStreamOut(out)
    }

    override def adjuncts: Adjuncts = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => if (a == 0.0 ) 1.0 else 0.0
    override val funII: Int    => Int    = a => if (a == 0   ) 1   else 0
  }

  object BitNot extends ProductReader[BitNot[_]] {
    final val id = 4

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): BitNot[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new BitNot[Any]()(_num)
    }
  }
  final case class BitNot[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: A => A = num.not

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in.tpe
      val out = stream.UnaryOp[A, A](name, num.not, in = in.toElem)
      tpe.mkStreamOut(out)
    }

    override def adjuncts: Adjuncts = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => (~a.toLong).toDouble
    override val funII: Int    => Int    = a => ~a
    override val funLL: Long   => Long   = a => ~a
  }

  object Abs extends ProductReader[Abs[_]] {
    final val id = 5

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Abs[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Abs[Any]()(_num)
    }
  }
  final case class Abs[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    def apply: A => A = num.abs

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in.tpe
      val out = stream.UnaryOp[A, A](name, num.abs, in = in.toElem)
      tpe.mkStreamOut(out)
    }

    override def adjuncts: Adjuncts = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.abs(a)
    override val funII: Int    => Int    = a => ri.abs(a)
    override val funLL: Long   => Long   = a => rl.abs(a)
  }

  object ToDouble extends ProductReader[ToDouble[_]] {
    final val id = 6

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ToDouble[_] = {
      require (arity == 0 && adj == 1)
      val _num: ScalarToNum[Any] = in.readAdjunct()
      new ToDouble[Any]()(_num)
    }
  }
  final case class ToDouble[A]()(implicit to: ScalarToNum[A])
    extends Op[A, Double] with OpDD with OpID with OpLD with ProductWithAdjuncts {

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val toDoubleF: A => Double = to.asInstanceOf[ToNum[_] with Scalar[_]] match {
      case BooleanTop => (IntTop.toDouble _).asInstanceOf[A => Double] // .asInstanceOf[ScalarToNum[A]]
      case _          => to.toDouble
    }

    override def apply: A => Double = toDoubleF // .toDouble

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = in.toDouble

    override def adjuncts: Adjuncts = to :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => a
    override val funID: Int    => Double = a => a.toDouble
    override val funLD: Long   => Double = a => a.toDouble
  }

  object ToInt extends ProductReader[ToInt[_]] {
    final val id = 7

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ToInt[_] = {
      require (arity == 0 && adj == 1)
      val _num: ScalarToNum[Any] = in.readAdjunct()
      new ToInt[Any]()(_num)
    }
  }
  case class ToInt[A]()(implicit to: ScalarToNum[A])
    extends Op[A, Int] with OpDD with OpDI with OpLI with OpII with ProductWithAdjuncts {

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val toC = to.asInstanceOf[ToNum[_] with Scalar[_]] match {
      case BooleanTop => IntTop.asInstanceOf[ScalarToNum[A]]
      case _          => to
    }

    override def apply: A => Int = toC.toInt

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = in.toInt

    override def adjuncts: Adjuncts = to :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => a.toInt.toDouble
    override val funDI: Double => Int    = a => a.toInt
    override val funLI: Long   => Int    = a => a.toInt
    override val funII: Int    => Int    = a => a
  }

  abstract class OpDouble extends OpSameBase[Double] with OpDD { op =>
    override private[UnaryOp] def makeStream(in: StreamIn.T[Double])(implicit b: Builder): StreamOut =
      stream.UnaryOp(name, op.apply, in = in.toElem)
  }

  object Ceil extends ProductReader[Ceil] {
    final val id = 8

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ceil = {
      require (arity == 0 && adj == 0)
      new Ceil
    }
  }
  case class Ceil() extends OpDouble {
    override def apply: Double => Double = rd.ceil

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.ceil(a)
  }

  object Floor extends ProductReader[Floor] {
    final val id = 9

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Floor = {
      require (arity == 0 && adj == 0)
      new Floor
    }
  }
  case class Floor() extends OpDouble {
    override def apply: Double => Double = rd.floor

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.floor(a)
  }

  object Frac extends ProductReader[Frac] {
    final val id = 10

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Frac = {
      require (arity == 0 && adj == 0)
      new Frac
    }
  }
  case class Frac() extends OpDouble {
    override def apply: Double => Double = rd.frac

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.frac(a)
  }

  object Signum extends ProductReader[Signum[_]] {
    final val id = 11

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Signum[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Signum[Any]()(_num)
    }
  }
  case class Signum[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: A => A = num.signum

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.signum(a)
    override val funII: Int    => Int    = a => ri.signum(a)
    override val funLL: Long   => Long   = a => rl.signum(a)
  }

  object Squared extends ProductReader[Squared[_]] {
    final val id = 12

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Squared[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Squared[Any]()(_num)
    }
  }
  case class Squared[A]()(implicit num: Num[A]) extends OpSameBase[A]
    with OpDD with OpIL with OpLL with ProductWithAdjuncts {

    override def apply: A => A = num.squared

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.squared(a)
    override val funIL: Int    => Long   = a => ri.squared(a)
    override val funLL: Long   => Long   = a => rl.squared(a)
  }

  object Cubed extends ProductReader[Cubed[_]] {
    final val id = 13

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Cubed[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Cubed[Any]()(_num)
    }
  }
  case class Cubed[A]()(implicit num: Num[A]) extends OpSameBase[A]
    with OpDD with OpIL with OpLL with ProductWithAdjuncts {

    override def apply: A => A = num.cubed

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => rd2.cubed(a)
    override val funIL: Int    => Long   = a => ri2.cubed(a)
    override val funLL: Long   => Long   = a => rl2.cubed(a)
  }

  object Sqrt extends ProductReader[Sqrt] {
    final val id = 14

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Sqrt = {
      require (arity == 0 && adj == 0)
      new Sqrt
    }
  }
  case class Sqrt() extends OpDouble {
    override def apply: Double => Double = rd.sqrt

    // ---- legacy ----
    val funDD: Double => Double = a => rd.sqrt(a)
  }

  object Exp extends ProductReader[Exp] {
    final val id = 15

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Exp = {
      require (arity == 0 && adj == 0)
      new Exp
    }
  }
  case class Exp() extends OpDouble {
    override def apply: Double => Double = rd.exp

    // ---- legacy ----
    val funDD: Double => Double = a => rd.exp(a)
  }

  object Reciprocal extends ProductReader[Reciprocal] {
    final val id = 16

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Reciprocal = {
      require (arity == 0 && adj == 0)
      new Reciprocal
    }
  }
  case class Reciprocal() extends OpDouble with OpID with OpLD {
    override def apply: Double => Double = rd2.reciprocal

    // ---- legacy ----
    override val funDD: Double => Double = a => rd2.reciprocal(a)
    override val funID: Int    => Double = a => rd2.reciprocal(a.toDouble)
    override val funLD: Long   => Double = a => rd2.reciprocal(a.toDouble)
  }

  object MidiCps extends ProductReader[MidiCps] {
    final val id = 17

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): MidiCps = {
      require (arity == 0 && adj == 0)
      new MidiCps
    }
  }
  case class MidiCps() extends OpDouble {
    override def apply: Double => Double = rd.midiCps

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.midiCps(a)
  }

  object CpsMidi extends ProductReader[CpsMidi] {
    final val id = 18

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): CpsMidi = {
      require (arity == 0 && adj == 0)
      new CpsMidi
    }
  }
  case class CpsMidi() extends OpDouble {
    override def apply: Double => Double = rd.cpsMidi

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.cpsMidi(a)
  }

  object MidiRatio extends ProductReader[MidiRatio] {
    final val id = 19

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): MidiRatio = {
      require (arity == 0 && adj == 0)
      new MidiRatio
    }
  }
  case class MidiRatio() extends OpDouble {
    override def apply: Double => Double = rd.midiRatio

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.midiRatio(a)
  }

  object RatioMidi extends ProductReader[RatioMidi] {
    final val id = 20

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RatioMidi = {
      require (arity == 0 && adj == 0)
      new RatioMidi
    }
  }
  case class RatioMidi() extends OpDouble {
    override def apply: Double => Double = rd.ratioMidi

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.ratioMidi(a)
  }

  object DbAmp extends ProductReader[DbAmp] {
    final val id = 21

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DbAmp = {
      require (arity == 0 && adj == 0)
      new DbAmp
    }
  }
  case class DbAmp() extends OpDouble {
    override def apply: Double => Double = rd.dbAmp

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.dbAmp(a)
  }

  object AmpDb extends ProductReader[AmpDb] {
    final val id = 22

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): AmpDb = {
      require (arity == 0 && adj == 0)
      new AmpDb
    }
  }
  case class AmpDb() extends OpDouble {
    override def apply: Double => Double = rd.ampDb

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.ampDb(a)
  }

  object OctCps extends ProductReader[OctCps] {
    final val id = 23

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): OctCps = {
      require (arity == 0 && adj == 0)
      new OctCps
    }
  }
  case class OctCps() extends OpDouble {
    override def apply: Double => Double = rd.octCps

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.octCps(a)
  }

  object CpsOct extends ProductReader[CpsOct] {
    final val id = 24

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): CpsOct = {
      require (arity == 0 && adj == 0)
      new CpsOct
    }
  }
  case class CpsOct() extends OpDouble {
    override def apply: Double => Double = rd.cpsOct

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.cpsOct(a)
  }

  object Log extends ProductReader[Log] {
    final val id = 25

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Log = {
      require (arity == 0 && adj == 0)
      new Log()
    }
  }
  final case class Log() extends OpDouble {
    override def apply: Double => Double = rd.log

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.log(a)
  }

  object Log2 extends ProductReader[Log2] {
    final val id = 26

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Log2 = {
      require (arity == 0 && adj == 0)
      new Log2
    }
  }
  final case class Log2() extends OpDouble {
    override def apply: Double => Double = rd.log2

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.log2(a)
  }

  object Log10 extends ProductReader[Log10] {
    final val id = 27

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Log10 = {
      require (arity == 0 && adj == 0)
      new Log10
    }
  }
  final case class Log10() extends OpDouble {
    override def apply: Double => Double = rd.log10

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.log10(a)
  }

  object Sin extends ProductReader[Sin] {
    final val id = 28

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Sin = {
      require (arity == 0 && adj == 0)
      new Sin
    }
  }
  final case class Sin() extends OpDouble {
    override def apply: Double => Double = rd.sin

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.sin(a)
  }

  object Cos extends ProductReader[Cos] {
    final val id = 29

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Cos = {
      require (arity == 0 && adj == 0)
      new Cos
    }
  }
  final case class Cos() extends OpDouble {
    override def apply: Double => Double = rd.cos

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.cos(a)
  }

  object Tan extends ProductReader[Tan] {
    final val id = 30

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Tan = {
      require (arity == 0 && adj == 0)
      new Tan
    }
  }
  final case class Tan() extends OpDouble {
    override def apply: Double => Double = rd.tan

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.tan(a)
  }

  object Asin extends ProductReader[Asin] {
    final val id = 31

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Asin = {
      require (arity == 0 && adj == 0)
      new Asin
    }
  }
  final case class Asin() extends OpDouble {
    override def apply: Double => Double = rd.asin

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.asin(a)
  }

  object Acos extends ProductReader[Acos] {
    final val id = 32

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Acos = {
      require (arity == 0 && adj == 0)
      new Acos
    }
  }
  final case class Acos() extends OpDouble {
    override def apply: Double => Double = rd.acos

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.acos(a)
  }

  object Atan extends ProductReader[Atan] {
    final val id = 33

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Atan = {
      require (arity == 0 && adj == 0)
      new Atan
    }
  }
  final case class Atan() extends OpDouble {
    override def apply: Double => Double = rd.atan

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.atan(a)
  }

  object Sinh extends ProductReader[Sinh] {
    final val id = 34

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Sinh = {
      require (arity == 0 && adj == 0)
      new Sinh
    }
  }
  final case class Sinh() extends OpDouble {
    override def apply: Double => Double = rd.sinh

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.sinh(a)
  }

  object Cosh extends ProductReader[Cosh] {
    final val id = 35

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Cosh = {
      require (arity == 0 && adj == 0)
      new Cosh
    }
  }
  final case class Cosh() extends OpDouble {
    override def apply: Double => Double = rd.cosh

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.cosh(a)
  }

  object Tanh extends ProductReader[Tanh] {
    final val id = 36

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Tanh = {
      require (arity == 0 && adj == 0)
      new Tanh
    }
  }
  final case class Tanh() extends OpDouble {
    override def apply: Double => Double = rd.tanh

    // ---- legacy ----
    override val funDD: Double => Double = a => rd.tanh(a)
  }

  abstract class OpPred[A] extends Op[A, Boolean] { op =>
    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in.tpe
      stream.UnaryOp[A, Int](name, x => if (op.apply(x)) 1 else 0, in = in.toElem)
    }
  }

  object IsNaN extends ProductReader[IsNaN] {
    final val id = 100

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): IsNaN = {
      require (arity == 0 && adj == 0)
      new IsNaN
    }
  }
  case class IsNaN() extends OpPred[Double] with OpDD with OpDI {
    override def apply: Double => Boolean = _.isNaN

    // ---- legacy ----
    override val funDD: Double => Double = a => if (java.lang.Double.isNaN(a)) 1.0 else 0.0
    override val funDI: Double => Int    = a => if (java.lang.Double.isNaN(a)) 1   else 0
  }

  object NextPowerOfTwo extends ProductReader[NextPowerOfTwo[_]] {
    final val id = 101

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): NextPowerOfTwo[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new NextPowerOfTwo[Any]()(_num)
    }
  }
  case class NextPowerOfTwo[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: A => A = num.nextPowerOfTwo

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => ri.nextPowerOfTwo(Math.ceil(a).toInt).toDouble
    override val funII: Int    => Int    = a => ri.nextPowerOfTwo(a)
    override val funLL: Long   => Long   = a => rl.nextPowerOfTwo(a)
  }

  object ToLong extends ProductReader[ToLong[_]] {
    final val id = 200

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ToLong[_] = {
      require (arity == 0 && adj == 1)
      val _num: ScalarToNum[Any] = in.readAdjunct()
      new ToLong[Any]()(_num)
    }
  }
  case class ToLong[A]()(implicit to: ScalarToNum[A])
    extends Op[A, Long] with OpDD with OpDL with OpLL with OpIL with ProductWithAdjuncts {

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val toC = to.asInstanceOf[ToNum[_] with Scalar[_]] match {
      case BooleanTop => IntTop.asInstanceOf[ScalarToNum[A]]
      case _          => to
    }

    override def apply: A => Long = toC.toLong

    override private[UnaryOp] def makeStream(in: StreamIn.T[A])(implicit b: Builder): StreamOut = in.toLong

    override def adjuncts: Adjuncts = to :: Nil

    // ---- legacy ----
    override val funDD: Double => Double = a => a.toInt.toDouble
    override val funDL: Double => Long   = a => a.toLong
    override val funLL: Long   => Long   = a => a
    override val funIL: Int    => Long   = a => a.toLong
  }

  // ----

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UGenSource.SingleOut[_] = {
    require (arity == 2 && adj == 0)
    in.readElem() match {
      case opId: Int => // legacy
        val _in = in.readGE[Any]()
        Legacy[Any](opId, _in)
      case _op0: Op[_, _]  =>
        val _op = _op0.asInstanceOf[Op[Any, Any]]
        val _in = in.readGE[Any]()
        new UnaryOp[Any, Any](_op, _in)
      case other          => sys.error(s"Unexpected element $other")
    }
  }

  private final case class Legacy[A](opId: Int, in: GE[A]) extends UGenSource.SingleOut[A] {
    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
      unwrap(this, Vector(in.expand))

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
      UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.Int(opId) :: Nil)

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
      val Vec(in) = args: @unchecked
      val op0 = UnaryOp.Op.legacy(opId)

      if (in.isDouble) {
        op0 match {
          case opDI: UnaryOp.OpDI =>
            stream.UnaryOp[Double , Int](op0.name, opDI.funDI, in = in.toDouble): StreamOut

          case opDL: UnaryOp.OpDL =>
            stream.UnaryOp[Double , Long](op0.name, opDL.funDL, in = in.toDouble): StreamOut

          case opDD: UnaryOp.OpDD =>
            stream.UnaryOp[Double , Double](op0.name, opDD.funDD, in = in.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }
      } else if (in.isInt) {
        op0 match {
          case opII: UnaryOp.OpII =>
            stream.UnaryOp[Int    , Int](op0.name, opII.funII, in = in.toInt   ): StreamOut

          case opIL: UnaryOp.OpIL =>
            stream.UnaryOp[Int    , Long](op0.name, opIL.funIL, in = in.toInt   ): StreamOut

          case opID: UnaryOp.OpID =>
            stream.UnaryOp[Int    , Double](op0.name, opID.funID, in = in.toInt   ): StreamOut

          case opDD: UnaryOp.OpDD =>
            stream.UnaryOp[Double , Double](op0.name, opDD.funDD, in = in.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }

      } else /*if (in.isLong)*/ {
        op0 match {
          case opLI: UnaryOp.OpLI =>
            stream.UnaryOp[Long, Int](op0.name, opLI.funLI, in = in.toLong  ): StreamOut

          case opLL: UnaryOp.OpLL =>
            stream.UnaryOp[Long, Long](op0.name, opLL.funLL, in = in.toLong  ): StreamOut

          case opLD: UnaryOp.OpLD =>
            stream.UnaryOp[Long, Double](op0.name, opLD.funLD, in = in.toLong  ): StreamOut

          case opDD: UnaryOp.OpDD =>
            stream.UnaryOp[Double, Double](op0.name, opDD.funDD, in = in.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }
      }
    }
  }
}
final case class UnaryOp[A, B](op: Op[A, B], in: GE[A]) extends UGenSource.SingleOut[B] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[B] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[B] =
    UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.String(op.name) :: Nil)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    val inC = in.cast[A]
//    import inC.tpe
//    val out = stream.UnaryOp[A, B](op.name, op.apply, in = inC.toElem)
//    tpe.mkStreamOut(out)
    op.makeStream(inC)
  }
}
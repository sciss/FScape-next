/*
 *  package.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import akka.NotUsed
import akka.stream.scaladsl.{FlowOps, GraphDSL}
import akka.stream.{Inlet, Outlet}

package object stream {
  type Signal[A] = FlowOps[A, NotUsed]

//  // to-do: `unfold` is unnecessarily inefficient because of producing `Option[Int]`.
//  implicit def constIntSignal   (i: Int   ): Signal[Int]    = Source.repeat(i) // or better `single`?
//  implicit def constDoubleSignal(d: Double): Signal[Double] = Source.repeat(d) // or better `single`?

  type Layer = Int

  type InI                     = Inlet[Buf.E[Int]]
  type InD                     = Inlet[Buf.E[Double]]
  type InL                     = Inlet[Buf.E[Long]]
  type InA                     = Inlet[Buf]
  type In[A]                   = Inlet[Buf.E[A]]

  final val Args: Specializable.Group[(Int, Long, Double, Boolean)] = null // not available in Scala 2.12

  @inline
  def  InI(name: String): InI = Inlet[Buf.E[Int]](name)
  @inline
  def  InD(name: String): InD = Inlet[Buf.E[Double]](name)
  @inline
  def  InL(name: String): InL = Inlet[Buf.E[Long]](name)
  @inline
  def  InA(name: String): InA = Inlet[Buf](name)

  @inline
  def In[A](name: String): In[A] = Inlet[Buf.E[A]](name)

  //  type Buf.E[A]               = Buf { type Elem = A }

  type OutI                 = Outlet[Buf.E[Int]]
  type OutD                 = Outlet[Buf.E[Double]]
  type OutL                 = Outlet[Buf.E[Long]]
  type OutA                 = Outlet[Buf]
  type Out[A]               = Outlet[Buf.E[A]]

  @inline
  def  OutI(name: String): OutI = Outlet[Buf.E[Int] /*BufI*/](name)
  @inline
  def  OutD(name: String): OutD = Outlet[Buf.E[Double] /*BufD*/](name)
  @inline
  def  OutL(name: String): OutL = Outlet[Buf.E[Long] /*BufL*/](name)

  @inline
  def Out[A](name: String): Out[A] = Outlet[Buf.E[A]](name)

  type GBuilder = GraphDSL.Builder[NotUsed]

  type StreamInElem[A1] = StreamIn { type A = A1 }
}
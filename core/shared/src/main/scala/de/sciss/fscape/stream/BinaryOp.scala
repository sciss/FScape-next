/*
 *  BinaryOp.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.stream.impl.Handlers.{InMain, OutMain}
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

import scala.annotation.tailrec
import scala.math.min

object BinaryOp {
  def apply[A, B, C](opName: String, op: (A, B) => C, in1: Out[A], in2: Out[B])
                                                   (implicit b: Builder,
                                                    in1Tpe: DataType[A],
                                                    in2Tpe: DataType[B],
                                                    outTpe: DataType[C]): Out[C] = {
    val stage0  = new Stage[A, B, C](layer = b.layer, opName = opName, op = op)
    val stage   = b.add(stage0)
    b.connect(in1, stage.in0)
    b.connect(in2, stage.in1)
    stage.out
  }

  private final val name = "BinaryOp"

  private type Shp[A, B, C] = FanInShape2[Buf.E[A], Buf.E[B], Buf.E[C]]

  private final class Stage[A, B, C](layer: Layer, opName: String, op: (A, B) => C)
                                 (implicit a: Allocator,
                                  in1Tpe: DataType[A],
                                  outTpe: DataType[C])
    extends StageImpl[Shp[A, B, C]](s"$name($opName)") { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.in1"),
      in1 = In [B](s"${stage.name}.in2"),
      out = Out[C](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_, _, _] = if (in1Tpe.isDouble) {
        if (outTpe.isDouble) {
          new Logic[Double, Double, Double](
            shape.asInstanceOf[Shp[Double, Double , Double]], layer, opName, op.asInstanceOf[(Double, Double)  => Double ])
        } else if (outTpe.isInt) {
          new Logic[Double, Double, Int](
            shape.asInstanceOf[Shp[Double, Double , Int   ]], layer, opName, op.asInstanceOf[(Double, Double)  => Int    ])
        } else {
          assert (outTpe.isLong)
          new Logic[Double, Double, Long](
            shape.asInstanceOf[Shp[Double, Double, Long  ]], layer, opName, op.asInstanceOf[(Double, Double)  => Long   ])
        }
      } else if (in1Tpe.isInt) {
        if (outTpe.isDouble) {
          new Logic[Int, Int, Double](
            shape.asInstanceOf[Shp[Int   , Int, Double]], layer, opName, op.asInstanceOf[(Int   , Int   )  => Double ])
        } else if (outTpe.isInt) {
          new Logic[Int, Int, Int](
            shape.asInstanceOf[Shp[Int   , Int, Int   ]], layer, opName, op.asInstanceOf[(Int   , Int   )  => Int    ])
        } else {
          assert (outTpe.isLong)
          new Logic[Int, Int, Long](
            shape.asInstanceOf[Shp[Int   , Int, Long  ]], layer, opName, op.asInstanceOf[(Int   , Int   )  => Long   ])
        }
      } else {
        assert(in1Tpe.isLong)
        if (outTpe.isDouble) {
          new Logic[Long, Long, Double](
            shape.asInstanceOf[Shp[Long  , Long, Double]], layer, opName, op.asInstanceOf[(Long  , Long  )  => Double ])
        } else if (outTpe.isInt) {
          new Logic[Long, Long, Int](
            shape.asInstanceOf[Shp[Long  , Long, Int   ]], layer, opName, op.asInstanceOf[(Long  , Long  )  => Int    ])
        } else {
          assert (outTpe.isLong)
          new Logic[Long, Long, Long](
            shape.asInstanceOf[Shp[Long  , Long, Long  ]], layer, opName, op.asInstanceOf[(Long  , Long  )  => Long   ])
        }
      }
      res.asInstanceOf[Logic[A, B, C]]
    }
  }

  private final class Logic[
    @specialized(Args) A,
    @specialized(Args) B,
    @specialized(Args) C](shape: Shp[A, B, C], layer: Layer, opName: String, op: (A, B) => C)
                         (implicit a: Allocator, inTpe1: DataType[A], inTpe2: DataType[B], outTpe: DataType[C])
    extends Handlers(s"$name($opName)", layer, shape) {

    private[this] val hA  : InMain  [A] = InMain [A](this, shape.in0)
    private[this] val hB  : InMain  [B] = InMain [B](this, shape.in1)
    private[this] val hOut: OutMain [C] = OutMain[C](this, shape.out)

    private[this] var aVal: A = _
    private[this] var bVal: B = _
    private[this] var aValid = false
    private[this] var bValid = false

    protected def onDone(inlet: Inlet[_]): Unit = {
      val live = if (inlet == hA.inlet) {
        aValid && !hB.isDone
      } else {
        assert (inlet == hB.inlet)
        bValid && !hA.isDone
      }
      if (live) process() else if (hOut.flush()) completeStage()
    }

    @tailrec
    protected def process(): Unit = {
      val remOut = hOut.available
      if (remOut == 0) return

      if (hB.isDone) {
        val remA = hA.available
        if (remA == 0) return

        val bv      = bVal
        val rem     = min(remA, remOut)
        val a       = hA  .array
        var offA    = hA  .offset
        val out     = hOut.array
        var offOut  = hOut.offset
        val stop    = offOut + rem
        var av: A   = null.asInstanceOf[A]
        while (offOut < stop) {
          av          = a(offA)
          out(offOut) = op(av, bv)
          offA   += 1
          offOut += 1
        }
        hA  .advance(rem)
        hOut.advance(rem)
        aVal    = av
        aValid  = true
        if (hA.isDone) {
          if (hOut.flush()) completeStage()
        } else {
          process()
        }

      } else if (hA.isDone) {
        val remB = hB.available
        if (remB == 0) return

        val av      = aVal
        val rem     = min(remB, remOut)
        val b       = hB  .array
        var offB    = hB  .offset
        val out     = hOut.array
        var offOut  = hOut.offset
        val stop    = offOut + rem
        var bv: B   = null.asInstanceOf[B]
        while (offOut < stop) {
          bv          = b(offB)
          out(offOut) = op(av, bv)
          offB   += 1
          offOut += 1
        }
        hB  .advance(rem)
        hOut.advance(rem)
        bVal    = bv
        bValid  = true
        if (hB.isDone) {
          if (hOut.flush()) completeStage()
        } else {
          process()
        }

      } else {
        val remIn = min(hA.available, hB.available)
        if (remIn == 0) return

        val rem     = min(remIn, remOut)
        val a       = hA  .array
        var offA    = hA  .offset
        val b       = hB  .array
        var offB    = hB  .offset
        val out     = hOut.array
        var offOut  = hOut.offset
        val stop    = offOut + rem
        var av: A   = null.asInstanceOf[A]
        var bv: B   = null.asInstanceOf[B]
        while (offOut < stop) {
          av          = a(offA)
          bv          = b(offB)
          out(offOut) = op(av, bv)
          offA   += 1
          offB   += 1
          offOut += 1
        }
        hA  .advance(rem)
        hB  .advance(rem)
        hOut.advance(rem)
        aVal    = av
        bVal    = bv
        bValid  = true
        aValid  = true
        if (hA.isDone && hB.isDone) {
          if (hOut.flush()) completeStage()
        } else {
          process()
        }
      }
    }
  }
}
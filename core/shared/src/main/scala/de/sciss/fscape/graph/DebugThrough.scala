/*
 *  DebugThrough.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object DebugThrough extends ProductReader[DebugThrough[_]] {
  implicit final class DebugThroughOp[A](private val in: GE[A]) extends AnyVal {
    def <| (label: String): GE[A] = DebugThrough(in, label)
  }

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DebugThrough[_] = {
    require (arity == 2 && adj == 0)
    val _in     = in.readGE[Any]()
    val _label  = in.readString()
    new DebugThrough(_in, _label)
  }
}
/** A UGen that passes through its input, and upon termination prints
  * the number of frames seen.
  *
  * For convenience, `import DebugThrough._` allows to write
  *
  * {{{
  *   val sin = SinOsc(freq) <| "my-sin"
  * }}}
  *
  * @param in     the input to be pulled.
  * @param label  an identifying label to prepend to the printing.
  */
final case class DebugThrough[A](in: GE[A], label: String) extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.String(label) :: Nil)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    import in.tpe
    val out = stream.DebugThrough[in.A](in = in.toElem, label = label)
    tpe.mkStreamOut(out)
  }
}
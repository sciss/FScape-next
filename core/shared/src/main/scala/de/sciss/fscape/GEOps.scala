/*
 *  GEOps.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import de.sciss.fscape.Ops._
import de.sciss.fscape.graph.{BinaryOp, ChannelProxy, Clip, ComplexBinaryOp, ComplexUnaryOp, Concat, Const, Differentiate, Distinct, Drop, DropRight, DropWhile, Elastic, ExpExp, ExpLin, FilterSeq, Fold, Indices, Length, LinExp, LinLin, MatchLen, Metro, Poll, RunningMax, RunningMin, RunningProduct, RunningSum, SampleRate, Take, TakeRight, TakeWhile, UnaryOp, UnzipWindow, Wrap, Zip}
import de.sciss.lucre.Adjunct.{Eq, Num, NumBool, NumDiv, NumInt, Ord, Scalar, ScalarOrd, ScalarToNum}
import de.sciss.optional.Optional

/** `GEOps1` are operations for graph elements (`GE`). Instead of having these operations directly defined
  * in each element, which is a huge list, they appear here as extension methods. `GEOps1` are unary
  * operators, whereas `GEOps2` are binary and n-ary operators.
  *
  * @see [[GE]]
  * @see [[GEOps2]]
  */
final class GEOps1[A](private val x: GE[A]) extends AnyVal { me =>

  /** Creates a proxy that represents a specific output channel of the element.
    *
    * @param index  channel-index, zero-based. Indices which are greater than or equal
    *               to the number of outputs are wrapped around.
    * @return a monophonic element that represents the given channel of the receiver
    */
  def out(index: Int): GE[A] = ChannelProxy(x, index)

  @inline private def unOp[B](op: UnaryOp.Op[A, B]): GE[B] = op.make(x)

  private def single[B](in: GE[B]): GE[B] =
    in match {
      case c: Const[B] => c
      case _              => in.head
    }

  import de.sciss.fscape.graph.UnaryOp.{Log => _Log, _}

  // unary ops
  def unary_-   (implicit num: Num[A])     : GE[A] = unOp(Neg()   )
  def unary_!   (implicit num: NumBool[A]) : GE[A] = unOp(Not()   )
  def unary_~   (implicit num: NumInt[A]  ): GE[A] = unOp(BitNot())
  def abs       (implicit num: Num[A]     ): GE[A] = unOp(Abs()   )
  def toDouble  (implicit to: ScalarToNum[A]): GE.D = ToDouble[A]().make(x)
  def toInt     (implicit to: ScalarToNum[A]): GE.I = ToInt   [A]().make(x)
  def toLong    (implicit to: ScalarToNum[A]): GE.L = ToLong  [A]().make(x)
  def ceil      (implicit ev: GE[A] =:= GE.D): GE.D  = Ceil ().make(ev(x))
  def floor     (implicit ev: GE[A] =:= GE.D): GE.D  = Floor().make(ev(x))

  def frac      (implicit ev: GE[A] =:= GE.D): GE.D = Frac().make(ev(x))
  def signum    (implicit num: Num[A]     ): GE[A] = unOp(Signum  [A]())
  def squared   (implicit num: Num[A]     ): GE[A] = unOp(Squared [A]())
  def cubed     (implicit num: Num[A]     ): GE[A] = unOp(Cubed   [A]())

  def sqrt      (implicit w: Widen[A, Double]): GE.D    = Sqrt().make(w.widen1(x))
  def exp       (implicit w: Widen[A, Double]): GE.D    = Exp ().make(w.widen1(x))

  /** The reciprocal is one divided by the input signal. */
  def reciprocal(implicit w: Widen[A, Double]): GE.D    = Reciprocal().make(w.widen1(x))

  /** Converts from MIDI note number to frequency in Hertz (or cycles-per-second) */
  def midiCps   (implicit w: Widen[A, Double]): GE.D    = MidiCps   ().make(w.widen1(x))
  /** Converts from frequency in Hertz (or cycles-per-second) to MIDI note number. */
  def cpsMidi   (implicit w: Widen[A, Double]): GE.D    = CpsMidi   ().make(w.widen1(x))
  def midiRatio (implicit w: Widen[A, Double]): GE.D    = MidiRatio ().make(w.widen1(x))
  def ratioMidi (implicit w: Widen[A, Double]): GE.D    = RatioMidi ().make(w.widen1(x))
  /** Converts from decimals to a linear value (or amplitude) */
  def dbAmp     (implicit w: Widen[A, Double]): GE.D    = DbAmp     ().make(w.widen1(x))
  /** Converts from a linear value (or amplitude) to decimals */
  def ampDb     (implicit w: Widen[A, Double]): GE.D    = AmpDb     ().make(w.widen1(x))

  def octCps    (implicit w: Widen[A, Double]): GE.D    = OctCps    ().make(w.widen1(x))
  def cpsOct    (implicit w: Widen[A, Double]): GE.D    = CpsOct    ().make(w.widen1(x))

  /** Natural logarithm */
  def log       (implicit w: Widen[A, Double]): GE.D    = _Log  ().make(w.widen1(x))
  /** Base-2 logarithm */
  def log2      (implicit w: Widen[A, Double]): GE.D    = Log2  ().make(w.widen1(x))
  /** Base-10 logarithm */
  def log10     (implicit w: Widen[A, Double]): GE.D    = Log10 ().make(w.widen1(x))
  def sin       (implicit w: Widen[A, Double]): GE.D    = Sin   ().make(w.widen1(x))
  def cos       (implicit w: Widen[A, Double]): GE.D    = Cos   ().make(w.widen1(x))
  def tan       (implicit w: Widen[A, Double]): GE.D    = Tan   ().make(w.widen1(x))
  def asin      (implicit w: Widen[A, Double]): GE.D    = Asin  ().make(w.widen1(x))
  def acos      (implicit w: Widen[A, Double]): GE.D    = Acos  ().make(w.widen1(x))
  def atan      (implicit w: Widen[A, Double]): GE.D    = Atan  ().make(w.widen1(x))
  def sinh      (implicit w: Widen[A, Double]): GE.D    = Sinh  ().make(w.widen1(x))
  def cosh      (implicit w: Widen[A, Double]): GE.D    = Cosh  ().make(w.widen1(x))
  def tanh      (implicit w: Widen[A, Double]): GE.D    = Tanh  ().make(w.widen1(x))

  /** Tests the input signal for NaNs (not-a-number). For instance, taking the logarithm
    * of a negative number produces a NaN. Useful to detect and replace those special values.
    */
  def isNaN     (implicit ev: GE[A] =:= GE.D): GE.B = IsNaN().make(ev(x))

  def nextPowerOfTwo(implicit num: NumInt[A]) : GE[A]  = NextPowerOfTwo[A]().make(x)

  def differentiate(implicit num: Num[A]): GE[A] = Differentiate(x)

  /** Inserts a number of buffering blocks to avoid signal deadlock in diamond shape topologies.
    *
    * @see [[de.sciss.fscape.graph.BufferMemory]]
    */
  def elastic(n: GE[Int] = 1): GE[A] = Elastic(x, n)

  // ---- sequence ----

  /** Concatenates another signal to this (finite) signal. */
  def ++ /*[B >: A]*/ (that: GE[A]): GE[A] = Concat(x, that)

  /** Prepends a single frame. */
  def +: /*[B >: A]*/ (elem: GE[A]): GE[A] = prepended(elem)

  /** Appends a single frame. */
  def :+ /*[B >: A]*/ (elem: GE[A]): GE[A] = appended(elem)

  /** Appends a single frame. */
  def appended /*[B >: A]*/(elem: GE[A]): GE[A] = Concat(x, single(elem))

  /** Concatenates another signal to this (finite) signal. */
  def concat /*[B >: A]*/(that: GE[A]): GE[A] = Concat(x, that)

//  def contains(elem: GE): GE = indexOf(elem) >= 0

  /** Filters elements to return only distinct values (removes duplicate values) */
  def distinct(implicit eq: Eq[A]): GE[A] = Distinct(x)

  /** Drops the first `length` elements of this signal. */
  def drop[L: NumInt](length: GE[L]): GE[A] = Drop(x, length = length)

  /** Drops the last `length` elements of this signal. */
  def dropRight[L: NumInt](length: GE[L]): GE[A] = DropRight(x, length = length)

  def dropWhile(p: GE[Boolean]): GE[A] = DropWhile(x, p)

//  def endsWith(that: GE): GE = ...

  def filter(p: GE[Boolean]): GE[A] = FilterSeq(x, p)

  def filterNot(p: GE[Boolean]): GE[A] = filter(!p)

    /** Outputs the first element of this signal, then terminates. */
  def head: GE[A] = take(1)

  /** Outputs a monotonous sequence from zero to the signal length minus one. */
  def indices: GE.L = Indices(x)

  def init: GE[A] = dropRight(1)

  def isDefinedAt[L](index: GE[L])(implicit w: Widen[L, Long]): GE[Boolean] = {
    val indexW = w.widen1(index)
    (indexW >= 0L) && (indexW < size)
  }

  def isEmpty: GE.B = size sig_== 0L

  /** Outputs the last element of this (finite) signal, then terminates. */
  def last: GE[A] = takeRight(1)

  /** Outputs a signal value, the length of the input signal. */
  def length: GE.L = Length(x)

  /** Returns the maximum value cross all elements (or an empty stream if the input is empty) */
  def maximum(implicit ord: Ord[A]): GE[A] = RunningMax(x).last

  /** Returns the minimum value cross all elements (or an empty stream if the input is empty) */
  def minimum(implicit ord: Ord[A]): GE[A] = RunningMin(x).last

  def nonEmpty: GE[Boolean] = size > 0L

//  def padTo(len: GE, elem: GE = 0.0): GE = ...

//  def patch(from: GE, other: GE, replaced: GE): GE = ...

  /** Prepends a single frame. */
  def prepended /*[B >: A]*/(elem: GE[A]): GE[A] = Concat(single(elem), x)

  def product(implicit num: Num[A]): GE[A] = RunningProduct(x).last

//  def reverse: GE = ...

  /** Outputs a signal value, the length of the input signal. Same as `length`. */
  def size: GE.L = length

  def slice[L: NumInt](from: GE[L], until: GE[L]): GE[A] = {
    val sz = until - from
    x.drop(from).take(sz)
  }

//  def sorted: GE = ...

  def splitAt[L: NumInt](n: GE[L]): (GE[A], GE[A]) = {
    val _1 = take(n)
    val _2 = drop(n)
    (_1, _2)
  }

//  def startsWith(that: GE, offset: GE = 0L): GE = ...

  def sum(implicit num: Num[A]): GE[A] = RunningSum(x).last

  def tail: GE[A] = drop(1)

  /** Takes at most `length` elements of this signal, then terminates. */
  def take[B: NumInt](length: GE[B]): GE[A] = Take(in = x, length = length)

  /** Takes at most the last `length` elements of this (finite) signal. */
  def takeRight[L: NumInt](length: GE[L]): GE[A] = TakeRight(in = x, length = length)

  def takeWhile(p: GE[Boolean]): GE[A] = TakeWhile(x, p)

  def unzip: (GE[A], GE[A]) = {
    val u = UnzipWindow(x)
    (u.out(0), u.out(1))
  }

  def zip /*[B >: A]*/(that: GE[A]): GE[A] = Zip(x, that)
}

/** `GEOps2` are operations for graph elements (`GE`). Instead of having these operations directly defined
  * in each element, which is a huge list, they appear here as extension methods. `GEOps1` are unary
  * operators, whereas `GEOps2` are binary and n-ary operators.
  *
  * @see [[GE]]
  * @see [[GEOps1]]
  */
final class GEOps2[A](private val x: GE[A]) extends AnyVal { me =>
//  def madd(mul: GE, add: GE): GE = MulAdd(g, mul, add)
//
//  def flatten               : GE = Flatten(g)

  /** Polls a single value from the element. */
  def poll: Poll[A] = poll()

  /** Polls a single value from the element, and prints it with a given label. */
  def poll(label: String): Poll[A] = poll(gate = Metro(0), label = Some(label))

  /** Polls the output values of this graph element, and prints the result to the console.
    * This is a convenient method for wrapping this graph element in a `Poll` UGen.
    *
    * @param   gate     a gate signal for the printing.
    * @param   label    a string to print along with the values, in order to identify
    *                   different polls. Using the special label `"#auto"` (default) will generated
    *                   automatic useful labels using information from the polled graph element
    * @see  [[de.sciss.fscape.graph.Poll]]
    */
  def poll(gate: GE.B = Metro(0), label: Optional[String] = None): Poll[A] =
    Poll(in = x, gate = gate, label = label.getOrElse {
      val str = x.toString
      val i   = str.indexOf('(')
      if (i >= 0) str.substring(0, i)
      else {
        val j = str.indexOf('@')
        if (j >= 0) str.substring(0, j)
        else str
      }
    })

  // binary ops
  @inline private def binOp[B, C](op: BinaryOp.Op[A, B, C], b: GE[B]): GE[C] = op.make(x, b)

  import de.sciss.fscape.graph.BinaryOp.{Eq => _Eq, _}

  /** Addition of two signals. */
  def +  [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num    [A2]): GE[A2] = Plus [A2]().make(w.widen1(x), w.widen2(that))

  /** Subtracts the second operand from the input signal. */
  def -  [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num    [A2]): GE[A2] = Minus[A2]().make(w.widen1(x), w.widen2(that))

  /** Multiplication of two signals. */
  def *  [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num    [A2]): GE[A2] = Times[A2]().make(w.widen1(x), w.widen2(that))
  def /  [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: NumDiv [A2]): GE[A2] = Div  [A2]().make(w.widen1(x), w.widen2(that))

  def %  [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num    [A2]): GE[A2] = ModJ[A2]().make(w.widen1(x), w.widen2(that))
  def mod[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num    [A2]): GE[A2] = Mod [A2]().make(w.widen1(x), w.widen2(that))

  /** Compares the two signals for equality, frame by frame */
  def sig_==(that: GE[A])(implicit eq: Eq[A] with Scalar[A]): GE.B = _Eq [A]().make(x, that)

  /** Compares the two signals for inequality, frame by frame */
  def sig_!=(that: GE[A])(implicit eq: Eq[A] with Scalar[A]): GE.B = Neq [A]().make(x, that)

  def < (that: GE[A])(implicit ord: ScalarOrd[A]): GE.B = binOp(Lt [A]()(ord), that)
  def > (that: GE[A])(implicit ord: ScalarOrd[A]): GE.B = binOp(Gt [A]()(ord), that)
  def <=(that: GE[A])(implicit ord: ScalarOrd[A]): GE.B = binOp(Leq[A](), that)
  def >=(that: GE[A])(implicit ord: ScalarOrd[A]): GE.B = binOp(Geq[A](), that)

  def min[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Min[A2]().make(w.widen1(x), w.widen2(that))
  def max[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Max[A2]().make(w.widen1(x), w.widen2(that))

  /** Bit-wise AND */
  def &   (that: GE[A])(implicit num: NumInt [A]): GE[A] = And[A]().make(x, that)
  /** Bit-wise OR */
  def |   (that: GE[A])(implicit num: NumInt [A]): GE[A] = Or [A]().make(x, that)
  /** Bit-wise XOR */
  def ^   (that: GE[A])(implicit num: NumInt [A]): GE[A] = Xor[A]().make(x, that)

  /** Least-common-multiple. */
  def lcm(that: GE[A])(implicit num: NumInt [A]): GE[A] = Lcm[A]().make(x, that)
  /** Greatest-common-denominator. */
  def gcd(that: GE[A])(implicit num: NumInt [A]): GE[A] = Gcd[A]().make(x, that)

  /** Logical AND */
  def &&  (that: GE[A])(implicit num: NumBool[A]): GE[A] = And   [A]().make(x, that)
  /** Logical OR */
  def ||  (that: GE[A])(implicit num: NumBool[A]): GE[A] = Or    [A]().make(x, that)
  /** Logical XOR */
  def ^^  (that: GE[A])(implicit num: NumBool[A]): GE[A] = Xor   [A]().make(x, that)

  def roundTo   [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = RoundTo  [A2]().make(w.widen1(x), w.widen2(that))
  def roundUpTo [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = RoundUpTo[A2]().make(w.widen1(x), w.widen2(that))
  def trunc     [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Trunc    [A2]().make(w.widen1(x), w.widen2(that))

  def atan2   [A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Atan2   ().make(w.widen1(x), w.widen2(that))
  def hypot   [A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Hypot   ().make(w.widen1(x), w.widen2(that))
  def hypotApx[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = HypotApx().make(w.widen1(x), w.widen2(that))

  /** '''Warning:''' Unlike a normal power operation, the signum of the
    * left operand is always preserved. I.e. `DC.kr(-0.5).pow(2)` will
    * not output `0.25` but `-0.25`. This is to avoid problems with
    * floating point noise and negative input numbers, so
    * `DC.kr(-0.5).pow(2.001)` does not result in a `NaN`, for example.
    */
  def pow[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Pow().make(w.widen1(x), w.widen2(that))

  def <<  (that: GE[A])(implicit num: NumInt[A]): GE[A] = LeftShift         [A]().make(x, that)
  def >>  (that: GE[A])(implicit num: NumInt[A]): GE[A] = RightShift        [A]().make(x, that)
  def >>> (that: GE[A])(implicit num: NumInt[A]): GE[A] = UnsignedRightShift[A]().make(x, that)

  def ring1[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Ring1().make(w.widen1(x), w.widen2(that))
  def ring2[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Ring2().make(w.widen1(x), w.widen2(that))
  def ring3[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Ring3().make(w.widen1(x), w.widen2(that))
  def ring4[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Ring4().make(w.widen1(x), w.widen2(that))

  def difSqr[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = DifSqr[A2]().make(w.widen1(x), w.widen2(that))
  def sumSqr[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = SumSqr[A2]().make(w.widen1(x), w.widen2(that))
  def sqrSum[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = SqrSum[A2]().make(w.widen1(x), w.widen2(that))
  def sqrDif[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = SqrDif[A2]().make(w.widen1(x), w.widen2(that))

  /** The absolute difference between two signals. */
  def absDif[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = AbsDif[A2]().make(w.widen1(x), w.widen2(that))

  /** Thresholding such that the input signal is passed when it is equal to or greater than the second operand,
    * otherwise zero is output.
    */
  def thresh  [A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = Thresh  ().make(w.widen1(x), w.widen2(that))

  /** Clipped amplitude-modulation. A multiplication of both signals, or zero if the second operand is
    * less than or equal to zero.
    */
  def amClip  [A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = AmClip  ().make(w.widen1(x), w.widen2(that))

  /** If the input is positive, passes it unmodified, if it is negative it is scaled (multiplied) by the
    * second operand.
    */
  def scaleNeg[A1](that: GE[A1])(implicit w: Widen2[A, A1, Double]): GE.D = ScaleNeg().make(w.widen1(x), w.widen2(that))

  /** Residual of clipping, also known as center clipping.
    * The formula is `(a - clip2(a, b))`.
    */
  def excess[A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Excess[A2]().make(w.widen1(x), w.widen2(that))

  def clip2 [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Clip2 [A2]().make(w.widen1(x), w.widen2(that))
  def fold2 [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Fold2 [A2]().make(w.widen1(x), w.widen2(that))
  def wrap2 [A1, A2](that: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Wrap2 [A2]().make(w.widen1(x), w.widen2(that))

  /** Truncates or extends the first operand to
    * match the length of `b`. This uses
    * the `SecondArg` operator with operands reversed.
    */
  def matchLen[B](b: GE[B]): GE[A] = MatchLen(x, b)

  def clip[A1, A2](lo: GE[A1], hi: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Clip(w.widen1(x), w.widen2(lo), w.widen2(hi))
  def fold[A1, A2](lo: GE[A1], hi: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Fold(w.widen1(x), w.widen2(lo), w.widen2(hi))
  def wrap[A1, A2](lo: GE[A1], hi: GE[A1])(implicit w: Widen2[A, A1, A2], num: Num[A2]): GE[A2] = Wrap(w.widen1(x), w.widen2(lo), w.widen2(hi))

  def linLin(inLo: GE.D, inHi: GE.D, outLo: GE.D, outHi: GE.D)
            (implicit w: Widen[A, Double]): GE.D = LinLin(w.widen1(x), inLo, inHi, outLo, outHi)
  def linExp(inLo: GE.D, inHi: GE.D, outLo: GE.D, outHi: GE.D)
            (implicit w: Widen[A, Double]): GE.D = LinExp(w.widen1(x), inLo, inHi, outLo, outHi)

  def expLin(inLo: GE.D, inHi: GE.D, outLo: GE.D, outHi: GE.D)
            (implicit w: Widen[A, Double]): GE.D = ExpLin(w.widen1(x), inLo, inHi, outLo, outHi)

  def expExp(inLo: GE.D, inHi: GE.D, outLo: GE.D, outHi: GE.D)
            (implicit w: Widen[A, Double]): GE.D = ExpExp(w.widen1(x), inLo, inHi, outLo, outHi)

  /** Enables operators for an assumed complex signal. */
  def complex(implicit ev: GE[A] =:= GE.D): GEComplexOps = new GEComplexOps(ev(x))

  // ---- SampleRate support ----

  /** Converts a frequency in Hertz to a normalized frequency, divided by the sampling rate. */
  def hertz(implicit sr: SampleRate, w: Widen[A, Double]): GE.D = w.widen1(x) / sr

  /** Converts a duration in seconds to a number of sample frames, by multiplying with the sampling rate.
    * The value is truncated to an integer number.
    */
  def seconds(implicit sr: SampleRate, w: Widen[A, Double]): GE.L = (w.widen1(x) * sr).toLong
}

final class GEComplexOps(private val x: GE.D) extends AnyVal { me =>

  @inline private def cUnOp(op: ComplexUnaryOp.Op): GE.D = op.make(x)

  import ComplexUnaryOp.{Log => _Log, _}

  // unary ops, complex output
  def abs       : GE.D  = cUnOp(Abs        )
  def squared   : GE.D  = cUnOp(Squared    )
  def cubed     : GE.D  = cUnOp(Cubed      )
  def exp       : GE.D  = cUnOp(Exp        )
  def reciprocal: GE.D  = cUnOp(Reciprocal )
  def log       : GE.D  = cUnOp(_Log       )
  def log2      : GE.D  = cUnOp(Log2       )
  def log10     : GE.D  = cUnOp(Log10      )
  def conj      : GE.D  = cUnOp(Conj       )
  def absSquared: GE.D  = cUnOp(AbsSquared )
  def carToPol  : GE.D  = cUnOp(CarToPol  )
  def polToCar  : GE.D  = cUnOp(PolToCar  )

  // unary ops, real output
  def real      : GE.D  = cUnOp(Real      )
  def imag      : GE.D  = cUnOp(Imag      )
  def mag       : GE.D  = cUnOp(Mag       )
  def phase     : GE.D  = cUnOp(Phase     )
  def magSquared: GE.D  = cUnOp(MagSquared)

  @inline private def cBinOp(op: ComplexBinaryOp.Op, that: GE.D): GE.D = op.make(x, that)

  import ComplexBinaryOp._

  // binary ops
  def + (that: GE.D): GE.D = cBinOp(Plus , that)
  def - (that: GE.D): GE.D = cBinOp(Minus, that)
  def * (that: GE.D): GE.D = cBinOp(Times, that)
}
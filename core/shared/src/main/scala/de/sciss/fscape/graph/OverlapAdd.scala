/*
 *  OverlapAdd.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object OverlapAdd extends ProductReader[OverlapAdd[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): OverlapAdd[_] = {
    require (arity == 3)
    val _in   = in.readGE[Any]()
    val _size = in.readGE_I()
    val _step = in.readGE_I()
    val _num  = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new OverlapAdd[Any](_in, _size, _step)(_num)
  }
}
/** A UGen that performs overlap-and-add operation on a stream of input windows.
  * The `size` and `step` parameters are demand-rate, polled once per (input) window.
  *
  * @param in     the non-overlapped input
  * @param size   the window size in the input
  * @param step   the step between successive windows in the output.
  *               when smaller than `size`, the overlapping portions are summed together.
  *               Currently `step` values larger than `size` are clipped to `size`. This may
  *               change in the future
  *
  * @see [[Sliding]]
  */
final case class OverlapAdd[A](in: GE[A], size: GE.I, step: GE.I)(implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, step.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, step) = args: @unchecked
    val inC = in.cast[A]
    implicit val tpe: DataType.Num[A] = inC.tpe.castNum
    val out = stream.OverlapAdd(
      in    = inC .toElem,
      size  = size.toInt,
      step  = step.toInt,
    )
    tpe.mkStreamOut(out)
  }
}
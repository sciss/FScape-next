/*
 *  ReduceWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.graph.BinaryOp.Op
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.fscape.{GE, UGen, UGenGraph, UGenIn, UGenInLike, UGenSource, stream}
import de.sciss.lucre.Adjunct.{Num, NumInt}

import scala.collection.immutable.{IndexedSeq => Vec}

object ReduceWindow extends ProductReader[GE[_]] {
  import BinaryOp.{And, Max, Min, Or, Plus, Times, Xor}
  def plus  [A: Num   ](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Plus ())
  def times [A: Num   ](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Times())
  def min   [A: Num   ](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Min  ())
  def max   [A: Num   ](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Max  ())
  def and   [A: NumInt](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = And  ())
  def or    [A: NumInt](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Or   ())
  def xor   [A: NumInt](in: GE[A], size: GE.I): ReduceWindow[A] = apply(in, size = size, op = Xor  ())

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GE[_] = {
    require (arity == 3 && adj == 0)
    val _in   = in.readGE[Any]()
    val _size = in.readGE_I()
    in.readElem() match {
      case opId: Int =>
        new Legacy[Any, Int](_in, _size, opId)
      case _op0: Op[_, _,_] =>
        val _op = _op0.asInstanceOf[Op[Any, Any, Any]]
        new ReduceWindow[Any](_in, _size, _op)
      case other =>
        sys.error(s"Unexpected element $other")
    }
  }

  private final case class Legacy[A, B](in: GE[A], size: GE[B], op: Int) extends UGenSource.SingleOut[A] {
    protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
      UGenSource.unwrap(this, Vector(in.expand, size.expand))

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit builder: UGenGraph.Builder): UGenInLike[A] =
      UGen.SingleOut(this, args, adjuncts = UGen.Adjunct.Int(op) :: Nil)

    private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
      val Vec(in, size) = args: @unchecked
      val op0 = BinaryOp.Op.legacy(op)

      op0 match {
        case opII: BinaryOp.OpII if in.isInt  =>
          stream.ReduceWindow[Int   ](op0.name, opII.funII, in = in.toInt   , size = size.toInt): StreamOut
        case opLL: BinaryOp.OpLL if in.isLong || in.isInt =>
          stream.ReduceWindow[Long  ](op0.name, opLL.funLL, in = in.toLong  , size = size.toInt): StreamOut
        case opDD: BinaryOp.OpDD =>
          stream.ReduceWindow[Double](op0.name, opDD.funDD, in = in.toDouble, size = size.toInt): StreamOut
        case _ =>
          throw new AssertionError(op0.toString)
      }
    }
  }
}

/** A UGen that reduces all elements in each window to single values, for example
  * by calculating the sum or product.
  */
final case class ReduceWindow[A](in: GE[A], size: GE.I, op: BinaryOp.Op[A, A, A]) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGenSource.unwrap(this, Vector(in.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args, adjuncts = UGen.Adjunct.String(op.name) :: Nil)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size) = args: @unchecked

    val fun = op.apply
    val inC = in.cast[A]
    import inC.tpe
    val out = stream.ReduceWindow[A](op.name, fun, in = inC.toElem, size = size.toInt)
    tpe.mkStreamOut(out)
  }
}

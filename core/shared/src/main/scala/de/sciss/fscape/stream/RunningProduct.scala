/*
 *  RunningProduct.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2}
import de.sciss.fscape.stream.impl.{NodeImpl, RunningValueImpl, StageImpl}

object RunningProduct {
  def apply[A](in: Out[A], gate: OutI)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in  , stage.in0)
    b.connect(gate, stage.in1)
    stage.out
  }

  private final val name = "RunningProduct"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.I, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A] (s"${stage.name}.in"  ),
      in1 = InI    (s"${stage.name}.trig"),
      out = Out[A] (s"${stage.name}.out" )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: RunningValueImpl[_] = if (tpe.isDouble) {
        new RunningValueImpl[Double](stage.name, layer, shape.asInstanceOf[Shp[Double ]], 1.0)(_ * _)
      } else if (tpe.isInt) {
        new RunningValueImpl[Int   ](stage.name, layer, shape.asInstanceOf[Shp[Int    ]], 1  )(_ * _)
      } else {
        assert (tpe.isLong)
        new RunningValueImpl[Long  ](stage.name, layer, shape.asInstanceOf[Shp[Long   ]], 1L )(_ * _)
      }
      res.asInstanceOf[RunningValueImpl[A]]
    }
  }
}
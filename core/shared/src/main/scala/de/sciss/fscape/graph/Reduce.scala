/*
 *  Reduce.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.graph.BinaryOp.Op
import de.sciss.fscape.{GE, UGenGraph, UGenIn, UGenInLike}
import de.sciss.lucre.Adjunct.{Num, NumLogic}

import scala.collection.immutable.{IndexedSeq => Vec}

object Reduce extends ProductReader[GE[_]] {
  import BinaryOp.{And, Or, Xor, Max, Min, Plus, Times}
  /** Same result as `Mix( _ )` */
  def +  [A: Num](elem: GE[A]): Reduce[A] = apply(elem, Plus [A]())
  def *  [A: Num](elem: GE[A]): Reduce[A] = apply(elem, Times[A]() )
  //   def all_sig_==( elem: GE ) = ...
  //   def all_sig_!=( elem: GE ) = ...
  def min[A: Num](elem: GE[A]): Reduce[A] = apply(elem, Min  [A]())
  def max[A: Num](elem: GE[A]): Reduce[A] = apply(elem, Max  [A]())
  def &  [A: NumLogic](elem: GE[A]): Reduce[A] = apply(elem, And  [A]())
  def |  [A: NumLogic](elem: GE[A]): Reduce[A] = apply(elem, Or   [A]())
  def ^  [A: NumLogic](elem: GE[A]): Reduce[A] = apply(elem, Xor  [A]())

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GE[_] = {
    require (arity == 2 && adj == 0)
    val _elem = in.readGE[Any]()
    in.readElem() match {
      case opId: Int =>
        new Legacy[Any](_elem, opId)
      case _op0: Op[_, _,_]  =>
        val _op = _op0.asInstanceOf[Op[Any, Any, Any]]
        new Reduce[Any](_elem, _op)
      case other          => sys.error(s"Unexpected element $other")
    }
  }

  private final case class Legacy[A](elem: GE[A], opId: Int) extends GE.Lazy[A] {

    override def productPrefix: String = "Reduce" // serialization

    override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
      val args = elem.expand.outputs
      args match {
        case head +: tail =>
//          val op1 = BinaryOp.Op.legacy(opId).asInstanceOf[Op[A, A, A]]
          tail.foldLeft(head: GE /* UGenInLike*/[A]) { (a, b) =>
            BinaryOp.legacy(opId, a, b)
//            op1.make(a, b) // .expand
          }
        case _ => UGenInGroup.empty
      }
    }
  }
}

/** A UGen that reduces the channels of the input signal, so it become a single-channel signal. */
final case class Reduce[A](elem: GE[A], op: Op[A, A, A]) extends GE.Lazy[A] {
  override protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] = {
    val args = elem.expand.outputs
    val argsC = args.asInstanceOf[Vec[UGenIn[A]]]
    argsC match {
      case head +: tail =>
        tail.foldLeft(head: GE /* UGenInLike*/[A]) { (a, b) =>
          op.make(a, b) // .expand
        }
      case _ => UGenInGroup.empty
    }
  }
}

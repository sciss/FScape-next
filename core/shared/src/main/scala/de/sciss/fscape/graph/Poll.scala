/*
 *  Poll.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.StreamIn

import scala.collection.immutable.{IndexedSeq => Vec}

object Poll extends ProductReader[Poll[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Poll[_] = {
    require (arity == 3 && adj == 0)
    val _in     = in.readGE[Any]()
    val _gate   = in.readGE_B()
    val _label  = in.readString()
    new Poll(_in, _gate, _label)
  }
}
/** A UGen that prints snapshots of its input to the console.
  * Note that arguments have different order than in ScalaCollider!
  *
  * @param in     the input to be pulled. If this is a constant,
  *               the UGen will close after polling it. This is to
  *               prevent a dangling `Poll` whose trigger is
  *               infinite (such as `Impulse`). If you want to avoid
  *               that, you should wrap the input in a `DC`.
  * @param gate   gate that causes the UGen to print a snapshot
  *               of the input when open.
  * @param label  an identifying label to prepend to the printing.
  */
final case class Poll[A](in: GE[A], gate: GE.B, label: String = "poll") extends UGenSource.ZeroOut {
  protected def makeUGens(implicit b: UGenGraph.Builder): Unit =
    unwrap(this, Vector(in.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): Unit = {
    UGen.ZeroOut(this, inputs = args, adjuncts = Adjunct.String(label) :: Nil)
    ()
  }

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Unit = {
    val Vec(in, gate) = args: @unchecked
    import in.tpe
    stream.Poll[in.A](in = in.toElem, gate = gate.toInt, label = label)
  }
}
/*
 *  UGenSource.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import de.sciss.fscape.graph.UGenInGroup
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object UGenSource {
  trait ZeroOut       extends UGenSource[Unit, Unit]
  trait SingleOut[A]  extends SomeOut[A, StreamOut]
  trait MultiOut[/*+*/A] extends SomeOut[A, Vec[StreamOut]]

  protected sealed trait SomeOut[/*+*/A, S] extends UGenSource[UGenInLike[A], S] with GE.Lazy[A]

  def unwrap[A, S](source: UGenSource.SomeOut[A, S], args: Vec[UGenInLike[_]])
                  (implicit b: UGenGraph.Builder): UGenInLike[A] = {
    var uIns    = Vector.empty: Vec[UGenIn[_]]
    var uInsOk  = true
    var exp     = 0
    args.foreach(_.unbubble match {
      case u: UGenIn[_] => if (uInsOk) uIns :+= u
      case g: UGenInGroup[_] =>
        exp     = math.max(exp, g.numOutputs)
        uInsOk  = false // don't bother adding further UGenIns to uIns
    })
    if (uInsOk) {
      // aka uIns.size == args.size
      source.makeUGen(uIns)
    } else {
      // rewrap(args, exp)
      UGenInGroup(Vector.tabulate(exp)(i => unwrap(source, args.map(_.unwrap(i)))))
    }
  }

  def unwrap(source: UGenSource.ZeroOut, args: Vec[UGenInLike[_]])(implicit b: UGenGraph.Builder): Unit = {
    var uIns    = Vector.empty: Vec[UGenIn[_]]
    var uInsOk  = true
    var exp     = 0
    args.foreach(_.unbubble match {
      case u: UGenIn[_] => if (uInsOk) uIns :+= u
      case g: UGenInGroup[_] =>
        exp     = math.max(exp, g.numOutputs)
        uInsOk  = false // don't bother adding further UGenIns to uIns
    })
    if (uInsOk) {
      // aka uIns.size == args.size
      source.makeUGen(uIns)
    } else {
      // rewrap(args, exp)
      var i = 0
      while (i < exp) {
        unwrap(source, args.map(_.unwrap(i)))
        i += 1
      }
    }
  }

  /** Simple forwarder to `in.expand` that can be used to access
    * the otherwise package-private method.
    */
  def expand[A](in: GE[A])(implicit b: UGenGraph.Builder): UGenInLike[A] = in.expand

  /** Simple forwarder to `in.outputs` that can be used to access
    * the otherwise package-private method.
    */
  def outputs[A](in: UGenInLike[A]): Vec[UGenInLike[A]] = in.outputs

  /** Simple forwarder to `in.flatOutputs` that can be used to access
    * the otherwise package-private method.
    */
  def flatOutputs[A](in: UGenInLike[A]): Vec[UGenIn[A]] = in.flatOutputs

  /** Simple forwarder to `in.unbubble` that can be used to access
    * the otherwise package-private method.
    */
  def unbubble[A](in: UGenInLike[A]): UGenInLike[A] = in.unbubble

  /** Simple forwarder to `in.unwrap` that can be used to access
    * the otherwise package-private method.
    */
  def unwrapAt[A](in: UGenInLike[A], index: Int): UGenInLike[A] = in.unwrap(index)
}

sealed trait UGenSource[+U, S] extends Lazy.Expander[U] {
  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): U

  final def name: String = productPrefix

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): S
}
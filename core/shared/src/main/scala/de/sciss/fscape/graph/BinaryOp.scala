/*
 *  BinaryOp.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.graph.BinaryOp.Op
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{Builder, StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{BooleanTop, IntTop, Num, NumDiv, NumInt, NumLogic, ScalarOrd}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}
import de.sciss.numbers.{DoubleFunctions => rd, DoubleFunctions2 => rd2, IntFunctions => ri, IntFunctions2 => ri2, LongFunctions => rl, LongFunctions2 => rl2}

import scala.annotation.switch
import scala.collection.immutable.{IndexedSeq => Vec}

object BinaryOp extends ProductReader[UGenSource.SingleOut[_]] {
  private[fscape] def legacy[A, B, C](opId: Int, a: GE[A], b: GE[B]): GE[C] =
    new Legacy[A, B, C](opId, a, b)

  object Op {
    def legacy(id: Int): Op[_, _, _] = (id: @switch) match {
      case Plus               .id => Plus ()(LegacyAdjunct.Num)
      case Minus              .id => Minus()(LegacyAdjunct.Num)
      case Times              .id => Times()(LegacyAdjunct.Num)
      case Div                .id => Div  ()(LegacyAdjunct.NumDiv)
      case Mod                .id => Mod  ()(LegacyAdjunct.Num)
      case Eq                 .id => Eq   ()(LegacyAdjunct.ScalarEq)
      case Neq                .id => Neq  ()(LegacyAdjunct.ScalarEq)
      case Lt                 .id => Lt   ()(LegacyAdjunct.ScalarOrd)
      case Gt                 .id => Gt   ()(LegacyAdjunct.ScalarOrd)
      case Leq                .id => Leq  ()(LegacyAdjunct.ScalarOrd)
      case Geq                .id => Geq  ()(LegacyAdjunct.ScalarOrd)
      case Min                .id => Min  ()(LegacyAdjunct.NumInt)
      case Max                .id => Max  ()(LegacyAdjunct.NumInt)
      case And                .id => And  ()(LegacyAdjunct.NumInt)
      case Or                 .id => Or   ()(LegacyAdjunct.NumInt)
      case Xor                .id => Xor  ()(LegacyAdjunct.NumInt)
      case Lcm                .id => Lcm  ()(LegacyAdjunct.NumInt)
      case Gcd                .id => Gcd  ()(LegacyAdjunct.NumInt)
      case RoundTo            .id => RoundTo  ()(LegacyAdjunct.Num)
      case RoundUpTo          .id => RoundUpTo()(LegacyAdjunct.Num)
      case Trunc              .id => Trunc    ()(LegacyAdjunct.Num)
      case Atan2              .id => Atan2    ()
      case Hypot              .id => Hypot    ()
      case HypotApx           .id => HypotApx ()
      case Pow                .id => Pow      ()
      case LeftShift          .id => LeftShift          ()(LegacyAdjunct.NumInt)
      case RightShift         .id => RightShift         ()(LegacyAdjunct.NumInt)
      case UnsignedRightShift .id => UnsignedRightShift ()(LegacyAdjunct.NumInt)
      case Ring1              .id => Ring1    ()
      case Ring2              .id => Ring2    ()
      case Ring3              .id => Ring3    ()
      case Ring4              .id => Ring4    ()
      case DifSqr             .id => DifSqr   ()(LegacyAdjunct.Num)
      case SumSqr             .id => SumSqr   ()(LegacyAdjunct.Num)
      case SqrSum             .id => SqrSum   ()(LegacyAdjunct.Num)
      case SqrDif             .id => SqrDif   ()(LegacyAdjunct.Num)
      case AbsDif             .id => AbsDif   ()(LegacyAdjunct.Num)
      case Thresh             .id => Thresh   ()
      case AmClip             .id => AmClip   ()
      case ScaleNeg           .id => ScaleNeg ()
      case Clip2              .id => Clip2    ()(LegacyAdjunct.Num)
      case Excess             .id => Excess   ()(LegacyAdjunct.Num)
      case Fold2              .id => Fold2    ()(LegacyAdjunct.Num)
      case Wrap2              .id => Wrap2    ()(LegacyAdjunct.Num)
      case FirstArg           .id => FirstArg ()
      case SecondArg          .id => SecondArg()
      case ModJ               .id => ModJ     ()(LegacyAdjunct.Num)
    }

    final val MinId = Plus  .id
    final val MaxId = ModJ  .id
  }

  abstract class Op[A, B, C] extends Product {
    op =>

    def apply: (A, B) => C // (a: A, b: B): C

    override def productPrefix = s"BinaryOp$$$name"

    override def toString: String = name

    def name: String = plainName.capitalize

    private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[B])(implicit b: stream.Builder): StreamOut

    def make(a: GE[A], b: GE[B]): GE[C] =
      (a, b) match {
        case (av: Const[A], bv: Const[B]) => Const(apply(av.value, bv.value))
        case _ => BinaryOp(op /*.id*/, a, b)
      }

    private def plainName: String = {
      val cn = getClass.getName
      val sz = cn.length
      val i  = cn.indexOf('$') + 1
      cn.substring(i, if (cn.charAt(sz - 1) == '$') sz - 1 else sz)
    }
  }

  /** Legacy */
  sealed trait OpDD /*extends Op*/ {
    def funDD: (Double, Double) => Double
  }
  /** Legacy */
  sealed trait OpDI /*extends Op*/ {
    def funDI: (Double, Double) => Int
  }
  /** Legacy */
  sealed trait OpDL /*extends Op*/ {
    def funDL: (Double, Double) => Long
  }
  /** Legacy */
  sealed trait OpID /*extends Op*/ {
    def funID: (Int, Int) => Double
  }
  /** Legacy */
  sealed trait OpLD /*extends Op*/ {
    def funLD: (Long, Long) => Double
  }
  /** Legacy */
  sealed trait OpII /*extends Op*/ {
    def funII: (Int, Int) => Int
  }
  sealed trait OpLL /*extends Op*/ {
    def funLL: (Long, Long) => Long
  }
  /** Legacy */
  sealed trait OpIL /*extends Op*/ {
    def funIL: (Int, Int) => Long
  }
  /** Legacy */
  sealed trait OpLI /*extends Op*/ {
    def funLI: (Long, Long) => Int
  }

  abstract class OpSameBase[A] extends Op[A, A, A] with OpDD { op =>
    override private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in1.tpe
      val out = stream.BinaryOp[A, A, A](name, op.apply, in1 = in1.toElem, in2 = in2.toElem)
      tpe.mkStreamOut(out)
    }
  }

  abstract class OpSame[A] extends OpSameBase[A] with OpII with OpLL { op =>
    override private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in1.tpe
      val out = stream.BinaryOp[A, A, A](name, op.apply, in1 = in1.toElem, in2 = in2.toElem)
      tpe.mkStreamOut(out)
    }
  }

  object Plus extends ProductReader[Plus[_]] {
    final val id = 0

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Plus[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Plus[Any]()(_num)
    }
  }
  case class Plus[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.plus

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => a + b
    override val funII: (Int   , Int   ) => Int    = (a, b) => a + b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a + b
  }

  object Minus extends ProductReader[Minus[_]] {
    final val id = 1

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Minus[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Minus[Any]()(_num)
    }
  }
  case class Minus[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def make(a: GE[A], b: GE[A]): GE[A] =
      (a, b) match {
        case (av: Const[A], _) if av.value == num.zero => b
        case (_, bv: Const[A]) if bv.value == num.zero => a
        case _ => super.make(a, b)
      }

    override def apply: (A, A) => A = num.minus

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => a - b
    override val funII: (Int   , Int   ) => Int    = (a, b) => a - b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a - b
  }

  object Times extends ProductReader[Times[_]] {
    final val id = 2

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Times[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Times[Any]()(_num)
    }
  }
  case class Times[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def make(a: GE[A], b: GE[A]): GE[A] =
      (a, b) match {
          // N.B. do not replace by Constant(0), because lengths might differ!
//      case (Constant(0), _)  => a
//      case (_, Constant(0))  => b
      case (av: Const[A], _) if av.value == num.one  => b
      case (_, bv: Const[A]) if bv.value == num.one  => a
      case (av: Const[A], _) if av.value == num.negate(num.one) => UnaryOp.Neg[A]().make(b) // -b
      case (_, bv: Const[A]) if bv.value == num.negate(num.one) => UnaryOp.Neg[A]().make(a) // -a
      case _                 => super.make(a, b)
    }

    override def apply: (A, A) => A = num.times

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => a * b
    override val funII: (Int   , Int   ) => Int    = (a, b) => a * b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a * b
  }

  object Div extends ProductReader[Div[_]] {
    final val id = 4

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Div[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumDiv[Any] = in.readAdjunct()
      new Div[Any]()(_num)
    }
  }
  case class Div[A]()(implicit num: NumDiv[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: (A, A) => A = num.div

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => a / b
    override val funII: (Int   , Int   ) => Int    = (a, b) => a / b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a / b
  }

  object Mod extends ProductReader[Mod[_]] {
    final val id = 5

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Mod[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Mod[Any]()(_num)
    }
  }
  case class Mod[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.mod

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.mod(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.mod(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.mod(a, b)
  }

  abstract class OpPred[A] extends Op[A, A, Boolean] with OpDD with OpDI with OpII with OpLI { op =>
    override private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[A])(implicit b: Builder): StreamOut = {
      import in1.tpe
      stream.BinaryOp[A, A, Int](name, (a, b) => if (op.apply(a, b)) 1 else 0, in1 = in1.toElem, in2 = in2.toElem)
    }
  }

  object Eq extends ProductReader[Eq[_]] {
    final val id = 6

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Eq[_] = {
      require (arity == 0 && adj == 1)
      val _eq: LAdjunct.Eq[Any] with LAdjunct.Scalar[Any] = in.readAdjunct()
      new Eq[Any]()(_eq)
    }
  }
  case class Eq[A]()(implicit eq: LAdjunct.Eq[A] with LAdjunct.Scalar[A])
    extends OpPred[A] with ProductWithAdjuncts {

    override def adjuncts: List[LAdjunct] = eq :: Nil

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val eqC = eq.asInstanceOf[LAdjunct.Eq[_] with LAdjunct.Scalar[_]] match {
      case BooleanTop => IntTop.asInstanceOf[LAdjunct.Eq[A] with LAdjunct.Scalar[A]]
      case _          => eq
    }

    override def apply: (A, A) => Boolean = eqC.eq

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a == b) 1d else 0d
    override val funDI: (Double, Double) => Int    = (a, b) => if (a == b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a == b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a == b) 1 else 0
  }

  object Neq extends ProductReader[Neq[_]] {
    final val id = 7

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Neq[_] = {
      require (arity == 0 && adj == 1)
      val _eq: LAdjunct.Eq[Any] with LAdjunct.Scalar[Any] = in.readAdjunct()
      new Neq[Any]()(_eq)
    }
  }
  case class Neq[A]()(implicit eq: LAdjunct.Eq[A] with LAdjunct.Scalar[A])
    extends OpPred[A] with ProductWithAdjuncts {

    override def adjuncts: List[LAdjunct] = eq :: Nil

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val eqC = eq.asInstanceOf[LAdjunct.Eq[_] with LAdjunct.Scalar[_]] match {
      case BooleanTop => IntTop.asInstanceOf[LAdjunct.Eq[A] with LAdjunct.Scalar[A]]
      case _          => eq
    }

    override def apply: (A, A) => Boolean = eqC.neq

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a != b) 1d else 0d
    override val funDI: (Double, Double) => Int    = (a, b) => if (a != b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a != b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a != b) 1 else 0
  }

  object Lt extends ProductReader[Lt[_]] {
    final val id = 8

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Lt[_] = {
      require (arity == 0 && adj == 1)
      val _ord: ScalarOrd[Any] = in.readAdjunct()
      new Lt[Any]()(_ord)
    }
  }
  case class Lt[A]()(implicit ord: ScalarOrd[A]) extends OpPred[A] with ProductWithAdjuncts {
    override def apply: (A, A) => Boolean = ord.lt

    override def adjuncts: List[LAdjunct] = ord :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a < b) 1d else 0d // NOT rd.< !
    override val funDI: (Double, Double) => Int    = (a, b) => if (a < b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a < b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a < b) 1 else 0
  }

  object Gt extends ProductReader[Gt[_]] {
    final val id = 9

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Gt[_] = {
      require (arity == 0 && adj == 1)
      val _ord: ScalarOrd[Any] = in.readAdjunct()
      new Gt[Any]()(_ord)
    }
  }
  case class Gt[A]()(implicit ord: ScalarOrd[A]) extends OpPred[A] with ProductWithAdjuncts {
    override def apply: (A, A) => Boolean = ord.gt

    override def adjuncts: List[LAdjunct] = ord :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a > b) 1d else 0d // NOT rd.> !
    override val funDI: (Double, Double) => Int    = (a, b) => if (a > b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a > b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a > b) 1 else 0
  }

  object Leq extends ProductReader[Leq[_]] {
    final val id = 10

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Leq[_] = {
      require (arity == 0 && adj == 1)
      val _ord: ScalarOrd[Any] = in.readAdjunct()
      new Leq[Any]()(_ord)
    }
  }
  case class Leq[A]()(implicit ord: ScalarOrd[A]) extends OpPred[A] with ProductWithAdjuncts {
    override def apply: (A, A) => Boolean = ord.lteq

    override def adjuncts: List[LAdjunct] = ord :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a <= b) 1d else 0d // NOT rd.<= !
    override val funDI: (Double, Double) => Int    = (a, b) => if (a <= b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a <= b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a <= b) 1 else 0
  }

  object Geq extends ProductReader[Geq[_]] {
    final val id = 11

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Geq[_] = {
      require (arity == 0 && adj == 1)
      val _ord: ScalarOrd[Any] = in.readAdjunct()
      new Geq[Any]()(_ord)
    }
  }
  case class Geq[A]()(implicit ord: ScalarOrd[A]) extends OpPred[A] with ProductWithAdjuncts {
    override def apply: (A, A) => Boolean = ord.gteq

    override def adjuncts: List[LAdjunct] = ord :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => if (a >= b) 1d else 0d // NOT rd.>= !
    override val funDI: (Double, Double) => Int    = (a, b) => if (a >= b) 1 else 0
    override val funII: (Int   , Int   ) => Int    = (a, b) => if (a >= b) 1 else 0
    override val funLI: (Long  , Long  ) => Int    = (a, b) => if (a >= b) 1 else 0
  }

  object Min extends ProductReader[Min[_]] {
    final val id = 12

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Min[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Min[Any]()(_num)
    }
  }
  case class Min[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.min

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.min(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.min(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.min(a, b)
  }

  object Max extends ProductReader[Max[_]] {
    final val id = 13

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Max[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Max[Any]()(_num)
    }
  }
  case class Max[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: (A, A) => A = num.max

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.max(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.max(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.max(a, b)
  }

  object And extends ProductReader[And[_]] {
    final val id = 14

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): And[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumLogic[Any] = in.readAdjunct()
      new And[Any]()(_num)
    }
  }
  case class And[A]()(implicit num: NumLogic[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val numC = num.asInstanceOf[NumLogic[_]] match {
      case BooleanTop => IntTop.asInstanceOf[NumLogic[A]]
      case _          => num
    }

    override def apply: (A, A) => A = numC.and

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong & b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a & b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a & b
  }

  object Or extends ProductReader[Or[_]] {
    final val id = 15

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Or[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumLogic[Any] = in.readAdjunct()
      new Or[Any]()(_num)
    }
  }
  case class Or[A]()(implicit num: NumLogic[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val numC = num.asInstanceOf[NumLogic[_]] match {
      case BooleanTop => IntTop.asInstanceOf[NumLogic[A]]
      case _          => num
    }

    override def apply: (A, A) => A = numC.or

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong | b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a | b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a | b
  }

  object Xor extends ProductReader[Xor[_]] {
    final val id = 16

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Xor[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumLogic[Any] = in.readAdjunct()
      new Xor[Any]()(_num)
    }
  }
  case class Xor[A]()(implicit num: NumLogic[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    // An ugly way to account for the stream representation of booleans as integers
    private[this] val numC = num.asInstanceOf[NumLogic[_]] match {
      case BooleanTop => IntTop.asInstanceOf[NumLogic[A]]
      case _          => num
    }

    override def apply: (A, A) => A = numC.xor

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong ^ b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a ^ b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a ^ b
  }

  object Lcm extends ProductReader[Lcm[_]] {
    final val id = 17

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Lcm[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new Lcm[Any]()(_num)
    }
  }
  case class Lcm[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.lcm

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rl.lcm(a.toLong, b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.lcm(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.lcm(a, b)
  }

  object Gcd extends ProductReader[Gcd[_]] {
    final val id = 18

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Gcd[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new Gcd[Any]()(_num)
    }
  }
  case class Gcd[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.gcd

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rl.gcd(a.toLong, b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.gcd(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.gcd(a, b)
  }

  object RoundTo extends ProductReader[RoundTo[_]] {
    final val id = 19

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RoundTo[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new RoundTo[Any]()(_num)
    }
  }
  case class RoundTo[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: (A, A) => A = num.roundTo

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .roundTo(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri2.roundTo(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.roundTo(a, b)
  }

  object RoundUpTo extends ProductReader[RoundUpTo[_]] {
    final val id = 20

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RoundUpTo[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new RoundUpTo[Any]()(_num)
    }
  }
  case class RoundUpTo[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def apply: (A, A) => A = num.roundUpTo

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .roundUpTo(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri2.roundUpTo(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.roundUpTo(a, b)
  }

  object Trunc extends ProductReader[Trunc[_]] {
    final val id = 21

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Trunc[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Trunc[Any]()(_num)
    }
  }
  case class Trunc[A]()(implicit num: Num[A]) extends OpSameBase[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.trunc

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.trunc(a, b)
  }

  abstract class OpDouble extends Op[Double, Double, Double] with OpDD { op =>
    override private[BinaryOp] def makeStream(in1: StreamIn.T[Double], in2: StreamIn.T[Double])
                                             (implicit b: Builder): StreamOut =
      stream.BinaryOp[Double, Double, Double](name, op.apply, in1 = in1.toElem, in2 = in2.toElem)
  }

  object Atan2 extends ProductReader[Atan2] {
    final val id = 22

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Atan2 = {
      require (arity == 0 && adj == 0)
      new Atan2
    }
  }
  case class Atan2() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd.atan2(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.atan2(a, b)
  }

  object Hypot extends ProductReader[Hypot] {
    final val id = 23

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Hypot = {
      require (arity == 0 && adj == 0)
      new Hypot
    }
  }
  case class Hypot() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd.hypot(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.hypot(a, b)
  }

  object HypotApx extends ProductReader[HypotApx] {
    final val id = 24

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): HypotApx = {
      require (arity == 0 && adj == 0)
      new HypotApx
    }
  }
  case class HypotApx() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd.hypotApx(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.hypotApx(a, b)
  }

  /** '''Warning:''' Unlike a normal power operation, the signum of the
    * left operand is always preserved. I.e. `DC.kr(-0.5).pow(2)` will
    * not output `0.25` but `-0.25`. This is to avoid problems with
    * floating point noise and negative input numbers, so
    * `DC.kr(-0.5).pow(2.001)` does not result in a `NaN`, for example.
    */
  object Pow extends ProductReader[Pow] {
    final val id = 25

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Pow = {
      require (arity == 0 && adj == 0)
      new Pow
    }
  }
  case class Pow() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd.pow(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.pow(a, b)
  }

  object LeftShift extends ProductReader[LeftShift[_]] {
    final val id = 26

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): LeftShift[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new LeftShift[Any]()(_num)
    }
  }
  case class LeftShift[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.shiftLeft

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong << b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a << b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a << b
  }

  object RightShift extends ProductReader[RightShift[_]] {
    final val id = 27

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RightShift[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new RightShift[Any]()(_num)
    }
  }
  case class RightShift[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.shiftRight

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong >> b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a >> b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a >> b
  }

  object UnsignedRightShift extends ProductReader[UnsignedRightShift[_]] {
    final val id = 28

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UnsignedRightShift[_] = {
      require (arity == 0 && adj == 1)
      val _num: NumInt[Any] = in.readAdjunct()
      new UnsignedRightShift[Any]()(_num)
    }
  }
  case class UnsignedRightShift[A]()(implicit num: NumInt[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.unsignedShiftRight

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => (a.toLong >>> b.toLong).toDouble
    override val funII: (Int   , Int   ) => Int    = (a, b) => a >>> b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a >>> b
  }

  // case object Fill           extends Op( 29 )

  object Ring1 extends ProductReader[Ring1] {
    final val id = 30

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ring1 = {
      require (arity == 0 && adj == 0)
      new Ring1
    }
  }
  case class Ring1() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.ring1(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.ring1(a, b)
  }

  object Ring2 extends ProductReader[Ring2] {
    final val id = 31

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ring2 = {
      require (arity == 0 && adj == 0)
      new Ring2
    }
  }
  case class Ring2() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.ring2(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.ring2(a, b)
  }

  object Ring3 extends ProductReader[Ring3] {
    final val id = 32

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ring3 = {
      require (arity == 0 && adj == 0)
      new Ring3
    }
  }
  case class Ring3() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.ring3(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.ring3(a, b)
  }

  object Ring4 extends ProductReader[Ring4] {
    final val id = 33

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Ring4 = {
      require (arity == 0 && adj == 0)
      new Ring4
    }
  }
  case class Ring4() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.ring4(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.ring4(a, b)
  }

//
//  sealed trait LongOp extends Op with OpIL with OpLL {
////    override def apply(a: Constant, b: Constant): Constant = (a, b) match {
////      case (ConstantI(av), ConstantI(bv)) => ConstantL(funIL(av, bv))
////      case (ConstantL(av), ConstantL(bv)) => ConstantL(funLL(av, bv))
////      case (ConstantL(av), ConstantI(bv)) => ConstantL(funLL(av, bv))
////      case (ConstantI(av), ConstantL(bv)) => ConstantL(funLL(av, bv))
////      case _                              => ConstantD(funDD(a.doubleValue, b.doubleValue))
////    }
//  }

  object DifSqr extends ProductReader[DifSqr[_]] {
    final val id = 34

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DifSqr[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new DifSqr[Any]()(_num)
    }
  }

  sealed trait OpXL extends OpDD with OpIL with OpLL

  case class DifSqr[A]()(implicit num: Num[A]) extends OpSameBase[A] with OpXL with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.difSqr

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .difSqr(a, b)
    override val funIL: (Int   , Int   ) => Long   = (a, b) => ri2.difSqr(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.difSqr(a, b)
  }

  object SumSqr extends ProductReader[SumSqr[_]] {
    final val id = 35

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SumSqr[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new SumSqr[Any]()(_num)
    }
  }
  case class SumSqr[A]()(implicit num: Num[A]) extends OpSameBase[A] with OpXL with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.sumSqr

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .sumSqr(a, b)
    override val funIL: (Int   , Int   ) => Long   = (a, b) => ri2.sumSqr(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.sumSqr(a, b)
  }

  object SqrSum extends ProductReader[SqrSum[_]] {
    final val id = 36

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SqrSum[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new SqrSum[Any]()(_num)
    }
  }
  case class SqrSum[A]()(implicit num: Num[A]) extends OpSameBase[A] with OpXL with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.sqrSum

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .sqrSum(a, b)
    override val funIL: (Int   , Int   ) => Long   = (a, b) => ri2.sqrSum(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.sqrSum(a, b)
  }

  object SqrDif extends ProductReader[SqrDif[_]] {
    final val id = 37

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SqrDif[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new SqrDif[Any]()(_num)
    }
  }
  case class SqrDif[A]()(implicit num: Num[A]) extends OpSameBase[A] with OpXL with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.sqrDif

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .sqrDif(a, b)
    override val funIL: (Int   , Int   ) => Long   = (a, b) => ri2.sqrDif(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.sqrDif(a, b)
  }

  object AbsDif extends ProductReader[AbsDif[_]] {
    final val id = 38

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): AbsDif[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new AbsDif[Any]()(_num)
    }
  }
  case class AbsDif[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.absDif

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd .absDif(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri2.absDif(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl2.absDif(a, b)
  }

  object Thresh extends ProductReader[Thresh] {
    final val id = 39

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Thresh = {
      require (arity == 0 && adj == 0)
      new Thresh
    }
  }
  case class Thresh() extends OpDouble {
    override def apply: (Double, Double) => Double = rd2.thresh

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.thresh(a, b)
  }

  object AmClip extends ProductReader[AmClip] {
    final val id = 40

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): AmClip = {
      require (arity == 0 && adj == 0)
      new AmClip
    }
  }
  case class AmClip() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.amClip(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.amClip(a, b)
  }

  object ScaleNeg extends ProductReader[ScaleNeg] {
    final val id = 41

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ScaleNeg = {
      require (arity == 0 && adj == 0)
      new ScaleNeg
    }
  }
  case class ScaleNeg() extends OpDouble {
    override def apply: (Double, Double) => Double = (a, b) => rd2.scaleNeg(a, b)

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd2.scaleNeg(a, b)
  }

  object Clip2 extends ProductReader[Clip2[_]] {
    final val id = 42

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Clip2[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Clip2[Any]()(_num)
    }
  }
  case class Clip2[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.clip2

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.clip2(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.clip2(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.clip2(a, b)
  }

  object Excess extends ProductReader[Excess[_]] {
    final val id = 43

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Excess[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Excess[Any]()(_num)
    }
  }
  case class Excess[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.excess

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.excess(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.excess(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.excess(a, b)
  }

  object Fold2 extends ProductReader[Fold2[_]] {
    final val id = 44

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Fold2[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Fold2[Any]()(_num)
    }
  }
  case class Fold2[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.fold2

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.fold2(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.fold2(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.fold2(a, b)
  }

  object Wrap2 extends ProductReader[Wrap2[_]] {
    final val id = 45

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Wrap2[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new Wrap2[Any]()(_num)
    }
  }
  case class Wrap2[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {

    override def apply: (A, A) => A = num.wrap2

    override def adjuncts: List[LAdjunct] = num :: Nil

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => rd.wrap2(a, b)
    override val funII: (Int   , Int   ) => Int    = (a, b) => ri.wrap2(a, b)
    override val funLL: (Long  , Long  ) => Long   = (a, b) => rl.wrap2(a, b)
  }

  object FirstArg extends ProductReader[FirstArg[_, _]] {
    final val id = 46

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FirstArg[_, _] = {
      require (arity == 0 && adj == 0)
      new FirstArg[Any, Any]
    }
  }
  case class FirstArg[A, B]() extends Op[A, B, A] with OpDD with OpII with OpLL { op =>

    override def apply: (A, B) => A = (a, _) => a

    override private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[B])
                                             (implicit b: Builder): StreamOut = {
      implicit val tpe1: DataType[A] = in1.tpe
      implicit val tpe2: DataType[B] = in2.tpe
      val out = stream.BinaryOp[A, B, A](name, op.apply, in1 = in1.toElem, in2 = in2.toElem)
      tpe1.mkStreamOut(out)
    }

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, _) => a
    override val funII: (Int   , Int   ) => Int    = (a, _) => a
    override val funLL: (Long  , Long  ) => Long   = (a, _) => a
  }

  // case object Rrand          extends Op( 47 )
  // case object ExpRRand       extends Op( 48 )

  object SecondArg extends ProductReader[SecondArg[_, _]] {
    final val id = 100

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SecondArg[_, _] = {
      require (arity == 0 && adj == 0)
      new SecondArg[Any, Any]
    }
  }
  case class SecondArg[A, B]() extends Op[A, B, B] with OpDD with OpII with OpLL { op =>

    override def apply: (A, B) => B = (_, b) => b

    override private[BinaryOp] def makeStream(in1: StreamIn.T[A], in2: StreamIn.T[B])
                                             (implicit b: Builder): StreamOut = {
      implicit val tpe1: DataType[A] = in1.tpe
      implicit val tpe2: DataType[B] = in2.tpe
      val out = stream.BinaryOp[A, B, B](name, op.apply, in1 = in1.toElem, in2 = in2.toElem)
      tpe2.mkStreamOut(out)
    }

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (_, b) => b
    override val funII: (Int   , Int   ) => Int    = (_, b) => b
    override val funLL: (Long  , Long  ) => Long   = (_, b) => b
  }

  object ModJ extends ProductReader[ModJ[_]] {
    final val id = 101

    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ModJ[_] = {
      require (arity == 0 && adj == 1)
      val _num: Num[Any] = in.readAdjunct()
      new ModJ[Any]()(_num)
    }
  }
  case class ModJ[A]()(implicit num: Num[A]) extends OpSame[A] with ProductWithAdjuncts {
    override def adjuncts: List[LAdjunct] = num :: Nil

    override def apply: (A, A) => A = num.rem

    // ---- legacy ----
    override val funDD: (Double, Double) => Double = (a, b) => a % b
    override val funII: (Int   , Int   ) => Int    = (a, b) => a % b
    override val funLL: (Long  , Long  ) => Long   = (a, b) => a % b
  }

  // ----

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): UGenSource.SingleOut[_] = {
    require (arity == 3 && adj == 0)
    in.readElem() match {
      case opId: Int =>
        val _a  = in.readGE[Any]()
        val _b  = in.readGE[Any]()
        Legacy[Any, Any, Any](opId, _a, _b)

      case x: Op[_, _,_]  =>
        val _op0: Op[_, _, _] = x
        val _op = _op0.asInstanceOf[Op[Any, Any, Any]]
        val _a  = in.readGE[Any]()
        val _b  = in.readGE[Any]()
        new BinaryOp(_op, _a, _b)
      case other          => sys.error(s"Unexpected element $other")
    }
  }

  // ---- legacy

  private final case class Legacy[A, B, C](opId: Int, a: GE[A], b: GE[B]) extends UGenSource.SingleOut[C] {
    override def productPrefix: String = "BinaryOp" // serialization

    protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[C] =
      unwrap(this, Vector(a.expand, b.expand))

    protected def makeUGen(args: Vec[UGenIn[_]])(implicit builder: UGenGraph.Builder): UGenInLike[C] =
      UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.Int(opId) :: Nil)

    override private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
      val Vec(in1, in2) = args: @unchecked
      val op0 = Op.legacy(opId)

      if (in1.isDouble || in2.isDouble) {
        op0 match {
          case opDI: OpDI =>
            stream.BinaryOp[Double, Double, Int](op0.name, opDI.funDI, in1 = in1.toDouble, in2 = in2.toDouble): StreamOut

          case opDL: OpDL =>
            stream.BinaryOp[Double, Double, Long](op0.name, opDL.funDL, in1 = in1.toDouble, in2 = in2.toDouble): StreamOut

          case opDD: OpDD =>
            stream.BinaryOp[Double, Double, Double](op0.name, opDD.funDD, in1 = in1.toDouble, in2 = in2.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }
      } else if (in1.isLong || in2.isLong) {
        op0 match {
          case opLI: OpLI =>
            stream.BinaryOp[Long, Long, Int](op0.name, opLI.funLI, in1 = in1.toLong, in2 = in2.toLong): StreamOut

          case opLL: OpLL =>
            stream.BinaryOp[Long, Long, Long](op0.name, opLL.funLL, in1 = in1.toLong, in2 = in2.toLong): StreamOut

          case opLD: OpLD =>
            stream.BinaryOp[Long, Long, Double](op0.name, opLD.funLD, in1 = in1.toLong, in2 = in2.toLong): StreamOut

          case opDD: OpDD =>
            stream.BinaryOp[Double, Double, Double](op0.name, opDD.funDD, in1 = in1.toDouble, in2 = in2.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }

      } else {
        assert (in1.isInt && in2.isInt)
        op0 match {
          case opII: OpII =>
            stream.BinaryOp[Int, Int, Int](op0.name, opII.funII, in1 = in1.toInt, in2 = in2.toInt): StreamOut

          case opIL: OpIL =>
            stream.BinaryOp[Int, Int, Long](op0.name, opIL.funIL, in1 = in1.toInt, in2 = in2.toInt): StreamOut

          case opID: OpID =>
            stream.BinaryOp[Int, Int, Double](op0.name, opID.funID, in1 = in1.toInt, in2 = in2.toInt): StreamOut

          case opDD: OpDD =>
            stream.BinaryOp[Double, Double, Double](op0.name, opDD.funDD, in1 = in1.toDouble, in2 = in2.toDouble): StreamOut

          case _ =>
            throw new AssertionError(op0.toString)
        }
      }
    }
  }
}

/** A binary operator UGen, for example two sum or multiply two signals.
  * The left or `a` input is "hot", i.e. it keeps the UGen running,
  * while the right or `b` input may close early, and the last value will
  * be remembered.
  *
  * @param op   the operator (e.g. `BinaryOp.Times`)
  * @param a    the left operand which determines how long the UGen computes
  * @param b    the right operand.
  */
final case class BinaryOp[A, B, C](op: Op[A, B, C], a: GE[A], b: GE[B]) extends UGenSource.SingleOut[C] {

  protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[C] =
    unwrap(this, Vector(a.expand, b.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit builder: UGenGraph.Builder): UGenInLike[C] =
    UGen.SingleOut(this, inputs = args, adjuncts = Adjunct.String(op.name) :: Nil)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in1, in2) = args: @unchecked
    val in1C = in1.cast[A]
    val in2C = in2.cast[B]
    op.makeStream(in1C, in2C)
  }
}
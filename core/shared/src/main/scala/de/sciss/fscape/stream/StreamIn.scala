/*
 *  StreamIn.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.Outlet
import de.sciss.lucre.Adjunct.{DoubleTop, IntTop, LongTop, ScalarNum}

import java.util
import scala.annotation.switch
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.language.implicitConversions

object StreamIn {
  def singleD(peer: OutD): StreamIn = new SingleD(peer)
  def singleI(peer: OutI): StreamIn = new SingleI(peer)
  def singleL(peer: OutL): StreamIn = new SingleL(peer)

  def multiD(peer: OutD, numSinks: Int): StreamIn = new MultiD(peer, numSinks)
  def multiI(peer: OutI, numSinks: Int): StreamIn = new MultiI(peer, numSinks)
  def multiL(peer: OutL, numSinks: Int): StreamIn = new MultiL(peer, numSinks)

  object unused extends StreamIn {
    private def unsupported(method: String): Nothing =
      throw new UnsupportedOperationException(s"StreamIn.unused.$method")

    def toAny   (implicit b: Builder): Outlet[Buf]  = unsupported("toAny")
    def toDouble(implicit b: Builder): OutD             = unsupported("toDouble")
    def toInt   (implicit b: Builder): OutI             = unsupported("toInt")
    def toLong  (implicit b: Builder): OutL             = unsupported("toLong")
    def toElem  (implicit b: Builder): Outlet[Buf]      = unsupported("toElem")

    override def isInt    : Boolean = false
    override def isLong   : Boolean = false
    override def isBoolean: Boolean = false
    override def isDouble : Boolean = true    // arbitrary

    // type Elem = Nothing

    def tpe: DataType[A] = unsupported("tpe")
  }

  object DoubleType extends DataType.Num[Double] {
    val ordering: Ordering[Double] = implicitly[Ordering[Double]] // Ordering.Double

    val peer: ScalarNum[Double] = DoubleTop

    final val zero      = 0.0
    final val minValue  = Double.NegativeInfinity
    final val maxValue  = Double.PositiveInfinity

    final def mkStreamOut(out: OutD): StreamOut = out

    override def allocBuf()(implicit allocator: Allocator): BufD = allocator.borrowBufD()

    override def newArray(size: Int): Array[Double] = new Array(size)

    override def fill(a: Array[Double], off: Int, len: Int, elem: Double): Unit =
      util.Arrays.fill(a, off, off + len, elem)

    override def clear(a: Array[Double], off: Int, len: Int): Unit =
      util.Arrays.fill(a, off, off + len, 0.0)

    override def add(in: Array[Double], inOff: Int, out: Array[Double], outOff: Int, len: Int): Unit =
      Util.add(in, inOff, out, outOff, len)

    override def reverse(a: Array[Double], off: Int, len: Int): Unit = {
      var i = off
      var j = i + len - 1
      while (i < j) {
        val tmp = a(i)
        a(i) = a(j)
        a(j) = tmp
        i += 1
        j -= 1
      }
    }

    override def isInt    : Boolean = false
    override def isLong   : Boolean = false
    override def isDouble : Boolean = true
//    override def isBoolean: Boolean = false
  }

  trait DoubleLike extends StreamIn {
    final override def isInt    : Boolean = false
    final override def isLong   : Boolean = false
    final override def isDouble : Boolean = true
    final override def isBoolean: Boolean = false

    final def toAny (implicit b: Builder): Outlet[Buf]  = toDouble.as[Buf]  // retarded Akka API. Why is Outlet not covariant?
    final def toElem(implicit b: Builder): OutD         = toDouble

    final type A    = Double
//    final type Buf  = BufD

    final def tpe: DataType[A] = DoubleType
  }

  private final class SingleD(peer: OutD) extends DoubleLike {
    private[this] var exhausted = false

    def toDouble(implicit b: Builder): OutD = {
      require(!exhausted)
      exhausted = true
      peer
    }

    def toInt(implicit builder: Builder): OutI = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "D.toInt") { bufD =>
        val bufI = allocator.borrowBufI()
        val sz = bufD.size
        bufI.size = sz
        val a = bufD.buf
        val b = bufI.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i)  = math.max(Int.MinValue, math.min(Int.MaxValue, math.round(x))).toInt
          i += 1
        }
        bufD.release()
        bufI
      }
    }

    def toLong(implicit builder: Builder): OutL = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "D.toLong") { bufD =>
        val bufL = allocator.borrowBufL()
        val sz = bufD.size
        bufL.size = sz
        val a = bufD.buf
        val b = bufL.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i) = math.round(x)
          i += 1
        }
        bufD.release()
        bufL
      }
    }
  }

  object IntType extends DataType.Num[Int] {
    val ordering: Ordering[Int] = Ordering.Int

    val peer: ScalarNum[Int] = IntTop

    final val zero      = 0
    final val minValue  = Int.MinValue
    final val maxValue  = Int.MaxValue

    final def mkStreamOut(out: OutI): StreamOut = out

    override def allocBuf()(implicit allocator: Allocator): BufI = allocator.borrowBufI()

    def newArray(size: Int): Array[Int] = new Array(size)

    def fill(a: Array[Int], off: Int, len: Int, elem: Int): Unit =
      util.Arrays.fill(a, off, off + len, elem)

    def clear(a: Array[Int], off: Int, len: Int): Unit =
      util.Arrays.fill(a, off, off + len, 0)

    override def add(in: Array[Int], inOff: Int, out: Array[Int], outOff: Int, len: Int): Unit =
      Util.add(in, inOff, out, outOff, len)

    def reverse(a: Array[Int], off: Int, len: Int): Unit = {
      var i = off
      var j = i + len - 1
      while (i < j) {
        val tmp = a(i)
        a(i) = a(j)
        a(j) = tmp
        i += 1
        j -= 1
      }
    }

    override def isInt    : Boolean = true
    override def isLong   : Boolean = false
    override def isDouble : Boolean = false
//    override def isBoolean: Boolean = false
  }

  trait IntLike extends StreamIn {
    final override def isInt    : Boolean = true
    final override def isLong   : Boolean = false
    final override def isDouble : Boolean = false
    final override def isBoolean: Boolean = false

    final def toAny (implicit b: Builder): Outlet[Buf]  = toInt.as[Buf]
    final def toElem(implicit b: Builder): OutI             = toInt

    final type A    = Int
//    final type Buf  = BufI

    final def tpe: DataType[Int] = IntType
  }

  private final class SingleI(peer: OutI) extends IntLike {
    private[this] var exhausted = false

    def toInt(implicit b: Builder): OutI = {
      require(!exhausted)
      exhausted = true
      peer
    }

    def toLong(implicit builder: Builder): OutL = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "I.toLong") { bufI =>
        val bufL = allocator.borrowBufL()
        val sz = bufI.size
        bufL.size = sz
        val a = bufI.buf
        val b = bufL.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i)  = x.toLong
          i += 1
        }
        bufI.release()
        bufL
      }
    }

    def toDouble(implicit builder: Builder): OutD = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "I.toDouble") { bufI =>
        val bufD = allocator.borrowBufD()
        val sz = bufI.size
        bufD.size = sz
        val a = bufI.buf
        val b = bufD.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i) = x.toDouble
          i += 1
        }
        bufI.release()
        bufD
      }
    }
  }

  object LongType extends DataType.Num[Long] {
    val ordering: Ordering[Long] = Ordering.Long

    val peer: ScalarNum[Long] = LongTop

    final val zero      = 0L
    final val minValue  = Long.MinValue
    final val maxValue  = Long.MaxValue

    final def mkStreamOut(out: OutL): StreamOut = out

    override def allocBuf()(implicit allocator: Allocator): BufL = allocator.borrowBufL()

    def newArray(size: Int): Array[Long] = new Array(size)

    def fill(a: Array[Long], off: Int, len: Int, elem: Long): Unit =
      util.Arrays.fill(a, off, off + len, elem)

    def clear(a: Array[Long], off: Int, len: Int): Unit =
      util.Arrays.fill(a, off, off + len, 0L)

    override def add(in: Array[Long], inOff: Int, out: Array[Long], outOff: Int, len: Int): Unit =
      Util.add(in, inOff, out, outOff, len)

    def reverse(a: Array[Long], off: Int, len: Int): Unit = {
      var i = off
      var j = i + len - 1
      while (i < j) {
        val tmp = a(i)
        a(i) = a(j)
        a(j) = tmp
        i += 1
        j -= 1
      }
    }

    def isInt   : Boolean = false
    def isLong  : Boolean = true
    def isDouble: Boolean = false
  }

  trait LongLike extends StreamIn {
    final override def isInt     : Boolean = false
    final override def isLong    : Boolean = true
    final override def isDouble  : Boolean = false
    final override def isBoolean : Boolean = false

    final def toAny (implicit b: Builder): Outlet[Buf]  = toLong.as[Buf]
    final def toElem(implicit b: Builder): OutL             = toLong

    final type A    = Long
//    final type Buf  = BufL

    final def tpe: DataType[Long] = LongType
  }

//  object BooleanType extends StreamType[Boolean, BufB] {
//    val ordering: Ordering[Boolean] = Ordering.Boolean
//
//    final val zero      = false
//    final val minValue  = false
//    final val maxValue  = true
//
//    final def mkStreamOut(out: OutL): StreamOut = out
//
//    override def allocBuf()(implicit allocator: Allocator): BufB = allocator.borrowBufB()
//
//    def newArray(size: Int): Array[Boolean] = new Array(size)
//
//    def fill(a: Array[Boolean], off: Int, len: Int, elem: Boolean): Unit =
//      util.Arrays.fill(a, off, off + len, elem)
//
//    def clear(a: Array[Boolean], off: Int, len: Int): Unit =
//      util.Arrays.fill(a, off, off + len, false)
//
//    def reverse(a: Array[Boolean], off: Int, len: Int): Unit = {
//      var i = off
//      var j = i + len - 1
//      while (i < j) {
//        val tmp = a(i)
//        a(i) = a(j)
//        a(j) = tmp
//        i += 1
//        j -= 1
//      }
//    }
//
//    override def isInt      : Boolean = false
//    override def isBoolean  : Boolean = true
//    override def isDouble   : Boolean = false
//    override def isLong     : Boolean = false
//  }

  private final class SingleL(peer: OutL) extends LongLike {
    private[this] var exhausted = false

    def toLong(implicit b: Builder): OutL = {
      require(!exhausted)
      exhausted = true
      peer
    }

    def toInt(implicit builder: Builder): OutI = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "L.toInt") { bufL =>
        val bufI = allocator.borrowBufI()
        val sz = bufL.size
        bufI.size = sz
        val a = bufL.buf
        val b = bufI.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i)  = math.max(Int.MinValue, math.min(Int.MaxValue, x)).toInt
          i += 1
        }
        bufL.release()
        bufI
      }
    }

    def toDouble(implicit builder: Builder): OutD = {
      require(!exhausted)
      exhausted = true
      val allocator = builder.allocator
      builder.map(peer, "L.toDouble") { bufL =>
        val bufD = allocator.borrowBufD()
        val sz = bufL.size
        bufD.size = sz
        val a = bufL.buf
        val b = bufD.buf
        var i = 0
        while (i < sz) {
          val x = a(i)
          b(i) = x.toDouble
          i += 1
        }
        bufL.release()
        bufD
      }
    }
  }

  private final class MultiD(peer: OutD, numSinks: Int) extends DoubleLike {
    private[this] var remain = numSinks
    private[this] var broad: Vec[OutD] = _ // create lazily because we need stream.Builder

    private def alloc()(implicit b: Builder): OutD = {
      require(remain > 0)
      if (broad == null) broad = Broadcast(peer, numSinks)
      remain -= 1
      val head +: tail = broad
      broad = tail
      head
    }

    def toDouble(implicit b: Builder): OutD = alloc()
    def toInt   (implicit b: Builder): OutI = singleD(alloc()).toInt   // just reuse this functionality
    def toLong  (implicit b: Builder): OutL = singleD(alloc()).toLong  // just reuse this functionality
  }

  private final class MultiI(peer: OutI, numSinks: Int) extends IntLike {
    private[this] var remain = numSinks
    private[this] var broad: Vec[OutI] = _ // create lazily because we need stream.Builder

    private def alloc()(implicit b: Builder): OutI = {
      require(remain > 0)
      if (broad == null) broad = Broadcast(peer, numSinks)
      remain -= 1
      val head +: tail = broad
      broad = tail
      head
    }

    def toDouble(implicit b: Builder): OutD = singleI(alloc()).toDouble   // just reuse this functionality
    def toInt   (implicit b: Builder): OutI = alloc()
    def toLong  (implicit b: Builder): OutL = singleI(alloc()).toLong     // just reuse this functionality
  }

  private final class MultiL(peer: OutL, numSinks: Int) extends LongLike {
    private[this] var remain = numSinks
    private[this] var broad: Vec[OutL] = _ // create lazily because we need stream.Builder

    private def alloc()(implicit b: Builder): OutL = {
      require(remain > 0)
      if (broad == null) broad = Broadcast(peer, numSinks)
      remain -= 1
      val head +: tail = broad
      broad = tail
      head
    }

    def toDouble(implicit b: Builder): OutD = singleL(alloc()).toDouble  // just reuse this functionality
    def toInt   (implicit b: Builder): OutI = singleL(alloc()).toInt     // just reuse this functionality
    def toLong  (implicit b: Builder): OutL = alloc()
  }

  type T[A1] = StreamIn { type A = A1 }
}
trait StreamIn {
  type A
//  type Buf >: Null <: Buf.E[A]
  type Buf = Buf.E[A]

  /** N.B. ConstantB reports `true` for this */
  def isInt     : Boolean
  def isLong    : Boolean
  def isDouble  : Boolean
  def isBoolean : Boolean

  def toInt   (implicit b: Builder): OutI
  def toLong  (implicit b: Builder): OutL
  def toDouble(implicit b: Builder): OutD
  def toAny   (implicit b: Builder): Outlet[Buf]
  def toElem  (implicit b: Builder): Out[A]

  def cast[B]: StreamIn.T[B] = this.asInstanceOf[StreamIn.T[B]]

  implicit def tpe: DataType[A]
}

object StreamOut {
  implicit def fromDouble   (peer:     OutD ):     StreamOut  = new StreamOutD(peer)
  implicit def fromDoubleVec(peer: Vec[OutD]): Vec[StreamOut] = peer.map(new StreamOutD(_))
  implicit def fromInt      (peer:     OutI ):     StreamOut  = new StreamOutI(peer)
  implicit def fromIntVec   (peer: Vec[OutI]): Vec[StreamOut] = peer.map(new StreamOutI(_))
  implicit def fromLong     (peer:     OutL ):     StreamOut  = new StreamOutL(peer)
  implicit def fromLongVec  (peer: Vec[OutL]): Vec[StreamOut] = peer.map(new StreamOutL(_))

  private final class StreamOutD(peer: OutD) extends StreamOut {
    override def toString = s"StreamOut($peer)"

    def toIn(numChildren: Int)(implicit b: stream.Builder): StreamIn = (numChildren: @switch) match {
      case 0 =>
        SinkIgnore(peer)
        StreamIn.unused
      case 1 => StreamIn.singleD(peer)
      case _ => StreamIn.multiD (peer, numChildren)
    }
  }

  private final class StreamOutI(peer: OutI) extends StreamOut {
    override def toString = s"StreamOut($peer)"

    def toIn(numChildren: Int)(implicit b: stream.Builder): StreamIn = (numChildren: @switch) match {
      case 0 =>
        SinkIgnore(peer)
        StreamIn.unused
      case 1 => StreamIn.singleI(peer)
      case _ => StreamIn.multiI (peer, numChildren)
    }
  }

  private final class StreamOutL(peer: OutL) extends StreamOut {
    override def toString = s"StreamOut($peer)"

    def toIn(numChildren: Int)(implicit b: stream.Builder): StreamIn = (numChildren: @switch) match {
      case 0 =>
        SinkIgnore(peer)
        StreamIn.unused
      case 1 => StreamIn.singleL(peer)
      case _ => StreamIn.multiL (peer, numChildren)
    }
  }
}
trait StreamOut {
  def toIn(numChildren: Int)(implicit b: stream.Builder): StreamIn
}
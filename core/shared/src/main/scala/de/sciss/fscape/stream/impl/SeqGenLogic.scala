/*
 *  SeqGenLogic.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream
package impl

import akka.stream.{FanInShape3, Inlet}
import de.sciss.fscape.DataType
import de.sciss.fscape.stream.impl.Handlers._

import scala.annotation.tailrec
import scala.math.min

final class SeqGenLogicI(name: String, shape: FanInShape3[Buf.I, Buf.I, Buf.L, Buf.I], layer: Layer)
                        (inc: (Int, Int) => Int)
                        (implicit a: Allocator)
  extends SeqGenLogic[Int](name, shape, layer)  {

  protected override val hStep : InIAux    = InIAux  (this, shape.in1)()
  protected override val hOut  : OutIMain  = OutIMain(this, shape.out)
  
  protected def run(x: Int, n: Int): Int = {
    var _x = x
    var i = 0
    while (i < n) {
      hOut.next(_x)
      val _step = hStep.next()
      _x = inc(_x, _step)
      i += 1
    }
    _x
  }
}

final class SeqGenLogicL(name: String, shape: FanInShape3[Buf.L, Buf.L, Buf.L, Buf.L], layer: Layer)
                        (inc: (Long, Long) => Long)
                        (implicit a: Allocator)
  extends SeqGenLogic[Long](name, shape, layer)  {

  protected override val hStep : InLAux    = InLAux  (this, shape.in1)()
  protected override val hOut  : OutLMain  = OutLMain(this, shape.out)

  protected def run(x: Long, n: Int): Long = {
    var _x = x
    var i = 0
    while (i < n) {
      hOut.next(_x)
      val _step = hStep.next()
      _x = inc(_x, _step)
      i += 1
    }
    _x
  }
}

final class SeqGenLogicD(name: String, shape: FanInShape3[Buf.D, Buf.D, Buf.L, Buf.D], layer: Layer)
                        (inc: (Double, Double) => Double)
                        (implicit a: Allocator)
  extends SeqGenLogic[Double](name, shape, layer)  {

  protected override val hStep : InDAux    = InDAux  (this, shape.in1)()
  protected override val hOut  : OutDMain  = OutDMain(this, shape.out)

  protected def run(x: Double, n: Int): Double = {
    var _x = x
    var i = 0
    while (i < n) {
      hOut.next(_x)
      val _step = hStep.next()
      _x = inc(_x, _step)
      i += 1
    }
    _x
  }
}

abstract class SeqGenLogic[A](name: String, shape: FanInShape3[Buf.E[A], Buf.E[A], Buf.L, Buf.E[A]],
                                                    layer: Layer)
                                                   (implicit a: Allocator, val tpe: DataType[A])
  extends Handlers(name, layer, shape)  {

  // ---- impl ----

  private[this] val hStart: InAux   [A]  = InAux   [A](this, shape.in0)()
  private[this] val hLen  : InLAux       = InLAux     (this, shape.in2)(math.max(0L, _))

  protected val hOut  : OutMain [A]
  protected val hStep : InAux   [A]

  private[this] var init = true

  private[this] var x     : A  = _
  private[this] var remain: Long    = _

  final protected def onDone(inlet: Inlet[_]): Unit = assert(false)

  protected def run(x: A, n: Int): A

  @tailrec
  final protected def process(): Unit = {
    while (init) {
      if (hLen.isConstant) {
        if (hOut.flush()) completeStage()
        return
      }
      if (!(hLen.hasNext && hStart.hasNext)) return
      remain  = hLen  .next()
      x       = hStart.next()
      if (remain > 0) init = false
    }

    val rem = min(min(hOut.available, hStep.available), remain).toInt
    if (rem == 0) return

    x = run(x, rem)
    remain -= rem

    if (remain == 0L) init = true
    process() // because we advance `hStep` we should always loop
  }
}

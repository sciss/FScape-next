/*
 *  MatrixInMatrix.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object MatrixInMatrix extends ProductReader[MatrixInMatrix[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): MatrixInMatrix[_] = {
    require (arity == 8 && adj == 0)
    val _in           = in.readGE[Any]()
    val _rowsOuter    = in.readGE_I()
    val _columnsOuter = in.readGE_I()
    val _rowsInner    = in.readGE_I()
    val _columnsInner = in.readGE_I()
    val _rowStep      = in.readGE_I()
    val _columnStep   = in.readGE_I()
    val _mode         = in.readGE_I()
    new MatrixInMatrix(_in, _rowsOuter, _columnsOuter, _rowsInner, _columnsInner, _rowStep, _columnStep, _mode)
  }
}
/** Note: `mode` is not yet implemented. */
final case class MatrixInMatrix[A](in          : GE[A],
                                   rowsOuter   : GE.I,
                                   columnsOuter: GE.I,
                                   rowsInner   : GE.I,
                                   columnsInner: GE.I,
                                   rowStep     : GE.I = 1,
                                   columnStep  : GE.I = 1,
                                   mode        : GE.I = 0,
                               )
  extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, rowsOuter.expand, columnsOuter.expand, rowsInner.expand, columnsInner.expand,
      rowStep.expand, columnStep.expand, mode.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, rowsOuter, columnsOuter, rowsInner, columnsInner, rowStep, columnStep, mode) = args: @unchecked
    import in.tpe
    val out = stream.MatrixInMatrix(
      in            = in          .toElem,
      rowsOuter     = rowsOuter   .toInt,
      columnsOuter  = columnsOuter.toInt,
      rowsInner     = rowsInner   .toInt,
      columnsInner  = columnsInner.toInt,
      rowStep       = rowStep     .toInt,
      columnStep    = columnStep  .toInt,
      mode          = mode        .toInt,
    )
    tpe.mkStreamOut(out)
  }
}
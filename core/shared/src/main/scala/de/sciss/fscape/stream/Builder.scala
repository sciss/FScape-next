/*
 *  Builder.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.NotUsed
import akka.stream.scaladsl.GraphDSL
import akka.stream.{Graph, Inlet, Outlet, Shape}

import scala.collection.immutable

object Builder {
  def apply()(implicit dsl: GraphDSL.Builder[NotUsed], ctrl: Control): Settable = new Impl(ctrl)

  trait Settable extends Builder {
    def layer_=(value: Int): Unit
    def allocator_=(value: Allocator): Unit
  }

  private final class Impl(val control: Control)(implicit b: GraphDSL.Builder[NotUsed]) extends Settable {
    var layer = 0
    var allocator: Allocator = Allocator(
      control     = control,
      blockSize   = control.config.blockSize,
      randomSeed  = control.config.seed.getOrElse(System.currentTimeMillis())
    )

    private var map = immutable.Map.empty[AnyRef, Any]

    override def put[U](ref: AnyRef, value: U): Unit =
      map += ref -> value

    override def get[U](ref: AnyRef): Option[U] =
      map.get(ref).asInstanceOf[Option[U]]

    def add[S <: Shape](graph: Graph[S, _]): S = b.add(graph)

    def dsl: GraphDSL.Builder[NotUsed] = b

    def connect[A](out: Outlet[A], in: Inlet[A]): Unit = {
      import GraphDSL.Implicits._
      out ~> in
    }

    def map[A, B](out: Outlet[A], name: String)(fun: A => B): Outlet[B] = {
      Map(out, name)(fun)(this)
//      import GraphDSL.Implicits._
//      out.map(fun).outlet
    }
  }
}
trait Builder {
  def control   : Control
  def allocator : Allocator

  def layer: Layer

  def dsl: GraphDSL.Builder[NotUsed]

  def add[S <: Shape](graph: Graph[S, _]): S

  /** Leaves a stream building result (such as a `Stage`) for future reference */
  def put[U](ref: AnyRef, value: U): Unit = throw new NotImplementedError()

  /** Obtains a stream building result (such as a `Stage`) by reference */
  def get[U](ref: AnyRef): Option[U] = throw new NotImplementedError()

  def map[A, B](out: Outlet[A], name: String)(fun: A => B): Outlet[B]

  def connect[A](out: Outlet[A], in: Inlet[A]): Unit
}
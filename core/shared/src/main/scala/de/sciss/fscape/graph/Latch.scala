/*
 *  Latch.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Latch extends ProductReader[Latch[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Latch[_] = {
    require (arity == 2 && adj == 0)
    val _in   = in.readGE[Any]()
    val _gate = in.readGE_B()
    new Latch(_in, _gate)
  }
}
/** A sample-and-hold UGen. It passes through its input while the gate is open,
  * and outputs the last held value while the gate is closed.
  *
  * @see [[Gate]]
  */
final case class Latch[A](in: GE[A], gate: GE.B) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, gate) = args: @unchecked
    import in.tpe
    val out = stream.Latch[in.A](in = in.toElem, gate = gate.toInt)
    tpe.mkStreamOut(out)
  }
}
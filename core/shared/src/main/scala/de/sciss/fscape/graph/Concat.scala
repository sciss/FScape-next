/*
 *  Concat.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Concat extends ProductReader[Concat[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Concat[_] = {
    require (arity == 2 && adj == 0)
    val _a = in.readGE[Any]()
    val _b = in.readGE[Any]()
    new Concat(_a, _b)
  }
}
/** Concatenates two signals. */
final case class Concat[A](a: GE[A], b: GE[A]) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(a.expand, b.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit builder: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit builder: stream.Builder): StreamOut = {
    val Vec(a, b) = args: @unchecked
    if (a.isDouble || b.isDouble) {
      stream.Concat[Double](a = a.toDouble, b = b.toDouble)
    } else if (a.isLong || b.isLong) {
      stream.Concat[Long  ](a = a.toLong  , b = b.toLong  )
    } else if (a.isInt) {
      stream.Concat[Int   ](a = a.toInt   , b = b.toInt   )
    } else {
      val aC = a.cast[A]
      val bC = b.cast[A]
      import aC.tpe
      val out = stream.Concat[A](a = aC.toElem, b = bC.toElem)
      tpe.mkStreamOut(out)
    }
  }
}
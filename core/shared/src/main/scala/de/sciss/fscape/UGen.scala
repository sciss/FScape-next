/*
 *  UGen.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import java.net.URI

import de.sciss.fscape.graph.impl.{MultiOutImpl, SingleOutImpl, ZeroOutImpl}
import de.sciss.fscape.graph.{ImageFile, UGenInGroup, UGenOutProxy, UGenProxy}
import de.sciss.serial.{DataOutput, Writable}

import scala.annotation.switch
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.language.implicitConversions

/** A UGen during graph building process is a more
  * rich thing than `RawUGen`: it implements equality
  * based on `isIndividual` status and may be omitted
  * from the final graph based on `hasSideEffect` status.
  */
sealed trait UGen extends Product {
// !!! WE CURRENTLY DISABLE STRUCTURAL EQUALITY
//  // initialize this first, so that debug printing in `addUGen` can use the hash code
//  override val hashCode: Int = if (isIndividual) super.hashCode() else scala.runtime.ScalaRunTime._hashCode(this)

  override def toString: String = inputs.mkString(s"$name(", ", ", ")")

  def name      : String

  def inputs    : Vec[UGenIn[_]]

  /** Additional UGen arguments that are not of type `UGenIn`.
    * These are included to achieve correct equality
    * (also as we do not transcode unary/binary operator ids
    * into special indices)
    */
  def adjuncts: List[UGen.Adjunct]

  def numInputs : Int = inputs.size
  def numOutputs: Int

  // the full UGen spec:
  // name, inputs, adjuncts
  override final def productPrefix: String = "UGen"
  final def productArity: Int = 3

  final def productElement(n: Int): Any = (n: @switch) match {
    case 0 => name
    case 1 => inputs
    case 2 => adjuncts
    case _ => throw new java.lang.IndexOutOfBoundsException(n.toString)
  }

  final def canEqual(x: Any): Boolean = x.isInstanceOf[UGen]

// !!! WE CURRENTLY DISABLE STRUCTURAL EQUALITY
//  override def equals(x: Any): Boolean = (this eq x.asInstanceOf[AnyRef]) || (!isIndividual && (x match {
//    case u: UGen =>
//      u.name == name && u.inputs == inputs && u.adjuncts == adjuncts && u.canEqual(this)
//    case _ => false
//  }))

  def isIndividual : Boolean
  def hasSideEffect: Boolean
}

object UGen /*extends UGenPlatform*/ {
  object SingleOut {
    def apply[A](source: UGenSource.SingleOut[A], inputs: Vec[UGenIn[_]], adjuncts: List[Adjunct] = Nil,
              isIndividual: Boolean = false,
              hasSideEffect: Boolean = false)(implicit b: UGenGraph.Builder): SingleOut[A] = {
      val res = new SingleOutImpl[A](source = source, inputs = inputs, adjuncts = adjuncts,
        isIndividual = isIndividual, hasSideEffect = hasSideEffect)
      b.addUGen(res)
      res
    }
  }
  /** A SingleOutUGen is a UGen which has exactly one output, and
    * hence can directly function as input to another UGen without expansion.
    */
  trait SingleOut[A] extends UGenProxy[A] with UGen {
    final def numOutputs  = 1
    final def outputIndex = 0
    final def ugen: UGen  = this

    def source: UGenSource.SingleOut[A]
  }

  object ZeroOut {
    def apply(source: UGenSource.ZeroOut, inputs: Vec[UGenIn[_]], adjuncts: List[Adjunct] = Nil,
              isIndividual: Boolean = false)
             (implicit b: UGenGraph.Builder): ZeroOut = {
      val res = new ZeroOutImpl(source = source, inputs = inputs, adjuncts = adjuncts, isIndividual = isIndividual)
      b.addUGen(res)
      res
    }
  }
  trait ZeroOut extends UGen {
    final def numOutputs    = 0
    final def hasSideEffect = true  // implied by having no outputs

    def source: UGenSource.ZeroOut
  }

  object MultiOut {
    def apply[A](source: UGenSource.MultiOut[A], inputs: Vec[UGenIn[_]], numOutputs: Int,
              adjuncts: List[Adjunct] = Nil, isIndividual: Boolean = false, hasSideEffect: Boolean = false)
             (implicit b: UGenGraph.Builder): MultiOut[A] = {
      val res = new MultiOutImpl[A](source = source, numOutputs = numOutputs, inputs = inputs,
        adjuncts = adjuncts, isIndividual = isIndividual,
        hasSideEffect = hasSideEffect)
      b.addUGen(res)
      res
    }
  }
  /** A class for UGens with multiple outputs. */
  trait MultiOut[A] extends UGenInGroup[A] with UGen {

    final def unwrap(i: Int): UGenInLike[A] = UGenOutProxy(this, i % numOutputs)

    def outputs: Vec[UGenIn[A]] =
      Vector.tabulate(numOutputs)(ch => UGenOutProxy(this, ch))

    def source: UGenSource.MultiOut[A]

    private[fscape] final def unbubble: UGenInLike[A] = if (numOutputs == 1) outputs(0) else this
  }

  object Adjunct /*extends AdjunctPlatform*/ {
    final case class FileOut(peer: URI) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(0)
        out.writeUTF(peer.toString)
      }
    }

    final case class FileIn(peer: URI) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(1)
        out.writeUTF( peer.toString)
        // SJSXXX
//        out.writeLong(peer.lastModified())
//        out.writeLong(peer.length())
      }
    }

    final case class Int(peer: scala.Int) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(2)
        out.writeInt(peer)
      }
    }

    final case class String(peer: scala.Predef.String) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(3)
        out.writeUTF(peer)
      }
    }

    final case class Long(peer: scala.Long) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(4)
        out.writeLong(peer)
      }
    }

    final case class Double(peer: scala.Double) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(5)
        out.writeDouble(peer)
      }
    }

    final case class AudioFileSpec(peer: de.sciss.audiofile.AudioFileSpec) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(10)
        de.sciss.audiofile.AudioFileSpec.format.write(peer, out)
      }
    }

    final case class ImageFileSpec(peer: ImageFile.Spec) extends Adjunct {
      def write(out: DataOutput): Unit = {
        out.writeByte(11)
        ImageFile.Spec.format.write(peer, out)
      }
    }
  }
  /** This is used for calculating caching values, but never de-serialized */
  trait Adjunct extends Writable
}

object UGenInLike {
  implicit def expand[A](ge: GE[A])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    ge.expand
}
sealed trait UGenInLike[/*+*/A] extends GE[A] {
  private[fscape] def outputs: Vec[UGenInLike[A]]
  private[fscape] def unbubble: UGenInLike[A]

  /** Returns the UGenInLike element of index i
    * regarding the ungrouped representation. Note
    * that for efficiency reasons this method will
    * automatically wrap the index around numElements!
    */
  private[fscape] def unwrap(i: Int): UGenInLike[A]
  private[fscape] def flatOutputs: Vec[UGenIn[A]]

  // ---- GE ----
  final private[fscape] def expand(implicit b: UGenGraph.Builder): UGenInLike[A] = this
}

/** An element that can be used as an input to a UGen.
  * This is after multi-channel-expansion, hence implementing
  * classes are `UGenProxy` (`UGenOutProxy` or `UGen.SingleOut`) and `Constant`.
  */
sealed trait UGenIn[/*+*/A] extends UGenInLike[A] {
  private[fscape] def outputs: Vec[UGenIn[A]] = Vector(this)
  private[fscape] final def unwrap(i: Int): UGenInLike[A] = this

  // don't bother about the index
  private[fscape] final def flatOutputs: Vec[UGenIn[A]] = Vector(this)
  private[fscape] final def unbubble   : UGenInLike[A]  = this
}

package graph {

  import akka.stream.Outlet
  import de.sciss.fscape.stream.{BufD, BufI, BufL, Buf, Builder, OutD, OutI, OutL, StreamIn}

  object UGenInGroup {
    private final val emptyVal = Apply[Any](Vector.empty)
    def empty[A]: UGenInGroup[A] = emptyVal.asInstanceOf[UGenInGroup[A]]
    def apply[A](xs: Vec[UGenInLike[A]]): UGenInGroup[A] = Apply[A](xs)

    private final case class Apply[A](outputs: Vec[UGenInLike[A]]) extends UGenInGroup[A] {
      override def productPrefix = "UGenInGroup"

      private[fscape] def numOutputs: Int = outputs.size
      private[fscape] def unwrap(i: Int): UGenInLike[A] = outputs(i % outputs.size)
      private[fscape] def unbubble: UGenInLike[A] = this

      override def toString: String = outputs.mkString("UGenInGroup(", ",", ")")
    }
  }
  sealed trait UGenInGroup[/*+*/A] extends UGenInLike[A] {
    private[fscape] def outputs: Vec[UGenInLike[A]]
    private[fscape] def numOutputs: Int
    private[fscape] final def flatOutputs: Vec[UGenIn[A]] =
      outputs.flatMap(_.flatOutputs)
  }

  sealed trait UGenProxy[A] extends UGenIn[A] {
    def ugen: UGen
    def outputIndex: Int
  }

  object Const {
    def unapply(c: Const[_]): Option[Double] = Some(c.doubleValue)

    private[fscape] def apply[A](value: A): Const[A] = value match {
      case i: Int     => ConstI(i).asInstanceOf[Const[A]]
      case n: Long    => ConstL(n).asInstanceOf[Const[A]]
      case d: Double  => ConstD(d).asInstanceOf[Const[A]]
      case b: Boolean => ConstB(b).asInstanceOf[Const[A]]
      case _          => Impl[A](value)
    }

    private final case class Impl[A1](value: A1) extends Const[A1] {
      private def unsupported(name: String): Nothing =
        throw new UnsupportedOperationException(s"$name of $value")

      override def doubleValue  : Double  = unsupported("doubleValue")
      override def intValue     : Int     = unsupported("intValue")
      override def longValue    : Long    = unsupported("longValue")
      override def booleanValue : Boolean = unsupported("booleanValue")

      override type A   = A1
      override type Buf = Buf.E[A]

      override def isInt    : Boolean = false
      override def isLong   : Boolean = false
      override def isDouble : Boolean = false
      override def isBoolean: Boolean = false

      override def toAny(implicit b: Builder): Outlet[Buf] = unsupported("toAny")

      override def toElem(implicit b: Builder): Outlet[Buf] = unsupported("toElem")

      override implicit def tpe: DataType[A] = unsupported("tpe")
    }
  }
  /** A scalar constant used as an input to a UGen. */
  sealed trait Const[A1] extends UGenIn[A1] with StreamIn {
    def doubleValue : Double
    def intValue    : Int
    def longValue   : Long
    def booleanValue: Boolean

    def value: A1

//    def toDouble(implicit b: stream.Builder): OutD = b.add(scaladsl.Source.single(BufD(doubleValue))).out
//    def toInt   (implicit b: stream.Builder): OutI = b.add(scaladsl.Source.single(BufI(intValue   ))).out
//    def toLong  (implicit b: stream.Builder): OutL = b.add(scaladsl.Source.single(BufL(longValue  ))).out

    def toDouble(implicit b: stream.Builder): OutD = stream.Constant[Double](BufD(doubleValue))
    def toInt   (implicit b: stream.Builder): OutI = stream.Constant[Int   ](BufI(intValue   ))
    def toLong  (implicit b: stream.Builder): OutL = stream.Constant[Long  ](BufL(longValue  ))
  }
  object ConstI {
    final val C0  = new ConstI(0)
    final val C1  = new ConstI(1)
    final val Cm1 = new ConstI(-1)
  }
  final case class ConstI(value: Int) extends Const[Int] with StreamIn.IntLike {
    override def doubleValue  : Double  = value.toDouble
    override def intValue     : Int     = value
    override def longValue    : Long    = value.toLong
    override def booleanValue : Boolean = value > 0

    override def toString: String = value.toString
  }
  object ConstD {
    final val C0  = new ConstD(0)
    final val C1  = new ConstD(1)
    final val Cm1 = new ConstD(-1)
  }
  final case class ConstD(value: Double) extends Const[Double] with StreamIn.DoubleLike {
    def doubleValue: Double = value

    def intValue: Int = {
      if (value.isNaN) throw new ArithmeticException("NaN cannot be translated to Int")
      val r = math.rint(value)
      if (r < Int.MinValue || r > Int.MaxValue)
        throw new ArithmeticException(s"Double $value exceeds Int range")
      r.toInt
    }

    def longValue: Long = {
      if (value.isNaN) throw new ArithmeticException("NaN cannot be translated to Long")
      val r = math.round(value)
      r
    }

    override def booleanValue: Boolean = {
      if (value.isNaN) throw new ArithmeticException("NaN cannot be translated to Boolean")
      value > 0.0
    }

    override def toString: String = value.toString
  }

  object ConstL {
    final val C0  = new ConstI(0)
    final val C1  = new ConstI(1)
    final val Cm1 = new ConstI(-1)
  }
  final case class ConstL(value: Long) extends Const[Long] with StreamIn.LongLike {
    override def doubleValue: Double = value.toDouble

    override def intValue: Int = {
      val res = value.toInt
      if (res != value) throw new ArithmeticException(s"Long $value exceeds Int range")
      res
    }

    override def longValue: Long = value

    override def booleanValue: Boolean = value > 0L

    override def toString: String = value.toString
  }

  object ConstB {
    final val Cfalse  = new ConstB(false)
    final val Ctrue   = new ConstB(true )
  }
  final case class ConstB(value: Boolean) extends Const[Boolean] with StreamIn /*.IntLike*/ {
    override def doubleValue  : Double  = if (value) 1.0 else 0.0
    override def intValue     : Int     = if (value) 1   else 0
    override def longValue    : Long    = if (value) 1L  else 0L
    override def booleanValue : Boolean = value

    /** N.B. ConstantB reports `true` for this */
    override def isInt    : Boolean = true  // !
    override def isLong   : Boolean = false
    override def isDouble : Boolean = false
    override def isBoolean: Boolean = true

    override def toString: String = value.toString

    override type A   = Int
    override type Buf = Buf.I

    override def toAny (implicit b: Builder): Outlet[Buf]  = toInt.as[Buf]
    override def toElem(implicit b: Builder): OutI             = toInt

    override implicit def tpe: DataType[Int] = DataType.int
  }
  /** A UGenOutProxy refers to a particular output of a multi-channel UGen.
    * A sequence of these form the representation of a multi-channel-expanded
    * UGen.
    */
  final case class UGenOutProxy[A](ugen: UGen.MultiOut[A], outputIndex: Int)
    extends UGenProxy[A] {

    override def toString: String =
      if (ugen.numOutputs == 1) ugen.toString else s"$ugen.\\($outputIndex)"
  }
}
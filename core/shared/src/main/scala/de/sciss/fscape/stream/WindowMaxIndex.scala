/*
 *  WindowMaxIndex.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2}
import de.sciss.fscape.stream.impl.logic.FilterWindowedInAOutB
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

object WindowMaxIndex {
  def apply[A](in: Out[A], size: OutI)(implicit b: Builder, tpe: DataType[A]): OutI = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in  , stage.in0)
    b.connect(size, stage.in1)
    stage.out
  }

  private final val name = "WindowMaxIndex"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.I, Buf.I]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In[A](s"${stage.name}.in"  ),
      in1 = InI  (s"${stage.name}.size"),
      out = OutI (s"${stage.name}.out" )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double ]], layer)(_ > _)
      } else if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int    ]], layer)(_ > _)
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape.asInstanceOf[Shp[Long   ]], layer)(_ > _)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer)(gt: (A, A) => Boolean)
                                               (implicit a: Allocator, protected val tpe: DataType[A])
    extends FilterWindowedInAOutB[A, Int, A, Shp[A]](name, layer, shape)(shape.in0, shape.out) {

    private[this] val hSize = Handlers.InIAux(this, shape.in1)(math.max(0 , _))

    private[this] var index   : Int = _
    private[this] var maxValue: A   = _

    protected def clearWindowTail(): Unit = ()

    protected def newWindowBuffer(n: Int): Array[A] = tpe.newArray(n)

    protected def tryObtainWinParams(): Boolean = {
      val ok = hSize.hasNext
      if (ok) {
        hSize.next()
        index = -1
      }
      ok
    }

    protected def winBufSize: Int = 0

    override protected def readWinSize  : Long = hSize.value
    override protected def writeWinSize : Long = 1

    protected def processWindow(): Unit = ()

    override protected def readIntoWindow(n: Int): Unit = {
      val in      = hIn.array
      val inOff   = hIn.offset
      var i       = inOff
      val stop    = i + n
      var _index  = index
      var _max    = maxValue
      val d       = readOff.toInt - inOff
      while (i < stop) {
        val v = in(i)
        if (_index < 0 || gt(v, _max)) {
          _max    = v
          _index  = i + d
        }
        i += 1
      }
      maxValue  = _max
      index     = _index
      hIn.advance(n)
    }

    override protected def writeFromWindow(n: Int): Unit = {
      assert (n == 1)
      hOut.next(index)
    }
  }
}
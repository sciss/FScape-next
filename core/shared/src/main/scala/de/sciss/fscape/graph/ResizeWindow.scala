/*
 *  ResizeWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object ResizeWindow extends ProductReader[ResizeWindow[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ResizeWindow[_] = {
    require (arity == 4 && adj == 0)
    val _in     = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _start  = in.readGE_I()
    val _stop   = in.readGE_I()
    new ResizeWindow[Any](_in, _size, _start, _stop)
  }
}
/** A UGen that resizes the windowed input signal by trimming each
  * windows boundaries (if `start` is greater than zero
  * or `stop` is less than zero) or padding the boundaries
  * with zeroes (if `start` is less than zero or `stop` is
  * greater than zero). The output window size is thus
  * `size - start + stop`.
  *
  * @param in     the signal to window and resize
  * @param size   the input window size
  * @param start  the delta window size at the output window's beginning
  * @param stop   the delta window size at the output window's ending
  */
final case class ResizeWindow[A](in: GE[A], size: GE.I, start: GE.I = 0, stop: GE.I = 0)
  extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, start.expand, stop.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, start, stop) = args: @unchecked
    import in.tpe
    val out = stream.ResizeWindow[in.A](in = in.toElem, size = size.toInt, start = start.toInt, stop = stop.toInt)
    tpe.mkStreamOut(out)
  }
}
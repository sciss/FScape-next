/*
 *  RunningWindowMin.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape3}
import de.sciss.fscape.stream.impl.{NodeImpl, RunningWindowValueLogic, StageImpl}

import scala.math.min

object RunningWindowMin {
  def apply[A](in: Out[A], size: OutI, gate: OutI)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in  , stage.in0)
    b.connect(size, stage.in1)
    b.connect(gate, stage.in2)
    stage.out
  }

  private final val name = "RunningWindowMin"

  private type Shp[A] = FanInShape3[Buf.E[A], Buf.I, Buf.I, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape3(
      in0 = In [A] (s"${stage.name}.in"  ),
      in1 = InI    (s"${stage.name}.size"),
      in2 = InI    (s"${stage.name}.gate"),
      out = Out[A] (s"${stage.name}.out" )
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: RunningWindowValueLogic[_] = if (tpe.isDouble) {
        new RunningWindowValueLogic[Double](stage.name, layer, shape.asInstanceOf[Shp[Double]])(min)
      } else if (tpe.isInt) {
        new RunningWindowValueLogic[Int   ](stage.name, layer, shape.asInstanceOf[Shp[Int]])(min)
      } else {
        assert (tpe.isLong)
        new RunningWindowValueLogic[Long  ](stage.name, layer, shape.asInstanceOf[Shp[Long]])(min)
      }
      res.asInstanceOf[RunningWindowValueLogic[A]]
    }
  }
}
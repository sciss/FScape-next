/*
 *  NumChannels.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.stream.{Builder, StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object NumChannels extends ProductReader[NumChannels[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): NumChannels[_] = {
    require (arity == 1 && adj == 0)
    val _in = in.readGE[Any]()
    new NumChannels(_in)
  }
}
/** A graph element that produces an integer with number-of-channels of the input element.
  */
final case class NumChannels[A](in: GE[A]) extends UGenSource.SingleOut[Int]  {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    UGenSource.unwrap(this, in.expand.outputs)

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Int] =
    ConstI(args.size)

  // XXX TODO --- is this ever called?
  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: Builder): StreamOut = {
    val Vec(sz) = args: @unchecked
    sz.toInt
  }
}

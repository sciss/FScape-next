/*
 *  WindowApply.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{Builder, Out, OutI, StreamIn, StreamInElem, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object WindowApply extends ProductReader[WindowApply[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): WindowApply[_] = {
    require (arity == 4 && adj == 0)
    val _in     = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _index  = in.readGE_I()
    val _mode   = in.readGE_I()
    new WindowApply[Any](_in, _size, _index, _mode)
  }
}
/** A UGen that extracts for each input window the element at a given index.
  *
  * For example, the first element per window can be extracted with `index = 0`,
  * and the last element per window can be extracted with `index = -1, mode = 1` (wrap).
  *
  * If the input `in` terminates before a window of `size` is full, it will be padded
  * with zeroes.
  *
  * @param in     the window'ed signal to index
  * @param size   the window size.
  * @param index  the zero-based index into each window. One value per window is polled.
  * @param mode   wrap mode. `0` clips indices, `1` wraps them around, `2` folds them, `3` outputs
  *               zeroes when index is out of bounds.
  */
final case class WindowApply[A](in    : GE[A],
                                size  : GE.I,
                                index : GE.I = 0,
                                mode  : GE.I = 0
                               )
  extends UGenSource.SingleOut[A] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, index.expand, mode.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, index, mode) = args: @unchecked
    mkStream[in.A](in = in, size = size.toInt, index = index.toInt, mode = mode.toInt)
  }

  private def mkStream[A1](in: StreamInElem[A1], size: OutI, index: OutI, mode: OutI)
                          (implicit b: Builder): StreamOut = {
    import in.{tpe => inTpe}
    val out: Out[in.A] = stream.WindowApply[A1](in = in.toElem, size = size, index = index, mode = mode)
    inTpe.mkStreamOut(out)
  }
}

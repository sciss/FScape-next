/*
 *  Constant.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.stage.{GraphStage, GraphStageLogic, OutHandler}
import akka.stream.{Attributes, SourceShape}
import de.sciss.fscape.stream

/** Similar to `GraphStages.SingleSource` but with proper `toString` */
object Constant {
  def apply[A](elem: Buf.E[A])(implicit b: stream.Builder): Out[A] = {
    require (elem.size == 1)
    val stage0  = new Stage[A](elem)
    val stage   = b.add(stage0)
    stage.out
  }

  private type Shp[A] = SourceShape[Buf.E[A]]

  private final class Stage[A](elem: Buf.E[A]) extends GraphStage[Shp[A]] {
    private val name: String = elem.buf(0).toString

    override def toString: String = name

    val shape: Shape = SourceShape(
      Out[A](s"$name.out")
    )

    override def initialAttributes: Attributes = Attributes.name(toString)

    def createLogic(attr: Attributes): GraphStageLogic = new Logic[A](shape, name, elem)
  }

  private final class Logic[A](shape: Shp[A], name: String, elem: Buf.E[A])
    extends GraphStageLogic(shape) with OutHandler {

    override def toString: String = name

    def onPull(): Unit = {
      push(shape.out, elem)
      completeStage()
    }
    setHandler(shape.out, this)
  }
}

/*
 *  Hash.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Hash extends ProductReader[Hash[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Hash[_] = {
    require (arity == 1 && adj == 0)
    val _in = in.readGE[Any]()
    new Hash[Any](_in)
  }
}
/** A UGen that produces a continuous hash value, useful for comparisons and debugging applications.
  * The hash is produced using the 64-bit Murmur 2 algorithm, however the continuous output is
  * given without "finalising"  the hash, and it is not initialized with the input's length
  * (as it is unknown at that stage).
  *
  * '''Not yet implemented:''' Only after the input terminates, one additional sample of
  * finalised hash is given (you can use `.last` to only retain the final hash).
  *
  * @param in   the signal to hash.
  */
final case class Hash[A](in: GE[A]) extends UGenSource.SingleOut[Long] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked

    if (in.isLong) {
      stream.Hash.fromLong(in.toLong)
    } else if (in.isDouble) {
      stream.Hash.fromDouble(in.toDouble)
    } else if (in.isInt) {
      stream.Hash.fromInt(in.toInt)
    } else {
      import in.tpe
      stream.Hash.from(in.toElem)
    }
  }
}

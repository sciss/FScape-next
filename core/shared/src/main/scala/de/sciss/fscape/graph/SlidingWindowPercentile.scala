/*
 *  SlidingWindowPercentile.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object SlidingWindowPercentile extends ProductReader[SlidingWindowPercentile[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SlidingWindowPercentile[_] = {
    require (arity == 5)
    val _in         = in.readGE[Any]()
    val _winSize    = in.readGE_I()
    val _medianLen  = in.readGE_I()
    val _frac       = in.readGE_D()
    val _interp     = in.readGE_I()
    val _num        = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new SlidingWindowPercentile[Any](_in, _winSize, _medianLen, _frac, _interp)(_num)
  }
}
/** A UGen that reports a percentile of a sliding window across every cell
  * of a window'ed input (such as an image).
  *
  * The UGen starts outputting values immediately, even if the `medianLen`
  * is not yet reached. This is because `medianLen` can be modulated (per input window).
  * If one wants to discard the initial values, use a `drop`, for example
  * for `medianLen/2 * winSize` frames.
  *
  * Note that for an even median length and no interpolation, the reported median
  * may be either the value at index `medianLen/2` or `medianLen/2 + 1` in the sorted window.
  *
  * All arguments but `in` are polled per input window. Changing the `frac` value
  * may cause an internal table rebuild and can thus be expensive.
  *
  * @param in         the window'ed input to analyze
  * @param winSize    the size of the input windows
  * @param medianLen  the length of the sliding median window (the filter window
  *                   applied to every cell of the input window)
  * @param frac       the percentile from zero to one. The default of 0.5 produces the median.
  * @param interp     if zero (default), uses nearest-rank, otherwise uses linear interpolation.
  *                   '''Note:''' currently not implemented, must be zero
  */
final case class SlidingWindowPercentile[A](in        : GE[A],
                                            winSize   : GE.I,
                                            medianLen : GE.I = 3,
                                            frac      : GE.D = 0.5,
                                            interp    : GE.I = 0
                                           )(implicit num: Num[A]) // use `Num` rather than `Ord` because we might implement `interp`
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, winSize.expand, medianLen.expand, frac.expand, interp.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, winSize, medianLen, frac, interp) = args: @unchecked
    import in.tpe
    val out = stream.SlidingWindowPercentile[in.A](
      in        = in        .toElem,
      winSize   = winSize   .toInt,
      medianLen = medianLen .toInt,
      frac      = frac      .toDouble,
      interp    = interp    .toInt,
    )
    tpe.mkStreamOut(out)
  }
}
/*
 *  GE.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import de.sciss.fscape.graph.impl.GESeq
import de.sciss.fscape.graph.{Const, ConstB, ConstD, ConstI, ConstL}

import scala.language.implicitConversions

object GE extends GEPlatform {
  trait Lazy[/*+*/A] extends Lazy.Expander[UGenInLike[A]] with GE[A]

  implicit def intConst     (x: Int     ): Const[Int     ] = new ConstI(x)
  implicit def doubleConst  (x: Double  ): Const[Double  ] = new ConstD(x)
  implicit def longConst    (x: Long    ): Const[Long    ] = new ConstL(x)
  implicit def booleanConst (x: Boolean ): Const[Boolean ] = new ConstB(x)

  implicit def fromSeq[A](xs: scala.Seq[GE[A]]): GE[A] = xs match {
    case scala.Seq(x) => x
    case _            => GESeq(xs.toIndexedSeq)
  }

  type D = GE[Double  ]
  type I = GE[Int     ]
  type L = GE[Long    ]
  type B = GE[Boolean ]
}
/** The main trait used in an FScape graph, a graph element, abbreviated as `GE`.
  *
  * A lot of operations on `GE` are defined separately in `GEOps1` and `GEOps2`
  *
  * @see [[GEOps1]]
  * @see [[GEOps2]]
  */
trait GE[A] extends Product {
  private[fscape] def expand(implicit b: UGenGraph.Builder): UGenInLike[A]
}
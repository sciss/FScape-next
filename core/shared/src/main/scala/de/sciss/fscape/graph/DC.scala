/*
 *  DC.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object DC extends ProductReader[DC[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DC[_] = {
    require (arity == 1 && adj == 0)
    val _in = in.readGE[Any]()
    new DC(_in)
  }
}
/** Creates a constant infinite signal. */
final case class DC[A](in: GE[A]) extends UGenSource.SingleOut[A] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    import in.tpe
    val out = stream.DC[in.A](in = in.toElem)
    tpe.mkStreamOut(out)
  }
}
/*
 *  GimpSlur.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object GimpSlur extends ProductReader[GimpSlur] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): GimpSlur = {
    require (arity == 8 && adj == 0)
    val _in           = in.readGE_D()
    val _width        = in.readGE_I()
    val _height       = in.readGE_I()
    val _kernel       = in.readGE_D()
    val _kernelWidth  = in.readGE_I()
    val _kernelHeight = in.readGE_I()
    val _repeat       = in.readGE_I()
    val _wrap         = in.readGE_I()
    new GimpSlur(_in, _width, _height, _kernel, _kernelWidth, _kernelHeight, _repeat, _wrap)
  }
}
/** A UGen similar to GIMP's Slur image filter. Instead of a hard-coded kernel,
  * the probability table must be provided as a separate input.
  * The kernel width and height should be odd, so that the kernel is considered to be
  * symmetric around each input image's pixel. If they are odd, the centre corresponds to
  * integer divisions `kernelWidth/2` and `kernelHeight/2`.
  *
  * @param in             image input
  * @param width          image width
  * @param height         image height
  * @param kernel         normalized and integrated probability table.
  *                       Like the image, the cells are read horizontally first,
  *                       every `widthKernel` cell begins a new cell. The cell probabilities
  *                       must have been integrated from first to last, and must be normalized
  *                       so that the last cell value equals one. A new kernel signal is read
  *                       once per input image. (If the signal ends, the previous kernel will
  *                       be used again).
  * @param kernelWidth    width of the kernel signal. Read once per input image.
  * @param kernelHeight   height of the kernel signal. Read once per input image.
  * @param repeat         number of recursive application of the displacement per image. Read once per input image.
  * @param wrap           if great than zero, wraps pixels around the image bounds, otherwise clips.
  */
final case class GimpSlur(in          : GE.D,
                          width       : GE.I,
                          height      : GE.I,
                          kernel      : GE.D,
                          kernelWidth : GE.I,
                          kernelHeight: GE.I,
                          repeat      : GE.I = 1,
                          wrap        : GE.I = 0,
                         )
  extends UGenSource.SingleOut[Double] {

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, width.expand, height.expand, kernel.expand,
      kernelWidth.expand, kernelHeight.expand, repeat.expand, wrap.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, width, height, kernel, kernelWidth, kernelHeight, repeat, wrap) = args: @unchecked
    stream.GimpSlur(
      in            = in          .toDouble,
      width         = width       .toInt,
      height        = height      .toInt,
      kernel        = kernel      .toDouble,
      kernelWidth   = kernelWidth .toInt,
      kernelHeight  = kernelHeight.toInt,
      repeat        = repeat      .toInt,
      wrap          = wrap        .toInt,
    )
  }
}
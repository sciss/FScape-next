/*
 *  SinOsc.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object SinOsc extends ProductReader[SinOsc] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SinOsc = {
    require (arity == 2 && adj == 0)
    val _freqN  = in.readGE_D()
    val _phase  = in.readGE_D()
    new SinOsc(_freqN, _phase)
  }
}
/** Sine oscillator.
  * Note that the frequency is not in Hertz but
  * the normalized frequency
  * as we do not maintained one global sample rate.
  * For a frequency in Hertz, `freqN` would be
  * that frequency divided by the assumed sample rate.
  *
  * @param freqN  normalized frequency (f/sr).
  * @param phase  phase offset in radians
  */
final case class SinOsc(freqN: GE.D, phase: GE.D = 0.0) extends UGenSource.SingleOut[Double] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(freqN.expand, phase.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(freqN, phase) = args: @unchecked
    stream.SinOsc(freqN = freqN.toDouble, phase = phase.toDouble)
  }
}
/*
 *  Done.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.stage.{InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape}
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object Done {
  def apply[A](in: Out[A])(implicit b: Builder): OutI = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "Done"

  private type Shp[A] = FlowShape[Buf.E[A], Buf.I]

  private final class Stage[A](layer: Layer)(implicit a: Allocator) extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FlowShape(
      in  = In[A](s"${stage.name}.in" ),
      out = OutI (s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic(shape, layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator)
    extends NodeImpl(name, layer, shape) with InHandler with OutHandler {

    setHandler(shape.in , this)
    setHandler(shape.out, this)

    override def onUpstreamFinish(): Unit = {
      if (/* !isAvailable(shape.in) && */ isAvailable(shape.out)) flush()
    }

    def onPull(): Unit = {
      if (isClosed(shape.in) /* && !isAvailable(shape.in) */) flush()
    }

//    private var NUM = 0L

    def onPush(): Unit = {
      val buf = grab(shape.in)
//      NUM += buf.size
      buf.release()
      pull(shape.in)
    }

    private def flush(): Unit = {
      val buf    = allocator.borrowBufI()
      buf.buf(0) = 1
      buf.size   = 1
      push(shape.out, buf)
//      println(s"NUM = $NUM")
      logStream.info(s"completeStage() $this")
      completeStage()
    }
  }
}
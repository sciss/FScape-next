/*
 *  SortWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{Builder, Out, OutI, StreamIn, StreamInElem, StreamOut}
import de.sciss.lucre.Adjunct.Ord
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object SortWindow extends ProductReader[SortWindow[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SortWindow[_, _] = {
    require (arity == 3)
    val _keys   = in.readGE[Any]()
    val _values = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _ord    = if (adj == 0) LegacyAdjunct.Ord else {
      require (adj == 1)
      in.readAdjunct[Ord[Any]]()
    }
    new SortWindow[Any, Any](_keys, _values, _size)(_ord)
  }
}
/** A UGen that sorts the input data window by window.
  *
  * @param keys   the sorting keys; output will be in ascending order
  * @param values the values corresponding with the keys and eventually
  *               output by the UGen. It is well possible to use the
  *               same signal both for `keys` and `values`.
  * @param size   the window size.
  */
final case class SortWindow[A, B](keys: GE[A], values: GE[B], size: GE.I)(implicit ord: Ord[A])
  extends UGenSource.SingleOut[B] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = ord :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[B] =
    unwrap(this, Vector(keys.expand, values.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[B] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(keys, values, size) = args: @unchecked
    mkStream[keys.A, values.A](keys = keys, values = values, size = size.toInt)  // IntelliJ doesn't get it
  }

  private def mkStream[A1, B1](keys  : StreamInElem[A1],
                               values: StreamInElem[B1], size: OutI)
                              (implicit b: Builder): StreamOut = {
    implicit val kTpe: DataType[A1] = keys  .tpe
    implicit val vTpe: DataType[B1] = values.tpe
    val out: Out[B1] = stream.SortWindow[A1, B1](keys = keys.toElem, values = values.toElem, size = size)
    vTpe.mkStreamOut(out)
  }
}
/*
 *  TakeWhile.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.{Attributes, FanInShape2, Inlet}
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream.impl.Handlers._
import de.sciss.fscape.stream.impl.{Handlers, NodeImpl, StageImpl}

object TakeWhile {
  def apply[A](in: Out[A], p: OutI)(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in0)
    b.connect(p , stage.in1)
    stage.out
  }

  private final val name = "TakeWhile"

  private type Shp[A] = FanInShape2[Buf.E[A], Buf.I, Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FanInShape2(
      in0 = In [A](s"${stage.name}.in" ),
      in1 = InI   (s"${stage.name}.p"  ),
      out = Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic[A](shape, layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends Handlers(name, layer, shape) {

    private[this] val hIn   : InMain [A] = InMain [A](this, shape.in0)
    private[this] val hPred : InIAux     = InIAux    (this, shape.in1)()
    private[this] val hOut  : OutMain[A] = OutMain[A](this, shape.out)

    private[this] var gate = true

    protected def onDone(inlet: Inlet[_]): Unit = {
      assert (inlet == shape.in0)
      if (hOut.flush()) completeStage()
    }

    def process(): Unit = {
      logStream.debug(s"process() $this")

      if (gate) {
        while (gate) {
          val remIn   = math.min(hIn.available, hPred.available)
          val remOut  = hOut.available
          if (remIn == 0 || remOut == 0) return
          var count   = 0
          var _gate   = true
          while (_gate && count < remIn && count < remOut) {
            _gate = hPred.next() > 0
            if (_gate) {
              count += 1
            }
          }
          if (count > 0) {
            hIn.copyTo(hOut, count)
          }
          if (hIn.isDone) _gate = false
          gate = _gate
        }
        if (hOut.flush()) completeStage()
      }

      // XXX TODO --- should we advance hIn and hPred ? Probably not...
    }
  }
}
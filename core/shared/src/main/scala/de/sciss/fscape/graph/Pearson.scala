/*
 *  Pearson.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Pearson extends ProductReader[Pearson] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Pearson = {
    require (arity == 3 && adj == 0)
    val _x    = in.readGE_D()
    val _y    = in.readGE_D()
    val _size = in.readGE_I()
    new Pearson(_x, _y, _size)
  }
}
/** A UGen that calculates the Pearson product-moment correlation coefficient of
  * two input matrices.
  *
  * @param x      first matrix
  * @param y      second matrix
  * @param size   matrix or window size
  */
final case class Pearson(x: GE.D, y: GE.D, size: GE.I) extends UGenSource.SingleOut[Double] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(x.expand, y.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(x, y, size) = args: @unchecked
    stream.Pearson(x = x.toDouble, y = y.toDouble, size = size.toInt)
  }
}
/*
 *  RepeatWindow.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object RepeatWindow extends ProductReader[RepeatWindow[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RepeatWindow[_, _] = {
    require (arity == 3)
    type A = Any
    type L = Any
    val _in     = in.readGE[A]()
    val _size   = in.readGE_I ()
    val _num    = in.readGE[L]()
    val _numB   = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[L]]()
    }
    new RepeatWindow[A, L](_in, _size, _num)(_numB)
  }
}
/** A UGen that repeats the contents of input windows a number of times.
  *
  * @param in   the input signal to group into windows of size `size` and
  *             repeat `num` times. If the input ends before a full window
  *             is filled, the last window is padded with zeroes to obtain
  *             an input window if size `size`
  * @param size the window size. one value is polled per iteration
  *             (outputting `num` windows of size `size`). the minimum
  *             `size` is one (it will be clipped to this).
  * @param num  the number of repetitions of the input windows. one value is polled per iteration
  *             (outputting `num` windows of size `size`). the minimum
  *             `num` is one (it will be clipped to this).
  */
final case class RepeatWindow[A, L](in  : GE[A],
                                    size: GE.I = 1,
                                    num : GE[L] = 2
                                   )
                                   (implicit numL: NumInt[L])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, size.expand, num.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, num) = args: @unchecked
    import in.tpe
    val out = stream.RepeatWindow[in.A](in = in.toElem, size = size.toInt, num = num.toLong)
    tpe.mkStreamOut(out)
  }
}
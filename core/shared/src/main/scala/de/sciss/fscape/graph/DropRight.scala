/*
 *  DropRight.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{Adjunct, ProductWithAdjuncts}

import scala.collection.immutable.{IndexedSeq => Vec}

object DropRight extends ProductReader[DropRight[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DropRight[_, _] = {
    require (arity == 2)
    val _in     = in.readGE[Any]()
    val _length = in.readGE[Any]()
    val _numL   = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[Any]]()
    }
    new DropRight[Any, Any](_in, _length)(_numL)
  }
}
final case class DropRight[A, L](in: GE[A], length: GE[L])(implicit numL: NumInt[L])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[Adjunct] = numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, length.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, length) = args: @unchecked
    val inE = in.toElem
    import in.tpe
    val out = stream.DropRight[in.A](in = inE, length = length.toInt)
    tpe.mkStreamOut(out)
  }
}
/*
 *  SinkIgnore.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.stage.InHandler
import akka.stream.{Attributes, SinkShape}
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object SinkIgnore {
  def apply[A](in: Out[A])(implicit b: stream.Builder): Unit = {
    val stage0  = new Stage[A](layer = b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
  }

  private final val name = "SinkIgnore"

  private type Shp[A] = SinkShape[Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator)
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = SinkShape[Buf.E[A]](
      in = In[A](s"${stage.name}.in")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = new Logic(shape = shape, layer = layer)
  }

  private final class Logic[A](shape: Shp[A], layer: Layer)(implicit a: Allocator)
    extends NodeImpl(name, layer, shape) with InHandler {

    setHandler(shape.in, this)

    override protected def launch(): Unit = {
      logStream.debug(s"$this - launch")
      completeStage()
      // super.launch()
    }

    // should never get here
    def onPush(): Unit = {
      val buf = grab(shape.in)
      buf.release()
      tryPull(shape.in)
    }
  }
}

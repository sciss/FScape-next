/*
 *  FilterInAAOut1A.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream
package impl.logic

import akka.stream.{FlowShape, Inlet, Shape}
import de.sciss.fscape.DataType
import de.sciss.fscape.stream.impl.Handlers
import de.sciss.fscape.stream.impl.Handlers.{InMain, OutMain}

import scala.annotation.tailrec
import scala.math.min

/** Building block for a filter with one hot inlet.
  * Implementing classes have to provide the core loop `run`, and
  * calculate the number of frames available for all but the hot inlet through `auxAvailable`.
  */
abstract class FilterInAOutB[A, B, S <: Shape](name: String, layer: Layer, shape: S)(inlet: In[A], outlet: Out[B])
                                                                                (implicit a: Allocator,
                                                                                 aTpe: DataType[A],
                                                                                 bTpe: DataType[B])
  extends Handlers(name, layer, shape) {

  private[this] val hIn : InMain [A] = InMain [A](this, inlet )
  private[this] val hOut: OutMain[B] = OutMain[B](this, outlet)

  protected def onDone(inlet: Inlet[_]): Unit =
    if (hOut.flush()) completeStage()

  protected def run(in: Array[A], inOff: Int, out: Array[B], outOff: Int, n: Int): Unit

  protected def auxInAvailable: Int

  @tailrec
  final protected def process(): Unit = {
    val remIO = min(hIn.available, hOut.available)
    if (remIO == 0) return
    val rem   = min(remIO, auxInAvailable)
    if (rem   == 0) return

    val in      = hIn .array
    val out     = hOut.array
    val inOff   = hIn .offset
    val outOff  = hOut.offset
    run(in, inOff, out, outOff, rem)
    hIn .advance(rem)
    hOut.advance(rem)

    if (hIn.isDone) {
      if (hOut.flush()) completeStage()
      return
    }
    process()
  }
}

/** Building block for a one-inlet in/out filter with `FlowShape`.
  * Implementing classes have to provide the core loop `run`.
  */
abstract class FilterIn1Out1[A, B](name: String, layer: Layer, shape: FlowShape[Buf.E[A], Buf.E[B]])
                                                                    (implicit a: Allocator,
                                                                     aTpe: DataType[A],
                                                                     bTpe: DataType[B])
  extends FilterInAOutB[A, B, FlowShape[Buf.E[A], Buf.E[B]]](name, layer, shape)(shape.in, shape.out) {

  protected final def auxInAvailable: Int = Int.MaxValue
}
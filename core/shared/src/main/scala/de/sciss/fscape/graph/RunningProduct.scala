/*
 *  RunningProduct.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object RunningProduct extends ProductReader[RunningProduct[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): RunningProduct[_] = {
    require (arity == 2)
    val _in   = in.readGE[Any]()
    val _gate = in.readGE_B()
    val _num  = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new RunningProduct[Any](_in, _gate)(_num)
  }
}
/** A UGen that tracks the maximum value seen so far, until a trigger resets it.
  *
  * @param in     the signal to track
  * @param gate   a gate that when greater than zero (and initially) sets the
  *               the internal state is set to one.
  */
final case class RunningProduct[A](in: GE[A], gate: GE.B = false)(implicit num: Num[A])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, gate.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, gate) = args: @unchecked
    import in.tpe
    val out = stream.RunningProduct[in.A](in = in.toElem, gate = gate.toInt)
    tpe.mkStreamOut(out)
  }
}
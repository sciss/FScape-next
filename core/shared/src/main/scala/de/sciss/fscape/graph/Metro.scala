/*
 *  Metro.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Metro extends ProductReader[Metro[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Metro[_] = {
    require (arity == 2)
    val _period = in.readGE[Any]()
    val _phase  = in.readGE[Any]()
    val _num    = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[Any]]()
    }
    new Metro[Any](_period, _phase)(_num)
  }

  def apply[A](period: GE[A])(implicit num: NumInt[A]): Metro[A] =
    new Metro[A](period, Const(num.zero))
}
/** Metronome (repeated dirac) generator.
  * For a single impulse that is never repeated,
  * use a period of zero. Unlike `Impulse` which
  * uses a frequency and generates fractional phases
  * prone to floating point noise, this is UGen is
  * useful for exact sample frame spacing. Unlike `Impulse`,
  * the phase cannot be modulated.
  *
  * @param period number of frames between impulses. Zero is short-hand
  *               for an infinitely long period. One value is read per output period.
  *
  * @param phase  phase offset in frames. One value is read per output period.
  */
final case class Metro[A](period: GE[A], phase: GE[A] = 0)(implicit num: NumInt[A])
  extends UGenSource.SingleOut[Boolean] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    unwrap(this, Vector(period.expand, phase.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Boolean] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(period, phase) = args: @unchecked
    stream.Metro(period = period.toLong, phase = phase.toLong)
  }
}
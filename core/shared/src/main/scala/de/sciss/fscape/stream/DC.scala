/*
 *  DC.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import akka.stream.stage.{InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape}
import de.sciss.fscape.DataType
import de.sciss.fscape.Log.{stream => logStream}
import de.sciss.fscape.stream.impl.{NodeImpl, StageImpl}

object DC {
  def apply[A](in: Out[A])(implicit b: Builder, tpe: DataType[A]): Out[A] = {
    val stage0  = new Stage[A](b.layer)
    val stage   = b.add(stage0)
    b.connect(in, stage.in)
    stage.out
  }

  private final val name = "DC"

  private type Shp[A] = FlowShape[Buf.E[A], Buf.E[A]]

  private final class Stage[A](layer: Layer)(implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = new FlowShape(
      in  = In [A](s"${stage.name}.in" ),
      out = Out[A](s"${stage.name}.out")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape.asInstanceOf[Shp[Double ]], layer)
      } else if (tpe.isInt) {
        new Logic[Int   ](shape.asInstanceOf[Shp[Int    ]], layer)
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape.asInstanceOf[Shp[Long   ]], layer)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer)
                                               (implicit a: Allocator, tpe: DataType[A])
    extends NodeImpl(name, layer, shape) with OutHandler { logic =>

    private[this] var hasValue = false
    private[this] var value: A = _

    def onPull(): Unit = {
      val ok = hasValue
      logStream.debug(s"$this onPull() $ok")
      if (ok) {
        process()
      }
    }

    private object InH extends InHandler {
      override def toString: String = s"$logic.in"

      def onPush(): Unit = {
        val buf = grab(shape.in)
        val ok  = !hasValue
        logStream.debug(s"$this onPush() $ok")
        if (ok) {
          value     = buf.buf(0)
          hasValue  = true
          if (isAvailable(shape.out)) process()
        }
        buf.release()
        tryPull(shape.in)
      }

      override def onUpstreamFinish(): Unit =
        if (!hasValue) super.onUpstreamFinish()
    }

    setHandler(shape.out, this)
    setHandler(shape.in, InH)

    private def process(): Unit = {
      val buf = tpe.allocBuf()
      tpe.fill(buf.buf, 0, buf.size, value)
      push(shape.out, buf)
    }
  }
}
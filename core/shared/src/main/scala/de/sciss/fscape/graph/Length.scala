/*
 *  Length.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.stream.{StreamIn, StreamOut}

import scala.collection.immutable.{IndexedSeq => Vec}

object Length extends ProductReader[Length[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Length[_] = {
    require (arity == 1 && adj == 0)
    val _in = in.readGE[Any]()
    new Length(_in)
  }
}
/** Reports the length of the input as a single value one the input has terminated. */
final case class Length[A](in: GE[A]) extends UGenSource.SingleOut[Long] {
  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    unwrap(this, Vector(in.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Long] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in) = args: @unchecked
    stream.Length(in = in.toElem)
  }
}
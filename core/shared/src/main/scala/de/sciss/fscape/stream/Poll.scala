/*
 *  Poll.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package stream

import akka.stream.Attributes
import de.sciss.fscape.stream.impl.shapes.SinkShape2
import de.sciss.fscape.stream.impl.{NodeImpl, PollImpl, StageImpl}

// XXX TODO --- we could use an `Outlet[String]` for label, that might be making perfect sense
object Poll {
  def apply[A](in: Out[A], gate: OutI, label: String)
              (implicit b: Builder, tpe: DataType[A]): Unit = {
    val stage0  = new Stage[A](layer = b.layer, label = label)
    val stage   = b.add(stage0)
    b.connect(in  , stage.in0)
    b.connect(gate, stage.in1)
  }

  private final val name = "Poll"

  private type Shp[A] = SinkShape2[Buf.E[A], Buf.I]

  private final class Stage[A](layer: Layer, label: String)
                              (implicit a: Allocator, tpe: DataType[A])
    extends StageImpl[Shp[A]](name) { stage =>

    val shape: Shape = SinkShape2(
      in0 = In[A](s"${stage.name}.in"),
      in1 = InI  (s"${stage.name}.gate")
    )

    def createLogic(attr: Attributes): NodeImpl[Shape] = {
      val res: Logic[_] = if (tpe.isDouble) {
        new Logic[Double](shape = shape.asInstanceOf[Shp[Double]], layer = layer, label = label)
      } else if (tpe.isInt) {
        new Logic[Int   ](shape = shape.asInstanceOf[Shp[Int]], layer = layer, label = label)
      } else {
        assert (tpe.isLong)
        new Logic[Long  ](shape = shape.asInstanceOf[Shp[Long]], layer = layer, label = label)
      }
      res.asInstanceOf[Logic[A]]
    }
  }

  private final class Logic[@specialized(Args) A](shape: Shp[A], layer: Layer, label: String)
                                                 (implicit a: Allocator, tpe: DataType[A])
    extends PollImpl[A](name, layer, shape) { logic =>

    override def toString = s"${logic.name}-L($label)"

    protected def trigger(buf: Array[A], off: Int): Unit = {
      val x0 = buf(off)
      // XXX TODO --- make console selectable
      println(s"$label: $x0")
    }
  }
}
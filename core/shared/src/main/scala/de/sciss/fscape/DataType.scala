/*
 *  DataType.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape

import de.sciss.fscape.stream.{Allocator, Args, Buf, Out, StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{ScalarNum, ScalarOrd}

object DataType {
  implicit def int    : DataType.Num[Int   ] = StreamIn.IntType
  implicit def double : DataType.Num[Double] = StreamIn.DoubleType
  implicit def long   : DataType.Num[Long  ] = StreamIn.LongType
  //  implicit def boolean: DataType[Boolean] = StreamIn.BooleanType

  trait Num[@specialized(Args) A] extends DataType[A] {
    override def peer: ScalarNum[A]

    def add(in: Array[A], inOff: Int, out: Array[A], outOff: Int, len: Int): Unit

    override def castNum: Num[A] = this

//    def minValue: A
//    def maxValue: A
  }

//  trait FromAny[@specialized(Args) A] extends DataType[A] {
//    override def peer: LFromAny[A] with ScalarOrd[A]
//  }
}
trait DataType[@specialized(Args) A] {
  def peer: ScalarOrd[A]

  def castNum: DataType.Num[A]

  implicit def ordering: Ordering[A]

  type Buf = Buf.E[A]

  def mkStreamOut(out: Out[A]): StreamOut

  def allocBuf()(implicit allocator: Allocator): Buf

  def fill    (a: Array[A], off: Int, len: Int, elem: A): Unit
  def clear   (a: Array[A], off: Int, len: Int): Unit
  def reverse (a: Array[A], off: Int, len: Int): Unit

  def zero    : A
//  def minValue: A
//  def maxValue: A

  def newArray(size: Int): Array[A]

  def isInt     : Boolean
  def isLong    : Boolean
  def isDouble  : Boolean
  //  def isBoolean : Boolean
}
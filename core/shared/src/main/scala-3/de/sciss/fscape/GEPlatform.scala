package de.sciss.fscape

import de.sciss.fscape.graph.ConstL
import de.sciss.fscape.graph.ConstD

object AsDouble {
  implicit object int extends AsDouble[Int] {
    def apply(x: Int): GE.D = ConstD(x)
  }

  implicit object double extends AsDouble[Double] {
    def apply(x: Double): GE.D = ConstD(x)
  }
}
trait AsDouble[A] {
  def apply(x: A): GE.D
}

trait GEPlatform {
  implicit def doubleFrom[A](in: A)(implicit as: AsDouble[A]): GE.D = as(in)
//  implicit def intLongConst(in: Int): GE[Long] = new ConstL(in)
//  implicit def intLongConst(in: GE[Int]): GE[Long] = ... // new ConstL(in)
//  implicit def intLongConst[A](in: A)(implicit view: A => GE[Int]): GE[Long] = ... // new ConstL(in)
}

/*
 *  Fourier.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.NumInt
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Fourier extends ProductReader[Fourier[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Fourier[_] = {
    require (arity == 5)
    val _in       = in.readGE[Double]()
    val _size     = in.readGE[Any]()
    val _padding  = in.readGE[Any]()
    val _dir      = in.readGE[Int]()
    val _mem      = in.readGE[Int]()
    val _num      = if (adj == 0) LegacyAdjunct.NumInt else {
      require (adj == 1)
      in.readAdjunct[NumInt[Any]]()
    }
    new Fourier[Any](_in, _size, _padding, _dir, _mem)(_num)
  }
}
/** Disk-buffered (large) Fourier transform.
  * Output windows will have a complex size of `(size + padding).nextPowerOfTwo`
  *
  * @param in       input signal to transform. This must be complex (Re, Im interleaved)
  * @param size     the (complex) window size
  * @param padding  the (complex) zero-padding size for each window
  * @param dir      the direction is `1` for forward and `-1` for backward transform.
  *                 other numbers will do funny things.
  * @param mem      the amount of frames (chunk size) to buffer in memory. this should be
  *                 a power of two.
  */
final case class Fourier[L](in: GE.D, size: GE[L], padding: GE[L] = 0, dir: GE.I = 1, mem: GE.I = 131072)
                           (implicit num: NumInt[L])
  extends UGenSource.SingleOut[Double] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    unwrap(this, Vector(in.expand, size.expand, padding.expand, dir.expand, mem.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[Double] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, size, padding, dir, mem) = args: @unchecked
    stream.Fourier(in = in.toDouble, size = size.toLong, padding = padding.toLong, dir = dir.toDouble, mem = mem.toInt)
  }
}
/*
 *  Plot1D.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGen.Adjunct
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.StreamIn
import de.sciss.lucre.{Adjunct => LAdjunct}
import de.sciss.lucre.Adjunct.Num
import de.sciss.lucre.ProductWithAdjuncts

import scala.collection.immutable.{IndexedSeq => Vec}

object Plot1D extends ProductReader[Plot1D[_]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Plot1D[_] = {
    require (arity == 3)
    val _in     = in.readGE[Any]()
    val _size   = in.readGE_I()
    val _label  = in.readString()
    val _num    = if (adj == 0) LegacyAdjunct.Num else {
      require (adj == 1)
      in.readAdjunct[Num[Any]]()
    }
    new Plot1D[Any](_in, _size, _label)(_num)
  }
}
/** Debugging utility that plots 1D "windows" of the input data.
  */
final case class Plot1D[A](in: GE[A], size: GE.I, label: String = "plot")(implicit num: Num[A])
  extends UGenSource.ZeroOut with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): Unit =
    unwrap(this, Vector(in.expand, size.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): Unit = {
    UGen.ZeroOut(this, inputs = args, adjuncts = Adjunct.String(label) :: Nil)
    ()
  }

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): Unit = {
    val Vec(in, size) = args: @unchecked
    import in.tpe
    stream.Plot1D[in.A](in = in.toElem, size = size.toInt, label = label)
  }
}

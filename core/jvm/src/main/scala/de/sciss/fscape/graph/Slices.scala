/*
 *  Slices.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape
package graph

import de.sciss.fscape.Graph.{ProductReader, RefMapIn}
import de.sciss.fscape.UGenSource.unwrap
import de.sciss.fscape.impl.LegacyAdjunct
import de.sciss.fscape.stream.{StreamIn, StreamOut}
import de.sciss.lucre.Adjunct.{Num, NumInt}
import de.sciss.lucre.{ProductWithAdjuncts, Adjunct => LAdjunct}

import scala.collection.immutable.{IndexedSeq => Vec}

object Slices extends ProductReader[Slices[_, _]] {
  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Slices[_, _] = {
    require (arity == 2)
    val _in     = in.readGE[Any]()
    val _spans  = in.readGE[Any]()
    require (adj == 2 || adj == 0)
    val _num    = if (adj == 0) LegacyAdjunct.Num     else in.readAdjunct[Num   [Any]]()
    val _numL   = if (adj == 0) LegacyAdjunct.NumInt  else in.readAdjunct[NumInt[Any]]()
    new Slices[Any, Any](_in, _spans)(_num, _numL)
  }
}
/** A UGen that assembles slices of an input signal in
  * random access fashion. It does so by buffering the
  * input to disk.
  *
  * @param in     the signal to re-arrange.
  * @param spans  successive frame start (inclusive) and
  *               stop (exclusive) frame
  *               positions determining the spans that
  *               are output by the UGen. This parameter
  *               is read on demand. First, the first two
  *               values are read, specifying the first span.
  *               Only after this span has been output,
  *               the next two values from `spans` are
  *               read, and so on. Values are clipped to
  *               zero (inclusive) and the length of the
  *               input signal (exclusive). If a
  *               start position is greater than a stop
  *               position, the span is output in reversed order.
  *
  * @see [[ScanImage]]
  */
final case class Slices[A, L](in: GE[A], spans: GE[L])(implicit num: Num[A], numL: NumInt[L])
  extends UGenSource.SingleOut[A] with ProductWithAdjuncts {

  override def adjuncts: List[LAdjunct] = num :: numL :: Nil

  protected def makeUGens(implicit b: UGenGraph.Builder): UGenInLike[A] =
    unwrap(this, Vector(in.expand, spans.expand))

  protected def makeUGen(args: Vec[UGenIn[_]])(implicit b: UGenGraph.Builder): UGenInLike[A] =
    UGen.SingleOut(this, args)

  private[fscape] def makeStream(args: Vec[StreamIn])(implicit b: stream.Builder): StreamOut = {
    val Vec(in, spans) = args: @unchecked
    import in.tpe
    val out = stream.Slices[in.A](in = in.toElem, spans = spans.toLong)
    tpe.mkStreamOut(out)
  }
}
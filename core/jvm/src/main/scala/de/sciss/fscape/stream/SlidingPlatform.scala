/*
 *  SlidingPlatform.scala
 *  (FScape)
 *
 *  Copyright (c) 2001-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.fscape.stream

import java.io.RandomAccessFile
import java.nio.channels.FileChannel

import de.sciss.file.File
import de.sciss.fscape.stream.Sliding.{Logic, Shp, Window, WindowD, WindowI, WindowL}

trait SlidingPlatform {
  private final class DisposeFile(f: File, raf: RandomAccessFile) extends (() => Unit) {
    def apply(): Unit = {
      raf.close()
      f.delete()
      ()
    }
  }

  protected final class LogicD(shape: Shp[Double], layer: Layer)(implicit a: Allocator)
    extends Logic[Double](shape, layer) with LogicPlatform[Double] {

    type A = Double

    protected def mkMemWindow(sz: Int): Window[A] = new WindowD(new Array[Double](sz))

    def mkDiskWindow(sz: Int, f: File, raf: RandomAccessFile): Window[A] = {
      val fch   = raf.getChannel
      val bb    = fch.map(FileChannel.MapMode.READ_WRITE, 0L, sz * 8)
      val db    = bb.asDoubleBuffer()
      new WindowD(db, sz, new DisposeFile(f, raf))
    }
  }

  protected final class LogicI(shape: Shp[Int], layer: Layer)(implicit a: Allocator)
    extends Logic[Int](shape, layer) with LogicPlatform[Int] {

    type A = Int

    protected def mkMemWindow(sz: Int): Window[A] = new WindowI(new Array(sz))

    def mkDiskWindow(sz: Int, f: File, raf: RandomAccessFile): Window[A] = {
      val fch   = raf.getChannel
      val bb    = fch.map(FileChannel.MapMode.READ_WRITE, 0L, sz * 4)
      val db    = bb.asIntBuffer()
      new WindowI(db, sz, new DisposeFile(f, raf))
    }
  }

  protected final class LogicL(shape: Shp[Long], layer: Layer)(implicit a: Allocator)
    extends Logic[Long](shape, layer) with LogicPlatform[Long] {

    type A = Long

    protected def mkMemWindow(sz: Int): Window[A] = new WindowL(new Array(sz))

    def mkDiskWindow(sz: Int, f: File, raf: RandomAccessFile): Window[A] = {
      val fch   = raf.getChannel
      val bb    = fch.map(FileChannel.MapMode.READ_WRITE, 0L, sz * 8)
      val db    = bb.asLongBuffer()
      new WindowL(db, sz, new DisposeFile(f, raf))
    }
  }

  protected trait LogicPlatform[A] {

    protected def mkMemWindow(sz: Int): Window[A]

    protected def mkDiskWindow(sz: Int, f: File, raf: RandomAccessFile): Window[A]

    protected def mkWindow(sz: Int)(implicit control: Control): Window[A] =
      if (sz <= control.nodeBufferSize) {
        mkMemWindow(sz)
      } else {
        val f     = control.createTempFile()
        val raf   = new RandomAccessFile(f, "rw")
        mkDiskWindow(sz, f, raf)
      }
  }
}

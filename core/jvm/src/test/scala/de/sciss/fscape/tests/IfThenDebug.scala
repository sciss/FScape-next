package de.sciss.fscape
package tests

import de.sciss.fscape.Ops._
import de.sciss.fscape.gui.SimpleGUI

import scala.swing.Swing

object IfThenDebug extends App {
  def any2stringadd: Any = ()

  val g = Graph {
    import graph._
    val sig0  = ArithmSeq(length = 4000) // DC(1234).take(4000)
    val sig   = If (false: GE.B) Then {
      (0: GE.I).poll(true, "COND is 1")      // expected sum: 7998000
      sig0
    } Else {
      (0: GE.I).poll("COND is NOT 1")  // expected sum: 8002000
      sig0 + 1
    }

    RunningSum(sig).last.poll("sum")
//    Length(sig).poll(0, "len")
  }

  val config      = stream.Control.Config()
  config.useAsync = false // for debugging
  val ctrl        = stream.Control(config)

  ctrl.run(g)

  Swing.onEDT {
    SimpleGUI(ctrl)
  }

  println("Running.")
}

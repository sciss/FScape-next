package de.sciss.fscape
package tests

import de.sciss.fscape.Ops._
import de.sciss.fscape.gui.SimpleGUI

import scala.swing.Swing

object IfThenElseUnitTest extends App {
  val g = Graph {
    import graph._
    DC(0).take(10000).poll( "zero")
    val p1: GE.B = true
    val p2: GE.B = false
    val p3: GE.B = false
    If (p1) Then {
      DC(1).take(20000).poll("one")
      If (p2) Then {
        DC(2).take(30000).poll("two")
      } ElseIf (p3) Then {
        DC(3).take(40000).poll("three")
      } Else {
        DC(4).take(50000).poll("four")
      }
    }
  }

  val config      = stream.Control.Config()
  config.useAsync = false // for debugging
  val ctrl        = stream.Control(config)

  ctrl.run(g)

  Swing.onEDT {
    SimpleGUI(ctrl)
  }

  println("Running.")
}

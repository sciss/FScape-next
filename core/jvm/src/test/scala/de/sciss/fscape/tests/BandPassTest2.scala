package de.sciss.fscape
package tests

import de.sciss.audiofile.AudioFile
import de.sciss.file._

object BandPassTest2 extends App {
  val fIn       = userHome / "Music" / "numbers-italian-cc.aif"
  val fOut      = userHome / "Music"/ "temp" / "filter-out.aif"
  val specIn    = AudioFile.readSpec(fIn)
  val specOut   = specIn  // you could change the file type or sample format here
  val sr        = specIn.sampleRate
  val inLen     = specIn.numFrames

  val f1        = 400.0   // lower frequency in Hz
  val f2        = 800.0   // upper frequency in Hz
  val numZero   = 6       // filter design: number of zero crossings in windowed sinc
  val kaiser    = 6.0     // filter design: kaiser window beta parameter
  val tpe       = 2       // 0: low-pass, 1: high-pass, 2: band-pass, 3: band-stop

  val f1N       = f1 / sr   // normalize frequencies
  val f2N       = f2 / sr
  val fltLenH   = (numZero / f1N).ceil.toInt.max(1)
  val fltLen    = fltLenH << 1

  val g = Graph {
    import graph._
    import Ops._
    val in = AudioFileIn(file = fIn.toURI, numChannels = specIn.numChannels)

    val flt0: GE.D = if (tpe == 0) {
      // low-pass
      GenWindow.Sinc(fltLen, param = f1N) * f1N
    } else if (tpe == 1) {
      // high-pass
      GenWindow.Sinc(fltLen, param = 0.5) * 0.5 -
      GenWindow.Sinc(fltLen, param = f1N) * f1N
    } else if (tpe == 2) {
      // band-pass
      GenWindow.Sinc(fltLen, param = f2N) * f2N -
      GenWindow.Sinc(fltLen, param = f1N) * f1N
    } else {
      // band-stop
      GenWindow.Sinc(fltLen, param = 0.5) * 0.5 -
      GenWindow.Sinc(fltLen, param = f2N) * f2N +
      GenWindow.Sinc(fltLen, param = f1N) * f1N
    }
    val win       = GenWindow.Kaiser(fltLen, param = kaiser)
    val flt       = flt0 * win
    val conv      = Convolution(in, flt, kernelLen = fltLen)
    val sig0      = conv.drop(fltLenH).take(inLen)

    def normalize(x: GE.D) = {
      val buf       = BufferDisk(x)
      val rMax      = RunningMax(Reduce.max(x.abs))
      val maxAmp    = rMax.last
      val gainAmtN  = 0.97 / maxAmp
      buf * gainAmtN
    }

    val sig = normalize(sig0)
    AudioFileOut(sig, fOut.toURI, specOut)
  }

  val ctrl  = stream.Control()

  ctrl.run(g)
  import ctrl.config.executionContext
  ctrl.status.foreach { _ => sys.exit() }
}

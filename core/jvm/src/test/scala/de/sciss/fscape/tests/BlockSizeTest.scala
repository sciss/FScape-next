package de.sciss.fscape.tests

import de.sciss.fscape.{GE, Graph, Ops, graph, stream}

// XXX TODO: make this a unit test
object BlockSizeTest extends App {
  val g = Graph {
    import graph._
    import Ops._

//    val sz = BlockSize(4) {
//      ControlBlockSize()
//    }
//    sz.poll(0, "block-size")
//

    val flag: GE.B = true

    BlockSize(32) {
      If (flag) Then {
        val sz = ControlBlockSize()
        sz.poll("block-size")
      }
      ()
    }

  }

  stream.Control().run(g)
}
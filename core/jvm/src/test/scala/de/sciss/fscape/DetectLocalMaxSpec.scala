package de.sciss.fscape

import de.sciss.fscape.Ops._
import de.sciss.kollflitz.Vec

import scala.concurrent.Promise

class DetectLocalMaxSpec extends UGenSpec {
  "The DetectLocalMax UGen" should "work as intended" in {
    val p = Promise[Vec[Int]]()
    val g = Graph {
      import graph._
      val sz      = 1024
      val period  = 100
      val block   = 2 * period + 1
      val gen     = SinOsc(1.0/period).take(sz) * Line(0.5, 1.0, sz)
      val det     = DetectLocalMax(gen, size = block)
      val d       = Frames(det).filter(det)
      DebugIntPromise(d.toInt, p)
    }

    runGraph(g, 1024)

    assert(p.isCompleted)
    val res = getPromiseVec(p)
    assert (res === Vec(226, 726))
  }

  it should "work for plateaus" in {
    val p = Promise[Vec[Int]]()
    val g = Graph {
      import graph._
      val diff = ValueDoubleSeq(
      //0    1    2   *3*   4    5    6   *7*   8    9   *10*  11
        0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 2.0, 1.0, 2.0, 3.0, 0.0
      )
      val det = DetectLocalMax(diff, size = 1)
      val d   = Indices(det).filter(det)
      DebugIntPromise(d.toInt, p)
    }

    runGraph(g, 1024)

    assert(p.isCompleted)
    val res = getPromiseVec(p)
    assert (res === Vec(3, 7, 10))
  }
}
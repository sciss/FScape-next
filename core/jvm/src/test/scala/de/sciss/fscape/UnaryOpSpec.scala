package de.sciss.fscape

import de.sciss.fscape.graph.UnaryOp
import de.sciss.fscape.graph.UnaryOp.Op
import de.sciss.kollflitz.Vec

import scala.concurrent.Promise

class UnaryOpSpec extends UGenSpec {
  case class Case[A, B, B1](op: Op[A, B], in: Vec[A], out: Vec[B1])

  "The UnaryOp UGen" should "work as intended" in {
    val dataSq = Seq[Case[_, _, _]](
      Case(UnaryOp.Abs[Double]()    , Vec(-3.0, -0.3, -0.0, 0.0, 1.3), Vec(3.0, 0.3, 0.0, 0.0, 1.3)),   // DD
      Case(UnaryOp.Abs[Int]()       , Vec(-3, 0, 1)                  , Vec(3, 0, 1)),                   // II
      Case(UnaryOp.Abs[Long]()      , Vec(-3L, 0L, 1L)               , Vec(3L, 0L, 1L)),                // LL
      Case(UnaryOp.IsNaN()          , Vec(-3.0, NaN, 0.0, Double.PositiveInfinity) , Vec(/*false*/ 0, /*true*/ 1, /*false*/ 0, /*false*/ 0)),  // DI
      Case(UnaryOp.ToLong[Double]() , Vec(-3.0, -0.3, 0.0, Double.PositiveInfinity), Vec(-3L, 0L, 0L, Long.MaxValue)),  // DL
      Case(UnaryOp.Reciprocal()     , Vec[Double](-3, 4, 1), Vec(1.0/(-3), 1.0/4, 1.0/1)),                    // ID -- NO, it's DD
      Case(UnaryOp.Squared[Long]()  , Vec[Long](-3, 4, 1), Vec(9L, 16L, 1L)),                           // IL -- NO, it's LL
      Case(UnaryOp.ToInt[Long]()    , Vec(-3L, 4L, Long.MaxValue), Vec(-3, 4, Int.MaxValue /* -1*/)),               // LI
    )

    for {
      Case(op, dataIn, exp) <- dataSq
    } {
      import graph._
      val p = Promise[Vec[Any]]()

      val g = Graph {
        val in: GE[_] = if (isD(dataIn)) {
          val inSq = asD(dataIn)
          ValueDoubleSeq(inSq: _*)
        } else if (isI(dataIn)) {
          val inSq = asI(dataIn)
          ValueIntSeq(inSq: _*)
        } else {
          assert(isL(dataIn))
          val inSq = asL(dataIn)
          ValueLongSeq(inSq: _*)
        }
        val sig = UnaryOp[Any, Any](op.asInstanceOf[Op[Any, Any]], in.asInstanceOf[GE[Any]])
        DebugAnyPromise(sig, p)
      }

      runGraph(g)
      val obs = p.future.value.get.get
      assert (obs === exp, s"For $op")
    }
  }
}